window.addEventListener("load", function(){
    
    var url = document.getElementById('URLPATH').value; 
    console.log(url);    

    $(".btn-like").css("cursor","pointer");
    $(".btn-dislike").css("cursor","pointer");

    function like(){
        //Boton de like
        //El unbind lo que hace es arreglarte los llamados al click parra que sea una sola vez
        $(".btn-like").unbind('click').click(function(){
            var thumb = $(this);
            /* $(this).addClass("btn-dislike").removeClass("btn-like");
            $(this).addClass("text-danger").removeClass("text-secondary"); */

            //Petición de ajax para que ejecute el evento de like
            var noticia_id = $(this).attr("id");
            $.ajax({
                url: url+'/like/'+noticia_id,
                type: 'GET',
                success: function(response){
                    if(response.like){
                        console.log(response.message);   
                        thumb.addClass("btn-dislike").removeClass("btn-like");
                        thumb.addClass("fas").removeClass("far");
                        $('#thumbCount'+noticia_id).text(response.cantidad);
                    }
                    else if(response.error){
                        console.log(response.error);
                    }
                    else{
                        console.log(response.message);
                        $('#thumbCount'+noticia_id).text(response.cantidad);
                    }
                }
            });

            dislike();
        });
    }
    like();

    function dislike(){
        //Boton de dislike
        //El unbind lo que hace es arreglarte los llamados al click parra que sea una sola vez
        $(".btn-dislike").unbind('click').click(function(){
            var thumb = $(this);
            /* $(this).addClass("btn-like").removeClass("btn-dislike");
            $(this).addClass("text-secondary").removeClass("text-danger"); */
            
            //Petición de ajax para que ejecute el evento de like
            var noticia_id = $(this).attr("id");
            $.ajax({
                url: url+'/dislike/'+noticia_id,
                type: 'GET',
                success: function(response){
                    if(response.dislike){
                        console.log(response.message);   
                        thumb.addClass("btn-like").removeClass("btn-dislike");
                        thumb.addClass("far").removeClass("fas");
                        $('#thumbCount'+noticia_id).text(response.cantidad);
                    }
                    else if(response.error){
                        console.log(response.error);
                    }
                    else{
                        console.log(response.message);
                        $('#thumbCount'+noticia_id).text(response.cantidad);
                    }
                }
            });

            like();
        });
    }
    dislike();

    //BUSCADOR
    /* $('#buscador').submit(function(){
        //CAPTURO EL EVENTO SUBMIT
        //MODIFICO EL VALOR DE LA URL DENTRO DEL ACTION
        $(this).attr('action',url+'user/people/'+$('#buscador #search').val());
    }); */

});