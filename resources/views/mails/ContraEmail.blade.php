<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0;">
  <meta name="format-detection" content="telephone=no" />

  <!-- Responsive Mobile-First Email Template by Konstantin Savchenko, 2015.
	https://github.com/konsav/email-templates/  -->

  <style>
    /* Reset styles */
    body {
      margin: 0;
      padding: 0;
      min-width: 100%;
      width: 100% !important;
      height: 100% !important;
    }

    body,
    table,
    td,
    div,
    p,
    a {
      -webkit-font-smoothing: antialiased;
      text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      line-height: 100%;
      text-decoration-line: none !important;
    }

    table,
    td {
      border-collapse: collapse !important;
      border-spacing: 0;
    }

    img {
      border: 0;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    @media all and (min-width: 560px) {
      .container {
        border-radius: 8px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        -khtml-border-radius: 8px;
      }
    }

    a,
    a:hover {
      color: #127DB3;
    }

    .footer a,
    .footer a:hover {
      color: #999999;
    }
  </style>

  <title>Get this responsive email template</title>

</head>

<body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #F0F0F0;
	color: #000000;" bgcolor="#F0F0F0" text="#000000">

  <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;" class="background">
    <tr>
      <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;" bgcolor="#F0F0F0">

        <table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;" class="container">

          <tr>
          <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
			" class="hero"><img border="0" vspace="0" hspace="0" src="https://i.ibb.co/WzCqfdH/4-KV-MAIL-BIENVENIDA.jpg" alt="Please enable images to view this content" title="Hero Image" width="560" style="
			width: 100%;
			max-width: 560px;
			color: #000000; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" /></td>
          
          </tr>
          <tr>
            <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 25px; font-weight: 300; line-height: 150%;
			padding-top: 20px;
			color: #e72f4b; font-weight: bold;
			font-family: sans-serif;" class="subheader">
              HOLA USUARIO!
            </td>
          </tr>

          <tr>
                        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 23px; font-weight: bold; line-height: 130%;
			padding-top: 5px;
			color: #000000;
			font-family: sans-serif;" class="header">
                            {{ $nombre }}
                        </td>
                    </tr>

          <tr>
          
          <tr>
            <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 400; line-height: 160%;
			padding-top: 25px;
            padding-bottom: 20px;
			color: #000000;
			font-family: sans-serif;" class="paragraph">
            Recibimos una solicitud de restablecimiento de contraseña para su cuenta. Posees 30 minutos para cambiar tu contraseña antes de que el token expire. <br><br>
            Si deseas hacer este cambio, <br> presiona en restablecer la contraseña:<br>
            </td>
          </tr>

         
          <tr>
            <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 10px;
			padding-bottom: 25px;" class="button"><a href="https://dentapp.panelrp.com" target="_blank" style="text-decoration: underline;">
                <table border="0" cellpadding="0" cellspacing="0" align="center" style=" max-width: 340px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;">
                  <tr>
                    <td align="center" valign="middle" style="align-items: center; display: flex; padding: 12px 24px; margin: 0; text-decoration: underline; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;" bgcolor="#01c3cc">
                    <a target="_blank" style="text-decoration-line: none;
					color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;" href="{{ $url }}">
                         RESTABLECER CONTRASEÑA
                      </a>
                    </td>
                  </tr>
                </table>
              </a>
            </td>
          </tr>

          <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: bold; line-height: 130%;
			padding-top: 5px;
            padding-bottom: 20px; 
			color: #000000;
			font-family: sans-serif;" class="header">
                             Si no solicitó un restablecimiento de contraseña, <br> no es necesario realizar ninguna otra acción.
                        </td>
                    </tr>

          <tr>

        </table>

        <table border="0" cellpadding="0" cellspacing="0" align="center" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;" class="wrapper">

          <tr>
            <td align="center" valign="top" style="background: #f7f7f7; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 10px; padding-bottom: 10px" class="social-icons">
              <table width="256" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; border-spacing: 0; padding: 0;">
                <tr>

                  <!-- ICON 1 -->
                  <td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 95px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;"><a target="_blank" href="https://play.google.com/store/apps/details?id=com.peoplemarketingsas.DentApp&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
					color: #000000;" alt="F" title="Play Store" width="120" height="44" src="https://miriadax.net/miriadax-theme/images/custom/svg/store/ico_app_google_es.png"></a></td>

                  <!-- ICON 2 -->
                  <td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;"><a target="_blank" href="https://apps.apple.com/ec/app/dentapp/id1574716462" style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
					color: #000000;" alt="T" title="App Store" width="100" height="30" src="https://appsflyersdk.github.io/appsflyer-onelink-smart-script/images/app_store.png"></a></td>

                  <!-- ICON 3 -->
                  <td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 95px; border-collapse: collapse; border-spacing: 0;"><a target="_blank" href="https://api.whatsapp.com/send?phone=+573125614712&text=Buen día, necesito ayuda con DentApp, mi nombre es ..." style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
					color: #000000;" alt="G" title="WhatsApp" width="44" height="44" src="https://i.ibb.co/kGXmLdP/whatsapp.png"></a></td>

             
                </tr>
              </table>
            </td>
          </tr>

        </table>


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;" class="wrapper">

          <tr>
            <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 87.5%;
			
			padding-bottom: 10px;">


              <img border="0" vspace="0" hspace="0" src="https://i.ibb.co/SRQq07d/footer.jpg" width="100%" height="35%" alt="Logo" title="Logo" style="
				color: #000000;
				font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" />

            </td>
          </tr>

        </table>

      </td>
    </tr>
  </table>

</body>

</html>