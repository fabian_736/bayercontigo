<div class="card-transparent py-3">
    <div class="form-reverse mx-auto">
        <a href="{{route('insignia_profesional.index')}}">
            <div class="col">

                <div class="bg-dark mx-auto d-flex align-items-center" style="background:radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%);;;
            border-radius: 80px; width: 130px; height: 130px;">
                    <i class="fas fa-medal text-white mx-auto" style="font-size: 75px"></i>
                </div>
            </div>
            <div class="col d-flex justify-content-center mt-2">
                <label for="" class="">INSIGNIAS</label>
            </div>
        </a>
    </div>

    <div class="form-reverse mx-auto my-3">
        <a href="{{route('premio_profesional.index')}}">
        <div class="col">
            <div class="bg-dark mx-auto d-flex align-items-center" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%);;
            border-radius: 80px; width: 130px; height: 130px;">
                <i class="fas fa-trophy text-white mx-auto" style="font-size: 75px"></i>
            </div>
        </div>
        <div class="col d-flex justify-content-center mt-2">
            <label for="" class="">PREMIOS</label>
        </div>
    </a>
    </div>

    <div class="form-reverse mx-auto">
        <a href="{{route('muro_profesional.index')}}">
            <div class="col">
                <div class="bg-dark mx-auto d-flex align-items-center" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%);;
            border-radius: 80px; width: 130px; height: 130px;">
                    <i class="fas fa-book text-white mx-auto" style="font-size: 75px"></i>
                </div>
            </div>
            <div class="col d-flex justify-content-center mt-2">
                <label for="" class="">MURO</label>
            </div>
        </a>
    </div>
</div>