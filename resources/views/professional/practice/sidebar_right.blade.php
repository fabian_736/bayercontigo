


<!-- HEAD (HTML) -->

@include('layouts.professional.components.head');

<!-- FIN HEAD (HTML) -->

<!-- NAVBAR -->

@include('layouts.professional.components.navbar');

<!-- FIN NAVBAR -->

<div class="content">
  <div class="content">
    <div class="row">

      <!-- BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO - SIDEBAR-RIGHT) -->

      <div class="col-md-1" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              @include('layouts.professional.components.left');
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card-transparent">
                @yield('content')
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="col-md-3" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <div class="card-transparent py-3">
                    <!-- ESPACIO CIRCULAR CON LOS ENTRENAMIENTOS -->
                    @foreach($capacitaciones as $key => $capacitacion)
                      <div class="form-reverse mx-auto">
                          <div class="col">
                              <a href="{{ route('practice_profesional.index', ['id' => $id_prod, 'id_capacitacion' => $capacitacion->id_capacitacion]) }}">
                                @if($id_cap)
                                  <div class="bg-dark mx-auto d-flex align-items-center clickHearth
                                      {{ $capacitacion->id_capacitacion == $id_cap ? 'selectedTraining' : 'unselectedTraining' }}">
                                      <i class="fas fa-heartbeat text-white mx-auto" style="font-size: 75px"></i>
                                  </div>
                                @else
                                  <div class="bg-dark mx-auto d-flex align-items-center clickHearth
                                      {{ $key == 0 ? 'selectedTraining' : 'unselectedTraining' }}">
                                      <i class="fas fa-heartbeat text-white mx-auto" style="font-size: 75px"></i>
                                  </div>
                                @endif
                              </a>
                          </div>
                          <div class="col d-flex justify-content-center mt-2">
                              <label for="" class="">ENTRENAMIENTO {{ $key + 1 }}</label>
                          </div>
                      </div>
                    @endforeach
                    <!-- FIN ESPACIO -->
                </div>
            </div>
          </div>
        </div>
      </div>

      <!-- FIN BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO - SIDEBAR-RIGHT) -->
    </div>
  </div>

</div>

<style>
    .selectedTraining {
      background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);
      border-radius: 80px; 
      width: 130px; 
      height: 130px;
    }

    .unselectedTraining {
      background: #CCCCCC;
      border-radius: 80px; 
      width: 130px; 
      height: 130px;
    }
</style>

<!-- FOOTER -->
@include('layouts.professional.components.footer');
<!-- FIN FOOTER -->

<!-- END (HTML) -->
@include('layouts.professional.components.end');
<!-- FIN END (HTML) -->

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $('.clickHearth').click(function(){
            $('.clickHearth').removeClass('selectedTraining');
            $('.clickHearth').addClass('unselectedTraining');

            $(this).removeClass('unselectedTraining');
            $(this).addClass('selectedTraining');
        });
    });
</script>