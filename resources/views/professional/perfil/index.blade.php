@extends('layouts.professional.app2')
@section('content')

<div class="row">

    <div class="col-12 text-center">
        @if (Session::has('message'))
            <div class="alert alert-success" style="padding: 10px;">
                {{ Session::get('message') }}
            </div>
        @endif
    </div>

    <!-- FRAJA LATERAL IZQUIERDA CON BOTONES -->
        <div class="col-md-1">
            <div style="width: 16px; height: 100% !important; border-radius: 40px; padding: 3px; background: #FFFFFF; box-shadow: inset 0px 12.6135px 12.6135px rgba(0, 0, 0, 0.25);">
                <div class="d-flex justify-content-center" style="height: 100%; width: 100%; border-radius: 40px;">
                    <div id="contenedor" style="position:absolute; width: 12px; border-radius: 40px; z-index: 1000"></div>
                    <div class="row d-flex justify-content-center" style="z-index: 1001">
                        <!-- PRIMER BOTON DEL PERFIL -->
                        <div class="bottom-circle-one circleClick" id="1"></div>
                        <!-- ENTRAR A OPCIÓN DE LOS CUESTIONARIOS COMPLETADOS -->
                        @foreach($user->cuestionarios as $key => $cuestionario)
                            <div class="bottom-circle circleClick" id="{{ $key + 2 }}"></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <!-- FIN FRANJA -->

    <!-- CARD A CARD POR CAPACITACION -->
        <div class="col-md-6">
            <div class="row">
                <!-- CONTENEDOR DEL PERFIL -->
                    <div class="card cuadroTexto m-0 p-5" id="card1">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <h3 for="" class="font-weight-bold" style="color:#053F72;">
                                        {{ $user->profesional ? $user->profesional->nombre . ' ' . $user->profesional->apellido : "Profesional" }}
                                    </h3><br>
                                </div>
                                <div class="row">
                                    <label for="" class="lead" style="color:#053F72; font-style: italic;">
                                        {{ $user->profesional ? $user->profesional->especialidad : "Médico" }}
                                    </label>
                                </div>
                                <div class="row my-3">
                                    <p>
                                        {{ $user->profesional ? $user->profesional->descripcion : "No posee" }}.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div style="width: 2px; height: 75%; background-color: rgba(3, 114, 140, 1); position: absolute; left: 50%; z-index: 1000"></div>
                                <div class="row">
                                    <!-- PUNTOS GANADOS DE FORMA GLOBAL -->
                                    <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 80px; height: 80px; border-radius: 80px; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);;">
                                        <label for="" class="lead font-weight-bold text-white">
                                            {{ $user->total_puntos }}
                                        </label> 
                                    </div>
                                </div>
                                <div class="row my-3">
                                    <!-- PREMIOS CANJEADOS -->
                                    <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);;">
                                        <label for="" class="lead font-weight-bold text-white">
                                            {{ $user->canjeados->count() }}
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- INSIGNIAS CONSEGUIDAS -->
                                    <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);;">
                                        <label for="" class="lead font-weight-bold text-white">
                                            {{ $user->profesional ? $user->profesional->insignias->count() : "0." }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <a href="{{route('perfil_professional.edit')}}" class="btn btn-danger ml-auto my-3" style="background: radial-gradient(59.13% 153.27% at 6.94% 90.83%, #C4C4C4 0%, #787878 100%); box-shadow: 4.53333px 2.26667px 4.53333px rgba(0, 0, 0, 0.18);">
                                EDITAR PERFIL
                            </a>
                        </div>
                        <div class="row">
                            <a href="{{route('login.logout')}}" class="btn btn-danger ml-auto mb-3" style="background: radial-gradient(59.13% 153.27% at 6.94% 90.83%, #C4C4C4 0%, #787878 100%); box-shadow: 4.53333px 2.26667px 4.53333px rgba(0, 0, 0, 0.18);">
                                CERRAR SESIÓN
                            </a>
                        </div>
                    </div>
                <!-- FIN CONTENEDOR DEL PERFIL -->

                <!-- CONTENEDOR DE LOS CUESTIONARIOS ENVIADOS -->
                    @foreach($user->cuestionarios as $key => $cuestionario)
                        <div class="card cuadroTexto m-0 p-5 d-none" id="card{{ $key + 2 }}">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <h3 for="" class="font-weight-bold" style="color:#053F72;">
                                            {{ $cuestionario->titulo }}
                                        </h3>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <label for="" class="lead" style="color:#053F72; font-style: italic;">
                                            {{ $cuestionario->tipo }}
                                        </label>
                                    </div>
                                    <div class="row my-3">
                                        <p>
                                            {{ $cuestionario->descripcion }}.
                                        </p>
                                    </div>
                                    <div class="row">
                                        <a href="{{ route('certificado_profesional.edit', $cuestionario->id_cuestionario_user) }}" class="lead font-weight-bold" style="color: #669D00;">
                                            <i class="fas fa-file-download mr-3"></i>
                                            DESCARGAR CERTIFICADO
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 80px; height: 80px; border-radius: 80px; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);;">
                                            <label for="" class="lead font-weight-bold text-white">
                                                {{ $cuestionario->puntos }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="mx-auto text-center d-flex justify-content-center align-items-center">
                                            <label for="" class="font-weight-bold">Puntos Conseguidos</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                <!-- FIN CONTENEDOR DE LOS CUESTIONARIOS -->
            </div>
        </div>
    <!-- FIN CARD A CARD -->

    <!-- POSICIONAMIENTO DE LA IMAGEN PERFIL + INSIGNIAS / MEDALLA -->
        <div class="col-md-5 mx-auto">
            <div style=" width: 30vw; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);; height: 30vw;" class="rounded-circle mx-auto">
                <img src="{{ route('user.icon', basename($user->avatar)) }}" class="rounded-circle" style="margin-left: 10%; margin-top: 5%" width="65%" height="70%" rel="nofollow" alt="...">
                
                <!-- INSIGNIAS O MEDALLAS -->
                    <div class="row d-flex justify-content-end">
                        <div class="row">
                            <div class="col" style="top: 70%;">
                                <div class="d-flex justify-content-center align-items-center" style="width: 5vw; height: 5vw; border-radius: 60px; background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%); box-shadow: 3.7358px 1.8679px 3.7358px rgba(0, 0, 0, 0.18);">
                                    <i class="fas fa-medal text-white" style="font-size: 30px"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="d-flex justify-content-center align-items-center" style="width: 5vw; height: 5vw; border-radius: 60px; background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%); box-shadow: 3.7358px 1.8679px 3.7358px rgba(0, 0, 0, 0.18);">
                                    <i class="fas fa-medal text-white" style="font-size: 30px"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="bottom: 100% !important; right: 20%;">
                                <div class="d-flex justify-content-center align-items-center" style="width: 5vw; height: 5vw; border-radius: 60px; background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%); box-shadow: 3.7358px 1.8679px 3.7358px rgba(0, 0, 0, 0.18);">
                                    <i class="fas fa-medal text-white" style="font-size: 30px"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- FIN INSIGNIAS O MEDALLAS -->
            </div>
        </div>
    <!-- FIN POSICIONAMIENTO -->
</div>

<!-- comentado no se el uso de #contenedor -->
<script>
    /* function f1() {
        $('#div1').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'position': 'absolute',
            'width': '12px',
            'border-radius': '40px',
            'z-index': '1000',
            'background': 'transparent'
        });
        $('#card1').css({
            'display':'block'
        });
        $('#card2').css({
            'display':'none'
        });
        $('#card3').css({
            'display':'none'
        });
    };

    function f2() {
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'height': '45%'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#card1').css({
            'display':'none'
        });
        $('#card2').css({
            'display':'block'
        });
        $('#card3').css({
            'display':'none'
        });
    };

    function f3() {
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);',
            'height': '100%'
        });
        $('#card1').css({
            'display':'none'
        });
        $('#card2').css({
            'display':'none'
        });
        $('#card3').css({
            'display':'block'
        });

    }; */
</script>

@endsection

<style>
    .bottom-circle {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .bottom-circle-one {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);;
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        
        //Cambio de contenedor a cuestionarios etc
        $('.circleClick').click(function(){
            //arreglo del borde del circulo
            $('.circleClick').removeClass('bottom-circle-one');
            $('.circleClick').addClass('bottom-circle');

            $(this).removeClass('bottom-circle');
            $(this).addClass('bottom-circle-one');

            //arreglo del contenedor a visualizarse
            var id = $(this).attr('id');
            $('.cuadroTexto').addClass('d-none');
            $('#card'+id).removeClass('d-none');
        });
    });
</script>