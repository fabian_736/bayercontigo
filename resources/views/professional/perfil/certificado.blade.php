@extends('layouts.professional.app2')
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="card m-0 p-5" id="card1">
                <div class="row mx-auto">
                    <div class="col-md-12">
                        <h3 style="color: #053F72; font-weight: bold">
                            {{ $cuestionario->titulo }} ®
                            <!-- Entrenamiento básico Adempas ® -->
                        </h3>
                    </div>
                    <div class="col-md-12">
                        <h5 style="color: #053F72; font-weight: bold" for="">Certificado</h5>
                    </div>
                </div>
                <div class="row mt-4">
                    <a href="#" onclick="roar()" class="btn btn-danger ml-auto" style="background: radial-gradient(59.13% 153.27% at 6.94% 90.83%, #C4C4C4 0%, #787878 100%); box-shadow: 4.53333px 2.26667px 4.53333px rgba(0, 0, 0, 0.18);">
                        DESCARGAR
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 mx-auto">
        <div style=" width: 30vw; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);; height: 30vw;" class="p-2 rounded-circle mx-auto">
            <div class="rounded-circle d-flex justify-content-center align-items-center" style="background: rgb(255,255,255);
background: linear-gradient(90deg, rgba(255,255,255,0.4962359943977591) 100%, rgba(0,212,255,1) 100%); width: 65%; height: 65%; margin-left: 10%; margin-top: 5%">
                <img src="{{url('img/certificado.png')}}" class="imagen w-50" alt="">
                <div class="circle_percent" data-percent="100" style="display: none">
                    <div class="circle_inner">
                        <div class="round_per"></div>
                    </div>
                </div>
            </div>

            <div class="row mx-auto">
                <a class="mx-auto btn-primary-outline btn-file mt-5" onclick="windows()" style="cursor: pointer">
                    <span class="text-white"><u>VER ARCHIVO</u></span>
                </a>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>


<script>
    var progress = document.getElementById('progress')

    progress.onclick = charge

    function charge() {
        document.body.onfocus = roar
        console.log('chargin')
    }

    function roar() {
        $(".circle_percent").css({
            'display': 'block'
        })
        $(".imagen").css({
            'display': 'none'
        })

        $(".circle_percent").each(function() {
            var $this = $(this),
                $dataV = $this.data("percent"),
                $dataDeg = $dataV * 3.6,
                $round = $this.find(".round_per");
            $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
            $this.append('<div class="circle_inbox"><span class="percent_text"></span></div>');

            $this.prop('Counter', 0).animate({
                Counter: $dataV,
            }, {
                duration: 2000,
                easing: 'swing',
                step: function(now) {
                    $this.find(".percent_text").text(Math.ceil(now) + "%");

                },

            });
            if ($dataV >= 100) {
                $round.css("transform", "rotate(" + 360 + "deg)");
                setTimeout(function() {
                    $this.addClass("percent_more");
                    Swal.fire({
                        title: '<strong class="mx-3">Su certificado ha sido descargado con éxito</strong>',
                        icon: 'success',
                        showCloseButton: true,
                        confirmButtonColor: "#AEDC5A",
                        showCancelButton: false,
                        focusConfirm: false,
                        confirmButtonText: '<strong>Volver al inicio</strong>',
                    }).then(function() {
                        window.location = "{{route('perfil_professional.index')}}";
                        });
                }, 2000);
                setTimeout(function() {
                    $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
                }, 2000);

            }

        });
        document.body.onfocus = null
        console.log('depleted')
    }
</script>

<script>
    function windows() {
        window.open("https://www.proturbiomarspa.com/files/_pdf-prueba.pdf", "ventana1", "width=1000,height=1000,scrollbars=NO,left=150")

    }
</script>

<script>
    function f1() {
        $('#div1').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'position': 'absolute',
            'width': '12px',
            'border-radius': '40px',
            'z-index': '1000',
            'background': 'transparent'
        });
        $('#card1').css({
            'display': 'block'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f2() {
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(805.33% 412.26% at 48.77% -319.62%, #AEDC5A 0%, #053F72 100%)',
            'height': '45%'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'block'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f3() {
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(805.33% 412.26% at 48.77% -319.62%, #AEDC5A 0%, #053F72 100%)',
            'height': '100%'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'block'
        });

    };
</script>

@endsection

<style>
    .bottom-circle {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .bottom-circle-one {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .circle_percent {
        font-size: 200px;
        width: 1em;
        height: 1em;
        position: relative;
        background: #eee;
        border-radius: 50%;
        overflow: hidden;
        display: inline-block;
        margin: 20px;
    }

    .circle_inner {
        position: absolute;
        left: 0;
        top: 0;
        width: 1em;
        height: 1em;
        clip: rect(0 1em 1em .5em);
    }

    .round_per {
        position: absolute;
        left: 0;
        top: 0;
        width: 1em;
        height: 1em;
        background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
        clip: rect(0 1em 1em .5em);
        transform: rotate(180deg);
        transition: 1.05s;
    }

    .percent_more .circle_inner {
        clip: rect(0 .5em 1em 0em);
    }

    .percent_more:after {
        position: absolute;
        left: .5em;
        top: 0em;
        right: 0;
        bottom: 0;
        background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
        content: '';
    }

    .circle_inbox {
        position: absolute;
        top: 10px;
        left: 10px;
        right: 10px;
        bottom: 10px;
        background: #fff;
        z-index: 3;
        border-radius: 50%;
    }

    .percent_text {
        position: absolute;
        font-size: 36px;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        z-index: 3;
    }
</style>