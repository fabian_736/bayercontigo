@extends('layouts.professional.app')
@section('content')

<div class="row-reverse">
    <div class="col">
        <label for="" class="h3 font-weight-bold">Insignias Obtenidas</label>
    </div>
    <div class="col mb-5">
        <!--Carousel Wrapper-->
        <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

            <!--Controls-->
            <div class="controls-top d-flex justify-content-between">
                <a class="btn-floating btn " href="#multi-item-example" data-slide="prev" style="background: #053F72; border-radius: 20px;">ATRAS</a>
                <a class="btn-floating btn" href="#multi-item-example" data-slide="next" style="background: #053F72; border-radius: 20px;">SIGUIENTE</a>
            </div>
            <!--/.Controls-->

            <!--Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                <li data-target="#multi-item-example" data-slide-to="1"></li>
                <li data-target="#multi-item-example" data-slide-to="2"></li>
            </ol>
            <!--/.Indicators-->

            <!--Slides-->
            <div class="carousel-inner" role="listbox">

                <!--First slide-->
                <div class="carousel-item active">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="width: 200px; height: 200px" src="{{url('img/insignia_1.png')}}" alt="Card image cap">
                                
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="width: 200px; height: 200px" src="{{url('img/insignia_2.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="width: 200px; height: 200px" src="{{url('img/insignia_3.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>
                    </div>

                </div>
                <!--/.First slide-->

                <!--Second slide-->
                <div class="carousel-item">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="width: 200px; height: 200px" src="{{url('img/insignia_4.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="width: 200px; height: 200px" src="{{url('img/insignia_1.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="width: 200px; height: 200px" src="{{url('img/insignia_2.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>
                    </div>

                </div>
                <!--/.Second slide-->

            </div>
            <!--/.Slides-->

        </div>
        <!--/.Carousel Wrapper-->
    </div>
</div>

<div class="row-reverse">
    <div class="col">
        <label for="" class="h3 font-weight-bold">Insignias por Obtener</label>
    </div>
    <div class="col">
        <!--Carousel Wrapper-->
        <div id="multi-item-example2" class="carousel slide carousel-multi-item" data-ride="carousel">

            <!--Controls-->
            <div class="controls-top d-flex justify-content-between">
                <a class="btn-floating btn" href="#multi-item-example2" data-slide="prev" style="background: #AEDC5A; border-radius: 20px;">ATRAS</a>
                <a class="btn-floating btn" href="#multi-item-example2" data-slide="next" style="background: #AEDC5A; border-radius: 20px;">SIGUIENTE</a>
            </div>
            <!--/.Controls-->

            <!--Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#multi-item-example2" data-slide-to="0" class="active"></li>
                <li data-target="#multi-item-example2" data-slide-to="1"></li>
                <li data-target="#multi-item-example2" data-slide-to="2"></li>
            </ol>
            <!--/.Indicators-->

            <!--Slides-->
            <div class="carousel-inner" role="listbox">

                <!--First slide-->
                <div class="carousel-item active">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="opacity: 0.5; width: 200px; height: 200px" src="{{url('img/insignia_1.png')}}" alt="Card image cap">
                                
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="opacity: 0.5; width: 200px; height: 200px" src="{{url('img/insignia_2.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="opacity: 0.5; width: 200px; height: 200px" src="{{url('img/insignia_3.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>
                    </div>

                </div>
                <!--/.First slide-->

                <!--Second slide-->
                <div class="carousel-item">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="opacity: 0.5; width: 200px; height: 200px" src="{{url('img/insignia_4.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="opacity: 0.5; width: 200px; height: 200px" src="{{url('img/insignia_1.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <img class="card-img-top mx-auto" style="opacity: 0.5; width: 200px; height: 200px" src="{{url('img/insignia_2.png')}}" alt="Card image cap">
                               
                            </div>
                        </div>
                    </div>

                </div>
                <!--/.Second slide-->

            </div>
            <!--/.Slides-->

        </div>
        <!--/.Carousel Wrapper-->
    </div>
</div>
@endsection