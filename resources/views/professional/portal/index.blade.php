@extends('layouts.professional.app')
@section('content')

<div class="col-12 text-center">
    @if (Session::has('message'))
        <div class="alert alert-success" style="padding: 10px;">
            {{ Session::get('message') }}
        </div>
    @endif
</div>

<div class="row-reverse">
    <h3 style="color: #053F72" class="font-weight-bold">
        {{ $user->profesional ? $user->profesional->nombre . ' ' . $user->profesional->apellido : "USUARIO" }}
    </h3>
    <label for="" style="color: #053F72" class="font-weight-bold lead">
        {{ $user->profesional ? $user->profesional->especialidad : "Especialista" }}
    </label>
</div>

<div class="row my-4">
    <div class="col-md-3">
        <a href="#" id="linkTraining">
            <label for="" class="lead font-weight-bold">ENTRENAMIENTOS</label>
        </a>
    </div>
    <div class="col-md-9" id="hrtitle">
        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
    </div>
</div>

<div class="row my-4">
    <div class="col-md-12">
        @if($productos->count() > 0)
            @include('layouts.carousel.carousel', ['productos' => $productos]);
        @else
            <h4>No hay productos registrados en el sistema</h4>
        @endif
    </div>
</div>

<div class="row my-4">
    <div class="col-md-12">
        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
    </div>
</div>
@endsection

<style>
    @media (max-width: 768px){
        #hrtitle {
            display: none
        }
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('#linkTraining').hover(function(){
            $(this).children().css("color", "#053F72");
            }, function(){
            $(this).children().css("color", "#AAAAAA");
        });
    });
</script>