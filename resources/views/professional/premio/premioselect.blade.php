@extends('layouts.professional.app')
@section('content')

<div class="row p-0 my-3 ">
    <div class="col ">
        <label for="" class="h2" style="color: #053F72"><b>Premios</b> - {{ $categoria->categoria }}</label>
    </div>
</div>

<!-- LISTADO DE PREMIOS -->
<div class="row-reverse ">
    <?php $cantidad = 0; $prem = $premios->count(); ?>
    @forelse($premios as $key => $premio)
        @if($cantidad > 1 || $key == 0)
            <div class="row p-0 m-0 mb-3">
        @endif
            <div class="col">
                <div class="row">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center premioSelected"
                        id="{{ $premio->id_premio }}">
                        <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class=" fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" 
                                id="itemDiv" style="width: 55px; height: 55px;">
                                <img src="{{ route('categoria.image', basename($premio->imagen)) }}" 
                                    style="width: 100%; height: 100%; border-radius: 60px" rel="nofollow" alt="...">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 px-2">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #053F72">
                                    {{ $premio->nombre }}
                                </label>
                                <label for="">
                                    {{ $premio->marca }}
                                </label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #053F72">
                                    {{ $premio->puntos }} Puntos
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @if($prem == $key+1 && $prem%2 != 0)<div class="col" ></div>@endif
        @if($cantidad == 1 || $prem == $key+1)
            </div>
        @endif
        <?php
            if($cantidad > 1) $cantidad = 0; 
            $cantidad++; 
        ?>
    @empty
        <div class="row p-0 m-0 mb-3">
            <div class="col" >
                <div class="row">
                    <div class="col-md-6 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #053F72">
                                    No hay premios disponibles en esta categoría por el momento.
                                </label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0"
                                    style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforelse
</div>
<!-- FIN LISTADO DE PREMIOS -->

<!-- CANJEAR / VER DETALLE PREMIO -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body m-0 p-0" style="padding-bottom: 25% !important;">
                <div class="row m-0 p-0">
                    <div class="col-md-3 p-0 m-0 d-flex justify-content-end align-items-center">
                        <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%); width: 250px; height:100px">
                                <a href="#" data-toggle="modal" data-target="#exampleModal2">
                                    <img src="" id="domImage" style="width: 100%; height: 100%; border-radius: 60px" rel="nofollow" alt="...">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label id="domName" class="h3 font-weight-bold" style="float: right; color: #053F72"></label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #AEDC5A">
                                    <span id="domPuntos"></span> Puntos
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="font-weight-bold">Entrega</label>
                    </div>
                    <div class="col">
                        <label id="domEntrega"></label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: #053F72">Información del producto</label>
                    </div>
                    <div class="col">
                        <label id="domDescripcion"></label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: #053F72">Características</label>
                    </div>
                    <div class="col ml-3">
                        <ul class="p-0 m-0" id="domCaracteristicas">
                            <li>Dos boletas de cinecolombia generales</li>
                            <li>No incluye alimentos</li>
                            <li>Vigencia de tres meses desde su obtención</li>
                        </ul>
                    </div>
                </div>
                <div class="row ">
                    <div id="domDiv" class="col d-flex justify-content-end" title="">
                        <a href="javascript:;" id="domModal" data-toggle="modal" data-target="">
                            <img src="{{url('img/CanjearPremio.png')}}" alt="" id="image" style="position: absolute; left: 69%; width: 30%; height: 80px">
                            <img src="{{url('img/CanjeadoPremio.png')}}" alt="" id="image2" style="display: none; position: absolute; left: 70%; width: 30%; height: 80px">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FORM DOMICILIO -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col">
                        <label for="" style="color: #053F72" class="font-weight-bold lead">CANJEAR PREMIO - FÍSICO</label>
                    </div>
                    <div class="col">
                        <label for="">Por favor registre y confirme los datos para poder canjear y entregar efectivamente tu premio:</label>
                    </div>
                </div>
                <div class="row-reverse my-3">
                    <form action="{{ route('premio_profesional.canjeo') }}" method="post" id="domForm">
                        @csrf
                        <div class="col">
                            <input required type="text" name="receptor" class="form-control" placeholder="NOMBRES Y APELLIDOS">
                        </div>
                        <div class="col my-2">
                            <input required type="number" name="telefono" class="form-control" placeholder="TELÉFONO">
                        </div>
                        <div class="col my-2">
                            <input required type="number" name="cedula" class="form-control" placeholder="CÉDULA">
                        </div>
                        <div class="col">
                            <input required type="text" name="direccion" placeholder="DIRECCIÓN" class="form-control">
                        </div>
                        <div class="col my-2">
                            <textarea required name="adicional" placeholder="INFORMACIÓN ADICIONAL" class="form-control" rows="3"></textarea>
                        </div>

                        <input type="hidden" name="id_premio" id="domIDPremio">
                        <input type="hidden" name="tipo" value="Domicilio">
                        <input class="d-none" type="number" name="cantidad" value="1">

                        <div id="domUploadStatus" style="display:none"></div>
                        <div class="col my-4 d-flex justify-content-center">
                            <button type="submit" class="btn text-white" aria-label="Close" style="background: #053F72; border-radius: 20px !important">CONFIRMAR</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FORM DIGITAL -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col">
                        <label for="" style="color: #053F72" class="font-weight-bold lead">CANJEAR PREMIO - DIGITAL</label>
                    </div>
                    <div class="col">
                        <label for="">Por favor registre y confirme los datos para poder canjear y entregar efectivamente tu premio:</label>
                    </div>
                </div>
                <div class="row-reverse my-3">
                    <form action="{{ route('premio_profesional.canjeo') }}" method="post" id="digForm">
                        @csrf
                        <div class="col">
                            <input type="email" name="correo" required class="form-control" placeholder="CORREO ELECTRÓNICO">
                        </div>
                        <div class="col my-2">
                            <input type="number" name="telefono" required class="form-control" placeholder="TELÉFONO">
                        </div>
                        <div class="col my-2">
                            <input type="number" name="cedula" required class="form-control" placeholder="CÉDULA">
                        </div>

                        <input type="hidden" name="id_premio" id="digIDPremio">
                        <input type="hidden" name="tipo" value="Online">
                        <input class="d-none" type="number" name="cantidad" value="1">

                        <div id="digUploadStatus" style="display:none"></div>
                        <div class="col my-4 d-flex justify-content-center">
                            <button type="submit" class="btn text-white" aria-label="Close" style="background: #053F72; border-radius: 20px !important">CONFIRMAR</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL FELICIDADES -->
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body m-0 p-0" style="padding-bottom: 5% !important;">
                <div class="row m-0 p-0">
                    <div class="col-md-3 p-0 m-0 d-flex justify-content-end align-items-center">
                        <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #053F72 100%); width: 250px; height:100px">
                                <a href="#">
                                    <img src="" id="felicitacionImage"  style="width: 100%; height: 100%; border-radius: 60px" rel="nofollow" alt="...">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label id="felicitacionName" class="h3 font-weight-bold" style="float: right; color: #053F72"></label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #AEDC5A">
                                    <span id="felicitacionPuntos"></span> Puntos
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-0 m-0">
                    <div class="col-4 ml-1">
                        <div class="row-reverse ">
                            <div class="col">
                                <label for="" class="font-weight-bold h4 p-0 m-0" style="color: #AEDC5A;"><i class="far fa-check-circle mr-1" style="color: #AEDC5A;"></i>CANJEADO</label>
                            </div>
                            <div class="col">
                                <label id="felicitacionFecha" class="p-0 m-0" style="color: #AEDC5A;"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <label for="" class="font-weight-bold h4" style="color: #053F72;">¡FELICIDADES <span id="felicitacionGanador"></span>!</label>
                            </div>
                            <div class="col">
                                <label for="" class="h5">Has canjeado <span id="felicitacionPuntos2"></span> puntos en uno de nuestros premios <span id="felicitacionCategoria"></span>.</label>
                            </div>
                            <div class="col my-2">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                            </div>
                            <div class="col">
                                <label for="">No olvides seguir haciendo los ejericios para acumular más putntos.</label>
                            </div>
                            <div class="col d-flex justify-content-end my-3">
                                <div class="row-reverse">
                                    <div class="col">
                                        <a class="btn text-white" data-dismiss="modal" aria-label="Close" style="background: #AEDC5A; border-radius: 20px !important">SEGUIR CANJEANDO</a>
                                    </div>
                                    <div class="col d-flex justify-content-end">
                                        <a href="{{ route('perfil_professional.index') }}">
                                            <label for="" style="text-decoration-line: underline; color: #AEDC5A; ">MI PROGRESO</label>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="urlModal" value="{{ route('premio_profesional.detail') }}">
<input type="hidden" id="urlImage" value="{{ route('categoria.image') }}">
@endsection

<style>
    img:hover {
        opacity: 0.5;
    }

    img{
        cursor: pointer;
    }

    .deshabilitar {
        pointer-events: none;
        cursor: default;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('.list-group-item').click(function() {
            $('.list-group-item').removeClass('active');
            $(this).closest('.list-group-item').addClass('active')
        });

        $('.premioSelected').click(function(){

            var id = $(this).attr('id');
            var url = $('#urlModal').val();
            
            $.ajax({
                url: url+'/'+id,
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content")
                },
                type: 'POST',
                success: function(response){
                    if(response.premio){
                        let premio = response.premio;
                        let src = $('#urlImage').val();
                        console.log(premio);
                        
                        //Arreglamos el Modal
                        $('#domImage').attr('src', src+'/'+premio.imagen);
                        $('#domName').text(premio.nombre);
                        $('#domPuntos').text(premio.puntos);
                        $('#domDescripcion').text(premio.descripcion);
                        $('#domEntrega').text(premio.entrega);
                        $('#domDiv').attr('title',premio.id_premio);
                        
                        $('#domCaracteristicas').empty();
                        if (premio.caracteristicas.length === 0){
                            html = "<li>No posee carácteristicas el premio...</li>";
                        }
                        else{
                            html = "";
                            premio.caracteristicas.forEach(function(element){
                                html += "<li>"+element.caracteristica+"</li>";
                            });
                        }
                        $("#domCaracteristicas").append(html);
                        
                        if(premio.entrega == "Domicilio")
                            $('#domModal').attr('data-target', '#exampleModal2');
                        else
                            $('#domModal').attr('data-target', '#exampleModal3');
                        
                        if(premio.puntos > response.user_puntos)
                            $('#domDiv').addClass('deshabilitar');
                        else
                            $('#domDiv').removeClass('deshabilitar');

                        $('#exampleModal').modal('show');
                    }
                    else{
                        Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'warning'
                        )
                    }
                }
            });
        });

        $('#domDiv').click(function(){
            var id = $(this).attr('title');
            $('#domIDPremio').val(id);
            $('#digIDPremio').val(id);
        });

        //Envio de canjear premio domicilio
        $('#domForm').on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');

            Swal.fire({
                title: 'Estas seguro de canjear el premio?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        type: type,
                        url: url,
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            jQuery('#domUploadStatus').hide();
                            jQuery('#domUploadStatus').html('');
                        },
                        error: function() {
                            $('#domUploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo enviar la solicitud de canjeo, error de conexión</strong></span>');
                        },
                        success: function(resp) {
                            console.log(resp);
                            jQuery('#domUploadStatus').show();

                            //Errores de los datos de validación
                            if(resp.errors){
                                jQuery.each(resp.errors, function(key, value) {
                                    jQuery('#domUploadStatus').append('<p class="mb-0"><span class="invalid-feedback d-block" role="alert"><strong>' + value + '</strong></span></p>');
                                });
                            }

                            //Todo fue de manera exitosa
                            if (resp.success) {

                                Swal.fire(
                                    'Premio Canjeado',
                                    resp.success,
                                    'success'
                                );

                                //Setteamos los valores del modal de exitoso
                                let premio = resp.premio;
                                let canjeado = resp.canjeado;
                                let src = $('#urlImage').val();

                                $('#felicitacionImage').attr('src', src+'/'+premio.imagen);
                                $('#felicitacionName').text(premio.nombre);
                                $('#felicitacionPuntos').text(premio.puntos);
                                $('#felicitacionGanador').text(resp.ganador);
                                $('#felicitacionPuntos2').text(premio.puntos);
                                $('#felicitacionCategoria').text(canjeado.categoria);
                                
                                let fecha = new Date(canjeado.created_at);
                                fecha = fecha.toISOString().split('T')[0];
                                $('#felicitacionFecha').text(fecha);
                                
                                //Cerramos modales y abrimos el que vamos a mostrar
                                $('#exampleModal').modal('hide');
                                $('#exampleModal2').modal('hide');
                                $('#exampleModal4').modal('show');            
                            }

                            //Error interno del servidor
                            if (resp.fallo){
                                Swal.fire(
                                    resp.message,
                                    resp.fallo,
                                    resp.status
                                )
                            }
                        },
                    });
                }
            })

        });

        //Envio de canjear premio online
        $('#digForm').on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');

            Swal.fire({
                title: 'Estas seguro de canjear el premio?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        type: type,
                        url: url,
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            jQuery('#digUploadStatus').hide();
                            jQuery('#digUploadStatus').html('');
                        },
                        error: function() {
                            $('#digUploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo enviar la solicitud de canjeo, error de conexión</strong></span>');
                        },
                        success: function(resp) {
                            console.log(resp);
                            jQuery('#digUploadStatus').show();

                            //Errores de los datos de validación
                            if(resp.errors){
                                jQuery.each(resp.errors, function(key, value) {
                                    jQuery('#digUploadStatus').append('<p class="mb-0"><span class="invalid-feedback d-block" role="alert"><strong>' + value + '</strong></span></p>');
                                });
                            }

                            //Todo fue de manera exitosa
                            if (resp.success) {

                                Swal.fire(
                                    'Premio Canjeado',
                                    resp.success,
                                    'success'
                                );

                                //Setteamos los valores del modal de exitoso
                                let premio = resp.premio;
                                let canjeado = resp.canjeado;
                                let src = $('#urlImage').val();

                                $('#felicitacionImage').attr('src', src+'/'+premio.imagen);
                                $('#felicitacionName').text(premio.nombre);
                                $('#felicitacionPuntos').text(premio.puntos);
                                $('#felicitacionGanador').text(resp.ganador);
                                $('#felicitacionPuntos2').text(premio.puntos);
                                $('#felicitacionCategoria').text(canjeado.categoria);
                                
                                let fecha = new Date(canjeado.created_at);
                                fecha = fecha.toISOString().split('T')[0];
                                $('#felicitacionFecha').text(fecha);
                                
                                //Cerramos modales y abrimos el que vamos a mostrar
                                $('#exampleModal').modal('hide');
                                $('#exampleModal2').modal('hide');
                                $('#exampleModal4').modal('show');            
                            }

                            //Error interno del servidor
                            if (resp.fallo){
                                Swal.fire(
                                    resp.message,
                                    resp.fallo,
                                    resp.status
                                )
                            }
                        },
                    });
                }
            })

        });
    });
</script>