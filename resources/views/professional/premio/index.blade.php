@extends('layouts.professional.app')
@section('content')
    <div class="row p-0 my-3 ">
        <div class="col ">
            <label for="" class="h2 font-weight-bold" style="color: #053F72">Premios</label>
        </div>
    </div>

    <div class="row-reverse ">
        <?php $cantidad = 0; $cat = $categorias->count(); ?>
        @forelse($categorias as $key => $categoria)
            @if($cantidad > 1 || $key == 0)
                <div class="row p-0 m-0">
            @endif
                <!-- ESPACIO DE CATEGORIAS -->
                <div class="col" >
                    <a href="{{ route('premio_profesional.select', $categoria->id_premio_cat) }}">
                        <div class="row">
                            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                                <div class="fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        style="width: 60px; height: 60px;">
                                        <img src="{{ route('categoria.image', basename($categoria->imagen)) }}" 
                                            style="width: 100%; height: 100%; border-radius: 60px" rel="nofollow" alt="...">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 p-0 m-0">
                                <div class="row-reverse p-0 m-0">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h3 font-weight-bold" style="float: right; color: #053F72">
                                            {{ $categoria->categoria }}
                                        </label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>
                </div>
                <!-- FIN ESPACIO DE CATEGORIAS -->
                @if($cat == $key+1 && $cat%2 != 0)<div class="col" ></div>@endif
            @if($cantidad == 1 || $cat == $key+1)
                </div>
            @endif
            <?php
                if($cantidad > 1) $cantidad = 0; 
                $cantidad++; 
            ?>
        @empty
            <div class="row p-0 m-0">
                <div class="col" >
                    <div class="row">
                        <div class="col-md-4 p-0 m-0">
                            <div class="row-reverse p-0 m-0">
                                <div class="col m-0 p-0">
                                    <label for="" class="h3 font-weight-bold" style="float: right; color: #053F72">
                                        No hay premios/categorias disponibles por el momento.
                                    </label>
                                </div>
                                <div class="col m-0 p-0">
                                    <hr class="m-0 p-0"
                                        style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforelse
    </div>
@endsection