@extends('layouts.patient.app')
@section('content')

<div class="row">
    <div class="col-12 text-center">
        @if (Session::has('message'))
            <div class="alert alert-success" style="padding: 10px;">
                {{ Session::get('message') }}
            </div>
        @endif
    </div>

    <div class="col mb-5">
        <div class="row">
            <div class="col-md-2 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white " style="width: 200px; height: 100px" >
                        <a href="{{route('profile_patient.index')}}">
                            <img src="{{ route('user.icon', basename($user->avatar)) }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="...">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p-0 m-0">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label for="" class="h3 font-weight-bold" style="float: right; color: #053F72">{{ $user->total_puntos }} puntos</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: blue; float: left">
                    </div>
                    <div class="col m-0 p-0">
                        <label for="" class="font-weight-bold" style="float: right; color: #053F72">AGREGAR PROGRESO</label>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div class="row-reverse mt-5 pl-4">
    <div class="col">
        <label for="" class="h2 font-weight-bold" style="color: #669D00">{{ $user->paciente ? $user->paciente->nombre . ' ' . $user->paciente->apellido : "USUARIO" }}</label>
    </div>
    <div class="col">
        <label for="" class="h3" style="color: #669D00">
            @if($user->paciente)
                @foreach($user->paciente->patologias->take(1) as $pat)
                    {{ $pat->patologia->nombre." / " }}
                @endforeach
            @else
                No posee alguna Patología
            @endif
        </label>
    </div>
    <div class="col">
        <label for="" class="h4" style="color: #669D00">
            <!-- N. 1234-BC -->
            N. {{ $user->paciente->identificacion }}
        </label>
    </div>
</div>


@endsection