@extends('layouts.patient.app')
@section('content')


<div class="row-reverse">
    <div class="col">
        <label for="" class="h2 font-weight-bold m-0 p-0" style="color: #053F72">
            Retos
        </label>
    </div>
</div>

@if (Session::has('message'))
    <div class="row mx-auto mt-2">
        <div class="col-md-12 alert alert-success" style="padding: 10px;">
            {{ Session::get('message') }}
        </div>
    </div>
@endif

<div class="row mx-auto mb-5 mt-2">
    <div class="col-md-8 " >
        <h3>{{ $capacitacion->titulo }}</h3>
        <label for="" class="lead">
            {{ $capacitacion->descripcion }}
        </label>
    </div>
</div>
<div class="row ">
    <div class="col-md-5 ">
        <!-- ESPACIO DE LOS RETOS -->
            @foreach($capacitacion->entrenamientos as $key => $entrenamiento)
                <div class="row mx-auto">
                    <div class="d-none docsKey">
                        <input type="hidden" id="docID{{ $key }}" value="{{ basename($entrenamiento->id_capacitacion_documento) }}">
                        <input type="hidden" id="pdf{{ $key }}" value="{{ basename($entrenamiento->documento) }}">
                        <input type="hidden" id="video{{ $key }}" value="{{ basename($entrenamiento->video) }}">
                    </div>
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center itemDiv" 
                                 style="width: 200px; height: 100px;">
                                <a href="javascript:;">
                                    <i class="fas fa-stethoscope iconDiv" style="font-size: 60px; color: #669D00;" id="{{ $key }}"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #669D00">RETO {{ $key+1 }}</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #669D00; float: left">
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        <!-- FIN ESPACIO DE LOS RETOS -->

        @if($capacitacion->cuestionario)
            <div class="row mx-auto ">
                <a href="{{route('cuestionario_patient.index', $capacitacion->id_capacitacion)}}">
                    <label for="" class="lead font-weight-bold my-5 " style="text-decoration-line: underline; color: #669D00; cursor: pointer">HACER EL CUESTIONARIO</label>
                </a>
            </div>
        @endif
    </div>
    @if($capacitacion->cuestionario)
        <div class="col-md-3 d-flex align-items-center">
            <div class="col">
                <a href="{{route('cuestionario_patient.index', $capacitacion->id_capacitacion)}}" class="btn rounded" style="background: #053F72">SIGUIENTE</a>
            </div>
        </div>
    @endif
</div>

<!-- MODAL QUE MUESTRA LAS OPCIONES DE VER EL VIDEO O PDF -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-left h3 font-weight-bold mx-auto" style="color: #669D00" id="exampleModalLabel">RETO <span id="retoID"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mx-auto">
              <div class="card col-md-5 mx-auto p-3 pdfArchive" style="box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%) !important">
                <input type="hidden" id="pdfUrl" value="">
                <img src="{{ url('img/pdf.png') }}" style="width: 90%; height: 100%" rel="nofollow" alt="...">
              </div>
              <div class="card col-md-5 mx-auto p-3 videoArchive" style="box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%) !important">
                <input type="hidden" id="videoUrl" value="">
                <img src="{{ url('img/video.png') }}" style="width: 90%; height: 100%" rel="nofollow" alt="...">
              </div>
          </div>
          <!-- <div class="row mx-auto my-3">
              <a href="javascript:;" onclick="windows()" id="clickCuest" class="mx-auto font-weight-bold" style="text-decoration-line: underline; color: #669D00">
                HACER EL CUESTIONARIO
              </a>
              <a href="javascript:;" onclick="exitmodal()" data-dismiss="modal" id="next" class="mx-auto font-weight-bold" style="text-decoration-line: underline; color: #669D00; display: none">
                  FINALIZAR
              </a>
          </div> -->
        </div>

      </div>
    </div>
</div>
<!-- FIN MODAL -->
<input type="hidden" id="controllerUrl" value="{{ route('retos_patient.archive') }}">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    function windows() {
        window.open("https://www.proturbiomarspa.com/files/_pdf-prueba.pdf", "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
        $('#next').css({
            'display' : 'block'
        })
        $('#clickCuest').css({
            'display' : 'none'
        })
    }

    function exitmodal(){
        $('#next').css({
            'display' : 'none'
        })
        $('#clickCuest').css({
            'display' : 'block'
        })
    }
</script>

<!-- ACTIVAR CADA RETO INDIVIDUALMENTE -->
<script>
    $(document).ready(function() {
        $('.iconDiv').click(function(){
            $('.iconDiv').css({'color' : '#669D00'});
            $('.itemDiv').css({'background' : 'white'});

            //AHORA ACOMODO EL QUE ES
            $(this).css({'color' : 'white'});
            $(this).closest('.itemDiv').css({'background' : '#669D00'});

            //Cogemos los números o valores y abrimos el modal
            var id = parseInt($(this).attr('id'));
            var pdf = $('#pdf'+id).val();
            var video = $('#video'+id).val();
            var doc = $('#docID'+id).val();
            console.log(id);
            console.log(pdf);
            console.log(video);
            console.log(doc);

            $('#retoID').text(id+1);
            $('#pdfUrl').attr('name',doc).val(pdf);
            $('#videoUrl').attr('name',doc).val(video);

            $('#exampleModal').modal('show');
        }); 

        $('.pdfArchive').click(function(){
            var url = $('#controllerUrl').val();
            var id = $('#pdfUrl').attr('name');
            var pdf = $('#pdfUrl').val();
            window.open(url+"/"+pdf+"/"+id, "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000");
        });
        
        $('.videoArchive').click(function(){
            var url = $('#controllerUrl').val();
            var id = $('#videoUrl').attr('name');
            var video = $('#videoUrl').val();
            window.open(url+"/"+video+"/"+id, "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000");
        });
    });
</script>
@endsection