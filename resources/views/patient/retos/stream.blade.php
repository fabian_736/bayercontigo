<style>
    .video {
        margin: 0;
        width: 100%;
        height: 0;
        padding-top: 56.25%;
        background-color: #ccc;
        position: relative;
    }
    .video iframe {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
    }
</style>

<?php $url = env('WEB_PANEL_URL')."capacitacion/getVideo/".$response ?>

<figure class="video">
    <iframe src="{{ $url }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>