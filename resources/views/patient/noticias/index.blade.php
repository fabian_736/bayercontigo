@extends('layouts.patient.app')
@section('content')

<!-- Header Noticias -->
    <div class="row p-3">
        <div class="col">
            <label for="" class="font-weight-bold h2" style="color: #053F72">Noticias</label>
        </div>
    </div>
<!-- Fin Header -->

<div class="row">
    <!-- Seleccionador de tipo de noticias -->
        <div class="col-3 d-flex justify-content-end mt-auto" style="width: 100%; max-height: 800px">
            <div class="row-reverse ">
                <div class="col">
                    <label for="" class="lead" onclick="recetas()" id="titlerecetas" style="color: #669D00; cursor: pointer">Recetas</label>
                </div>
                <div class="col">
                    <label for="" class="lead"  onclick="moda()" id="titlemoda" style="color: #669D00; cursor: pointer">Tips de Moda</label>
                </div> 
                <div class="col">
                    <label for="" class="lead" onclick="salud()" id="titlesalud" style="color: #669D00; cursor: pointer">Salud</label>
                </div>
            </div>
        </div>
    <!-- Fin seleccionado de tipo de noticias -->

    <div class="col-9 p-3 m-0 card" id="cardcontainer" style="width: 100%; max-height: 800px; overflow-y: scroll;">
        <!-- RECETAS -->
            <div id="recetas">
                @forelse($recetas as $receta)
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="{{ route('noticia.image', basename($receta->imagen)) }}"
                                        alt="" width="100%" height="300">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #669D00;" class="lead font-weight-bold">
                                                {{ $receta->titulo }}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="">
                                                {{ $receta->descripcion }}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">{{ $receta->categoria }}</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">

                                            <!-- SECCIÓN DE LOS LIKES -->
                                            <?php $user_like = false; ?>
                                            @foreach($receta->likes as $like)
                                                @if($like->FK_id_user == Auth::user()->id)
                                                    <?php $user_like = true; ?>
                                                @endif
                                            @endforeach

                                            <div class="col">
                                                @if($user_like)
                                                    <i id="{{$receta->id_noticia}}" class="fas fa-thumbs-up btn-dislike" style="font-size: 20px; color: #053F72"></i>
                                                @else
                                                    <i id="{{$receta->id_noticia}}" class="far fa-thumbs-up btn-like" style="font-size: 20px; color: #053F72"></i>
                                                @endif
                                                <span id="thumbCount{{$receta->id_noticia}}" class="font-weight-bold">{{ $receta->likes->count() }}</span>
                                            </div>

                                            <div class="col">
                                                <a class="btn font-weight-bold recetaModal" id="{{ $receta->id_noticia }}"
                                                    style="background: #C0E47F; color: #053F72">
                                                    VER MÁS
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                    <label for="" style="color: #669D00;" class="lead font-weight-bold">No hay noticias tipo receta</label>
                @endforelse
            </div>
        <!-- FIN RECETAS -->

        <!-- MODA -->
            <div id="moda" style="display: none">
                @forelse($modas as $moda)
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="{{ route('noticia.image', basename($moda->imagen)) }}"
                                        alt="" width="100%" height="300">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #669D00;" class="lead font-weight-bold">
                                                {{ $moda->titulo }}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="">
                                                {{ $moda->descripcion }}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">
                                                {{ $moda->categoria }}
                                            </label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">

                                            <!-- SECCIÓN DE LOS LIKES -->
                                            <?php $user_like = false; ?>
                                            @foreach($moda->likes as $like)
                                                @if($like->FK_id_user == Auth::user()->id)
                                                    <?php $user_like = true; ?>
                                                @endif
                                            @endforeach

                                            <div class="col">
                                                @if($user_like)
                                                    <i id="{{$moda->id_noticia}}" class="fas fa-thumbs-up btn-dislike" style="font-size: 20px; color: #053F72"></i>
                                                @else
                                                    <i id="{{$moda->id_noticia}}" class="far fa-thumbs-up btn-like" style="font-size: 20px; color: #053F72"></i>
                                                @endif
                                                <span id="thumbCount{{$moda->id_noticia}}" class="font-weight-bold">{{ $moda->likes->count() }}</span>
                                            </div>

                                            <div class="col">
                                                <a class="btn font-weight-bold modaModal" id="{{ $moda->id_noticia }}"
                                                    style="background: #C0E47F; color: #053F72">
                                                    VER MÁS
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                    <label for="" style="color: #669D00;" class="lead font-weight-bold">No hay noticias tipo moda</label>
                @endforelse
            </div>
        <!-- FIN MODA -->

        <!-- SALUD -->
            <div id="salud" style="display: none">
                @forelse($saludes as $salud)
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="{{ route('noticia.image', basename($salud->imagen)) }}"
                                        alt="" width="100%" height="300">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #669D00;" class="lead font-weight-bold">
                                                {{ $salud->titulo }}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="">
                                                {{ $salud->descripcion }}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">{{ $salud->categoria }}</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">

                                            <!-- SECCIÓN DE LOS LIKES -->
                                            <?php $user_like = false; ?>
                                            @foreach($salud->likes as $like)
                                                @if($like->FK_id_user == Auth::user()->id)
                                                    <?php $user_like = true; ?>
                                                @endif
                                            @endforeach

                                            <div class="col">
                                                @if($user_like)
                                                    <i id="{{$salud->id_noticia}}" class="fas fa-thumbs-up btn-dislike" style="font-size: 20px; color: #053F72"></i>
                                                @else
                                                    <i id="{{$salud->id_noticia}}" class="far fa-thumbs-up btn-like" style="font-size: 20px; color: #053F72"></i>
                                                @endif
                                                <span id="thumbCount{{$salud->id_noticia}}" class="font-weight-bold">{{ $salud->likes->count() }}</span>
                                            </div>

                                            <div class="col">
                                                <a class="btn font-weight-bold saludModal" id="{{ $salud->id_noticia }}"
                                                    style="background: #C0E47F; color: #053F72">
                                                    VER MÁS
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                    <label for="" style="color: #669D00;" class="lead font-weight-bold">No hay noticias tipo salud</label>
                @endforelse
            </div>
        <!-- FIN SALUD -->
    </div>
</div>

<!-- ruta del detail y dar like-->
<input type="hidden" id="getDetail" value="{{ route('noticias_patient.detail') }}">
<input type="hidden" id="getDocument" value="{{ route('noticia.document') }}">
<input type="hidden" id="getVideo" value="{{ route('noticia.video') }}">
<input type="hidden" id="URLPATH" value="{{ route('noticias_patient.like.url') }}">

<!-- Modal para ver el detalle de la receta -->
<div class="modal fade" id="recetaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closereceta">
                    <span aria-hidden="true">&times;</span>
                    <script>
                        $('#closereceta').click(function() {
                            $('#recetaModal').modal('hide');
                        });
                    </script>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div style="cursor: pointer; border-radius: 110px; width: 210px; height: 210px; border-width: 1px; border-style: solid; border-color: radial-gradient(64.71% 64.71% at 78.25% 81.92%, #669D00 0%, #02718B 100%);"
                            class="d-flex justify-content-center align-items-center openDoc" id="recetaDocument">
                            <div style="width: 200px; height: 200px; border-radius: 100px; background: radial-gradient(64.71% 64.71% at 78.25% 81.92%, #669D00 0%, #02718B 100%);"
                                class="d-flex justify-content-center align-items-center">
                                <i class="fas" id="recetaIcon" style="font-size: 40px; color: white"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <label id="recetaTitulo" class="h3 font-weight-bold" style="color:#669D00"></label>
                            </div>
                            <div class="col">
                                <label for="">INGREDIENTES</label>
                            </div>
                            <div class="col p-0">
                                <ul id="recetaIngrediente">
                                    <!-- <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el detalle del tip de moda -->
<div class="modal fade" id="modaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closemoda">
                    <span aria-hidden="true">&times;</span>
                    <script>
                        $('#closemoda').click(function() {
                            $('#modaModal').modal('hide');
                        });
                    </script>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div style="cursor: pointer; border-radius: 110px; width: 210px; height: 210px; border-width: 1px; border-style: solid; border-color: radial-gradient(64.71% 64.71% at 78.25% 81.92%, #669D00 0%, #02718B 100%);"
                            class="d-flex justify-content-center align-items-center openDoc" id="modaDocument">
                            <div style="width: 200px; height: 200px; border-radius: 100px; background: radial-gradient(64.71% 64.71% at 78.25% 81.92%, #669D00 0%, #02718B 100%);"
                                class="d-flex justify-content-center align-items-center">
                                <i class="fas" id="modaIcon" style="font-size: 40px; color: white"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <label id="modaTitulo" class="h3 font-weight-bold" style="color:#669D00"></label>
                            </div>
                            <div class="col">
                                <label for="" id="modaConcepto"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el detalle de la salud -->
<div class="modal fade" id="saludModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closesalud">
                    <span aria-hidden="true">&times;</span>
                    <script>
                        $('#closesalud').click(function() {
                            $('#saludModal').modal('hide');
                        });
                    </script>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col saludDiv"></div>
                    <div class="col-3 saludDiv">
                        <div style="cursor: pointer; border-radius: 110px; width: 210px; height: 210px; border-width: 1px; border-style: solid; border-color: radial-gradient(64.71% 64.71% at 78.25% 81.92%, #669D00 0%, #02718B 100%);"
                            class="d-flex justify-content-center align-items-center openDoc" id="saludDocument">
                            <div style="width: 200px; height: 200px; border-radius: 100px; background: radial-gradient(64.71% 64.71% at 78.25% 81.92%, #669D00 0%, #02718B 100%);"
                                class="d-flex justify-content-center align-items-center">
                                <i class="fas" id="saludIcon" style="font-size: 40px; color: white"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col saludDiv"></div>
                    <div class="col-7">
                        <div class="row-reverse">
                            <div class="col">
                                <label id="saludTitulo" class="h3 font-weight-bold" style="color:#669D00"></label>
                            </div>
                            <div class="col">
                                <label for="" id="saludConcepto"></label>
                            </div>
                            <div id="saludDirectorio">
                                <!-- <div class="col">
                                    <label class="h3 font-weight-bold" style="color:#669D00">A</label>
                                    <ul>
                                        <li>Especialidad</li>
                                        <li>Especialidad</li>
                                        <li>Especialidad</li>
                                        <li>Especialidad</li>
                                    </ul>
                                    <hr>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    @media only screen and (max-width: 750px) and (min-width: 321px) {
        #cardcontainer {
            max-height: 200px !important;
        }

        #colimg {
            display: none;
        }
    }

    #cardcontainer::-webkit-scrollbar {
        width: 8px;
        /* Tamaño del scroll en vertical */
        height: 8px;
        /* Tamaño del scroll en horizontal */
    }

    #cardcontainer::-webkit-scrollbar-thumb {
        background: #ccc;
        border-radius: 4px;
    }

    /* Cambiamos el fondo y agregamos una sombra cuando esté en hover */
    #cardcontainer::-webkit-scrollbar-thumb:hover {
        background: #b3b3b3;
        box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
    }

    /* Cambiamos el fondo cuando esté en active */
    #cardcontainer::-webkit-scrollbar-thumb:active {
        background-color: #999999;
    }

    #cardcontainer::-webkit-scrollbar-track {
        background: #e1e1e1;
        border-radius: 4px;
    }

    /* Cambiamos el fondo cuando esté en active o hover */
    #cardcontainer::-webkit-scrollbar-track:hover,
    #cardcontainer::-webkit-scrollbar-track:active {
        background: #d4d4d4;
    }

</style>

<script>
    function recetas(){
        $('#recetas').css({
            'display' : 'block'
        })
        $('#moda').css({
            'display':'none'
        })
        $('#salud').css({
            'display':'none'
        })
        $('#titlerecetas').css({
            'font-weight':'bold'
        })
        $('#titlemoda').css({
            'font-weight':'normal'
        })
        $('#titlesalud').css({
            'font-weight':'normal'
        })
    }

    function moda(){
        $('#recetas').css({
            'display' : 'none'
        })
        $('#moda').css({
            'display':'block'
        })
        $('#salud').css({
            'display':'none'
        })
        $('#titlerecetas').css({
            'font-weight':'normal'
        })
        $('#titlemoda').css({
            'font-weight':'bold'
        })
        $('#titlesalud').css({
            'font-weight':'normal'
        })
    }

    function salud(){
        $('#salud').css({
            'display' : 'block'
        })
        $('#recetas').css({
            'display':'none'
        })
        $('#moda').css({
            'display':'none'
        })
        $('#titlerecetas').css({
            'font-weight':'normal'
        })
        $('#titlemoda').css({
            'font-weight':'normal'
        })
        $('#titlesalud').css({
            'font-weight':'bold'
        })
    }
</script>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="{{ asset('js/like.js') }}"></script>
<script>
    $(document).ready(function() {

        //abrir la venta del documento de la noticia (video | imagen | pdf)
        $('.openDoc').click(function(){
            let document = $(this).attr('name');
            let extension = document.split(".").pop();
            let urlDoc = $('#getDocument').val()+"/"+document;
            let urlVid = $('#getVideo').val()+"/"+document;
            
            if(extension == "mp4"){
                window.open(urlVid, "ventana1",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000");
            }
            else{
                window.open(urlDoc, "ventana1",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000");
            }
        });

        //abrir modal de la receta
        $('.recetaModal').click(function(){
            var id = $(this).attr('id');
            var urlNoticia = $('#getDetail').val()+"/"+id;
            
            $.ajax({
                url: urlNoticia,
                type: 'GET',
                success: function(response){
                    if(response.noticia){
                        $('#recetaTitulo').text(response.noticia.titulo);
                        if(response.noticia.documento){
                            $('#recetaDocument').parent().removeClass('d-none');
                            $('#recetaDocument').attr('name', response.noticia.documento);

                            let extension = response.noticia.documento.split(".").pop();
                            
                            $('#recetaIcon').removeClass('fa-play');
                            $('#recetaIcon').removeClass('fa-file-pdf');
                            $('#recetaIcon').removeClass('fa-image');

                            if(extension == "mp4") $('#recetaIcon').addClass('fa-play');
                            else if(extension == "pdf") $('#recetaIcon').addClass('fa-file-pdf');
                            else $('#recetaIcon').addClass('fa-image');
                        }
                        else
                            $('#recetaDocument').parent().addClass('d-none');

                        $('#recetaIngrediente').empty();
                        response.items.forEach(function(element){
                            console.log(element.item);
                            var html = "<li>"+element.item+"</li>";
                            $("#recetaIngrediente").append(html);
                        });
                        $('#recetaModal').modal('show');
                    }
                    else{
                        Swal.fire({
                            text: response.message,
                            icon: 'warning',
                            allowOutsideClick: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Aceptar'
                        })
                    }
                }
            });
        });

        //abrir modal del tip de moda
        $('.modaModal').click(function(){
            var id = $(this).attr('id');
            var urlNoticia = $('#getDetail').val()+"/"+id;
            
            $.ajax({
                url: urlNoticia,
                type: 'GET',
                success: function(response){
                    if(response.noticia){
                        $('#modaTitulo').text(response.noticia.titulo);
                        if(response.noticia.documento){
                            $('#modaDocument').parent().removeClass('d-none');
                            $('#modaDocument').attr('name', response.noticia.documento);

                            let extension = response.noticia.documento.split(".").pop();
                            
                            $('#modaIcon').removeClass('fa-play');
                            $('#modaIcon').removeClass('fa-file-pdf');
                            $('#modaIcon').removeClass('fa-image');

                            if(extension == "mp4") $('#modaIcon').addClass('fa-play');
                            else if(extension == "pdf") $('#modaIcon').addClass('fa-file-pdf');
                            else $('#modaIcon').addClass('fa-image');
                        }
                        else
                            $('#modaDocument').parent().addClass('d-none');

                        $('#modaConcepto').text(response.noticia.contenido);
                        $('#modaModal').modal('show');
                    }
                    else{
                        Swal.fire({
                            text: response.message,
                            icon: 'warning',
                            allowOutsideClick: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Aceptar'
                        })
                    }
                }
            });
        });

        //abrir modal de salud
        $('.saludModal').click(function(){
            var id = $(this).attr('id');
            var urlNoticia = $('#getDetail').val()+"/"+id;
            
            $.ajax({
                url: urlNoticia,
                type: 'GET',
                success: function(response){
                    if(response.noticia){
                        $('#saludTitulo').text(response.noticia.titulo);
                        if(response.noticia.documento){
                            //$('#saludDocument').parent().removeClass('d-none');
                            $('.saludDiv').removeClass('d-none');
                            $('#saludDocument').attr('name', response.noticia.documento);

                            let extension = response.noticia.documento.split(".").pop();
                            
                            $('#saludIcon').removeClass('fa-play');
                            $('#saludIcon').removeClass('fa-file-pdf');
                            $('#saludIcon').removeClass('fa-image');

                            if(extension == "mp4") $('#saludIcon').addClass('fa-play');
                            else if(extension == "pdf") $('#saludIcon').addClass('fa-file-pdf');
                            else $('#saludIcon').addClass('fa-image');
                        }
                        else{
                            //$('#saludDocument').parent().addClass('d-none');
                            $('.saludDiv').addClass('d-none');
                        }

                        $('#saludConcepto').text(response.noticia.contenido);
                        
                        $('#saludDirectorio').empty();
                        response.directorio.forEach(function(element){
                            //console.log(element);
                            var html = '<div class="col">'+
                                            '<label class="h3 font-weight-bold" style="color:#669D00">'+element.profesional.nombre+' '+element.profesional.apellido+'</label>'+
                                            '<ul>'+
                                                '<li>Especialidad: '+element.profesional.especialidad+'</li>'+
                                                '<li>Teléfono: '+element.profesional.telefono+'</li>'+
                                                '<li>Dirección: '+element.profesional.user.pais+', '+element.profesional.direccion+'</li>'+
                                                '<li>Correo: '+element.profesional.user.email+'</li>'+
                                            '</ul>'+
                                            '<hr>'+
                                        '</div>';
                            $("#saludDirectorio").append(html);
                        });

                        $('#saludModal').modal('show');
                    }
                    else{
                        Swal.fire({
                            text: response.message,
                            icon: 'warning',
                            allowOutsideClick: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Aceptar'
                        })
                    }
                }
            });
        });

    });
</script>