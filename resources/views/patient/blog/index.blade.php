@extends('layouts.patient.app')
@section('content')

<div class="col-12 text-center">
    @if (Session::has('create'))
        <div class="alert alert-success" style="padding: 10px;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div class="alert alert-danger" style="padding: 10px;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger" style="padding: 10px;">
            "El comentario no puede tener una longitud de 255 carácteres"
        </div>
    @endif
</div>

<div class="row-reverse p-0 m-0">
    <div class="col p-0 m-0">
        <label for="" class="h3 font-weight-bold" style="color: #053F72">Blog</label>
    </div>
    <div class="col p-0 m-0">
        <!-- <label for="" class="lead font-weight-bold" style="color: #053F72">Tema del mes</label> -->
    </div>
</div>

<div class="row-reverse">
    <!-- TRENDING ACTUAL POR LIKES -->
        <?php $id_trending = null ?>
        @if($trending->cantidad > 0)
            <?php $id_trending = $trending->id_muro ?>
            <div class="col">
                <div class="card p-3 mx-auto">
                    <div class="row">
                        <!-- ICONO DEL QUE HIZO EL POST -->
                        <div class="col-md-2 d-flex justify-content-center">
                            <div class="fileinput fileinput-new text-center " data-provides="fileinput">
                                <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="width:300px; height:100px">
                                    <!-- <a href="{{ route('perfil_professional.index') }}"> -->
                                    <a href="#">
                                        <img src="{{ route('user.icon', basename($trending->creador->avatar)) }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="...">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <!-- INFORMACIÓN DEL POST -->
                                <div class="row d-flex justify-content-between">
                                    <div class="col">
                                        <label for="" class="lead font-weight-bold" style="color: #053F72">{{ $trending->creador->nombre." ".$trending->creador->apellido }}</label>
                                    </div>
                                    <div class="col d-flex justify-content-end">
                                        <label for="" class="font-weight-bold" style="color: #053F72">Tema del mes</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="">{{ $trending->contenido }}</label>
                                    </div>
                                </div>
                                @if($trending->imagen)
                                    <div class="row">
                                        <div class="col">
                                            <img src="{{ route('muro.image', basename($trending->imagen)) }}" 
                                                class="w-100" height="300" alt="">
                                        </div>
                                    </div>
                                @endif
                            <!-- FIN INFORMACION DEL POST -->

                            <!-- CHAT DEL POST -->
                                <div class="row">
                                    <div class="col card" id="cardcontainer" style="height: 100px; overflow-y: scroll;">
                                        <div id="chat_converse" class="chat_converse">
                                            @foreach($trending->comments as $comentario)
                                                <div class="comentario my-1" id="row{{ $comentario->id_muro_comentario }}">
                                                    <h5 class="mb-0">
                                                        @if( $comentario->creadorUser )
                                                            @if($comentario->creadorUser->tipo == 'paciente')
                                                                {{
                                                                    $comentario->creadorUser->paciente ? 
                                                                    $comentario->creadorUser->paciente->nombre . ' ' . $comentario->creadorUser->paciente->apellido : "" 
                                                                }}
                                                            @else
                                                                {{
                                                                    $comentario->creadorUser->profesional ? 
                                                                    $comentario->creadorUser->profesional->nombre . ' ' . $comentario->creadorUser->profesional->apellido : ""
                                                                }}
                                                            @endif
                                                        @else
                                                            {{ $comentario->creadorAdmin->nombre." ".$comentario->creadorAdmin->apellido }}
                                                        @endif
                                                    </h5>
                                                    <label for="">{{ $comentario->comentario }}</label> 
                                                    @if($comentario->FK_id_user == Auth::user()->id)
                                                        <a id="{{ route('blog_patient.comment.delete', $comentario->id_muro_comentario) }}" 
                                                            type="{{ $comentario->id_muro_comentario }}" class="deleteComment">
                                                            <button class="btn btn-sm" style="background-color: #669D00;">
                                                                <i class="far fa-trash-alt"></i>
                                                            </button>
                                                        </a>
                                                    @endif
                                                    <hr>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            <!-- FIN DEL CHAT -->

                            <!-- FORM PARA ENVIAR UN COMENTARIO -->
                                <div class="row mx-auto">
                                    <form method="post" action="{{ route('blog_patient.comment.store', $trending->id_muro ) }}" class="row container-fluid">
                                        @csrf
                                        <div class="col-md-9 m-0 p-0">
                                            <textarea id="chatSend" name="comentario" cols="30" rows="10" required
                                                    class="form-control" placeholder="Escribe un comentario..." 
                                                    style="width: 100%; height: 40px !important; background: #F5F5F5; border-radius: 20px; padding: 10px"
                                            ></textarea>
                                        </div>
                                        <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-center align-items-center" style="cursor: pointer; width: 40px; height: 40px; border-radius: 60px; background: #053F72">
                                            <button type="submit">
                                                <i class="fas fa-paper-plane" style="font-size: 20px; color: white; "></i>
                                            </button>
                                        </div>

                                        <!-- SECCIÓN DE LOS LIKES -->
                                        <?php $user_like = false; ?>
                                        @foreach($trending->likes as $like)
                                            @if($like->FK_id_user == Auth::user()->id)
                                                <?php $user_like = true; ?>
                                            @endif
                                        @endforeach
                                        <div class="col-md-1 col-sm-12 col-xs-12 px-2 m-0 d-flex justify-content-start align-items-center" style="cursor: pointer;">
                                            @if($user_like)
                                                <i id="{{$trending->id_muro}}" class="fas fa-thumbs-up btn-dislike" style="color: #669D00; font-size: 20px" ></i>
                                            @else
                                                <i id="{{$trending->id_muro}}" class="far fa-thumbs-up btn-like" style="font-size: 20px; color: #669D00"></i>
                                            @endif
                                            <span id="thumbCount{{$trending->id_muro}}" class="font-weight-bold px-1">{{ $trending->likes->count() }}</span>
                                        </div>
                                    </form>
                                </div>
                            <!-- FIN FORM PARA ENVIAR UN COMENTARIO -->
                        </div>
                    </div>
                </div>
            </div>
        @endif
    <!-- TRENDING ACTUAL POR LIKES -->
    
    <!-- RESTO DE LOS COMENTARIOS -->
    @foreach($muros as $muro)
        @if($muro->id_muro == $id_trending) @continue @endif
        <div class="col">
            <div class="card p-3 mx-auto">
                <div class="row">
                    <!-- ICONO DEL QUE HIZO EL POST -->
                    <div class="col-md-2 d-flex justify-content-center">
                        <div class="fileinput fileinput-new text-center " data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="width:300px; height:100px">
                                <!-- <a href="{{ route('perfil_professional.index') }}"> -->
                                <a href="#">
                                    <img src="{{ route('user.icon', basename($muro->creador->avatar)) }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="...">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <!-- INFORMACIÓN DEL POST -->
                            <div class="row d-flex justify-content-between">
                                <div class="col">
                                    <label for="" class="lead font-weight-bold" style="color: #053F72">{{ $muro->creador->nombre." ".$muro->creador->apellido }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label for="">{{ $muro->contenido }}</label>
                                </div>
                            </div>
                            @if($muro->imagen)
                                <div class="row">
                                    <div class="col">
                                        <img src="{{ route('muro.image', basename($muro->imagen)) }}" 
                                            class="w-100" height="300" alt="">
                                    </div>
                                </div>
                            @endif
                        <!-- FIN INFORMACION DEL POST -->

                        <!-- CHAT DEL POST -->
                            <div class="row">
                                <div class="col card" id="cardcontainer" style="height: 100px; overflow-y: scroll;">
                                    <div id="chat_converse" class="chat_converse">
                                        @foreach($muro->comments as $comentario)
                                            <div class="comentario my-1" id="row{{ $comentario->id_muro_comentario }}">
                                                <h5 class="mb-0">
                                                    @if( $comentario->creadorUser )
                                                        @if($comentario->creadorUser->tipo == 'paciente')
                                                            {{
                                                                $comentario->creadorUser->paciente ? 
                                                                $comentario->creadorUser->paciente->nombre . ' ' . $comentario->creadorUser->paciente->apellido : "" 
                                                            }}
                                                        @else
                                                            {{
                                                                $comentario->creadorUser->profesional ? 
                                                                $comentario->creadorUser->profesional->nombre . ' ' . $comentario->creadorUser->profesional->apellido : ""
                                                            }}
                                                        @endif
                                                    @else
                                                        {{ $comentario->creadorAdmin->nombre." ".$comentario->creadorAdmin->apellido }}
                                                    @endif
                                                </h5>
                                                <label for="">{{ $comentario->comentario }}</label> 
                                                @if($comentario->FK_id_user == Auth::user()->id)
                                                    <a id="{{ route('blog_patient.comment.delete', $comentario->id_muro_comentario) }}" 
                                                        type="{{ $comentario->id_muro_comentario }}" class="deleteComment">
                                                        <button class="btn btn-sm" style="background-color: #669D00;">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </a>
                                                @endif
                                                <hr>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        <!-- FIN DEL CHAT -->

                        <!-- FORM PARA ENVIAR UN COMENTARIO -->
                            <div class="row mx-auto">
                                <form method="post" action="{{ route('blog_patient.comment.store', $muro->id_muro ) }}" class="row container-fluid">
                                    @csrf
                                    <div class="col-md-9 m-0 p-0">
                                        <textarea id="chatSend" name="comentario" cols="30" rows="10" required
                                                class="form-control" placeholder="Escribe un comentario..." 
                                                style="width: 100%; height: 40px !important; background: #F5F5F5; border-radius: 20px; padding: 10px"
                                        ></textarea>
                                    </div>
                                    <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-center align-items-center" style="cursor: pointer; width: 40px; height: 40px; border-radius: 60px; background: #053F72">
                                        <button type="submit">
                                            <i class="fas fa-paper-plane" style="font-size: 20px; color: white; "></i>
                                        </button>
                                    </div>
                                    <!-- <div class="col-md-1 col-sm-12 col-xs-12 pt-2 m-0 d-flex justify-content-center align-items-center">
                                        <label for="file-upload" class="subir text-dark ">
                                            <i class="fas fa-paperclip m-0 p-0" style="font-size: 20px; cursor: pointer;"></i>
                                        </label>
                                        <input id="file-upload" onchange='cambiar()' type="file" style='display: none;' />
                                    </div> -->

                                    <!-- SECCIÓN DE LOS LIKES -->
                                    <?php $user_like = false; ?>
                                    @foreach($muro->likes as $like)
                                        @if($like->FK_id_user == Auth::user()->id)
                                            <?php $user_like = true; ?>
                                        @endif
                                    @endforeach
                                    <div class="col-md-1 col-sm-12 col-xs-12 px-2 m-0 d-flex justify-content-start align-items-center" style="cursor: pointer;">
                                        @if($user_like)
                                            <i id="{{$muro->id_muro}}" class="fas fa-thumbs-up btn-dislike" style="color: #669D00; font-size: 20px" ></i>
                                        @else
                                            <i id="{{$muro->id_muro}}" class="far fa-thumbs-up btn-like" style="font-size: 20px; color: #669D00"></i>
                                        @endif
                                        <span id="thumbCount{{$muro->id_muro}}" class="font-weight-bold px-1">{{ $muro->likes->count() }}</span>
                                    </div>
                                </form>
                            </div>
                        <!-- FIN FORM PARA ENVIAR UN COMENTARIO -->
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <!-- FIN RESTO DE LOS COMENTARIOS -->
</div>

<input type="hidden" id="URLPATH" value="{{ route('blog_patient.like.url') }}">
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<script>
    function cambiar() {
        var pdrs = document.getElementById('file-upload').files[0].name;
    }
</script>

<style>
     #cardcontainer::-webkit-scrollbar {
        width: 8px;
        /* Tamaño del scroll en vertical */
        height: 8px;
        /* Tamaño del scroll en horizontal */
    }

    #cardcontainer::-webkit-scrollbar-thumb {
        background: #ccc;
        border-radius: 4px;
    }

    /* Cambiamos el fondo y agregamos una sombra cuando esté en hover */
    #cardcontainer::-webkit-scrollbar-thumb:hover {
        background: #b3b3b3;
        box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
    }

    /* Cambiamos el fondo cuando esté en active */
    #cardcontainer::-webkit-scrollbar-thumb:active {
        background-color: #999999;
    }

    #cardcontainer::-webkit-scrollbar-track {
        background: #e1e1e1;
        border-radius: 4px;
    }

    /* Cambiamos el fondo cuando esté en active o hover */
    #cardcontainer::-webkit-scrollbar-track:hover,
    #cardcontainer::-webkit-scrollbar-track:active {
        background: #d4d4d4;
    }

    button, input[type="submit"], input[type="reset"] {
        background: none;
        color: inherit;
        border: none;
        padding: 0;
        font: inherit;
        cursor: pointer;
        outline: inherit;
    }
</style>

@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="{{ asset('js/murolike.js') }}"></script>
<script>
    $(document).ready(function() {

        $('.deleteComment').click(function(){
            var id = $(this).attr('type');
            var url = $(this).attr('id');
            
            Swal.fire({
                icon: 'warning',
                title: '¿Esta seguro de borrar este comentario?',
                type: 'warning',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        data: {
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        type: 'POST',
                        success: function(response){
                            if(response.delete){
                                $("#row"+id).remove();
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'success'
                                )
                            }
                            else{
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'warning'
                                )
                            }
                        }
                    });
                }
            });
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>