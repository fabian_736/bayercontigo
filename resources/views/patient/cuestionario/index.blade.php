@extends('layouts.patient.app2')
@section('content')

<div class="row ">
    <div class="row-reverse">
        <div class="col">
            <!-- <label for="" class="h2" style="color: #053F72"><b>Cuestionario |</b> Reto 1 </label> -->
            <label for="" class="h2" style="color: #053F72"><b>Cuestionario |</b> {{ $cuestionario->titulo }} </label>
        </div>
        <div class="col">
            <!-- <label for="" class="h4" style="color: #053F72">Bienvenido a la prueba de adherencia al
                conocimiento del módulo Barreras mentales</label> -->
            <label for="" class="h4" style="color: #053F72">{{ $cuestionario->descripcion }}</label>
        </div>
    </div>
</div>


<div class="row my-5 ">
    
    <!-- POSICIONES -->
    <div class="col-md-1 p-0 m-0">
        <div class="row-reverse">
            @foreach($cuestionario->preguntas as $key => $pregunta)
                @if($key == 0)
                    <div class="passNumber col" id="{{ $key }}">
                        <div class="col-md-1">
                            <div class="d-flex justify-content-center align-items-center" style="width: 40px; height: 40px; border-radius: 60px; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%);">
                                <a href="#" class="text-white font-weight-bold" id="number{{ $key }}">{{ $key+1 }}</a>
                                <i class="fas fa-check" id="ico{{ $key }}" style="display: none"></i>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="passNumber col {{ $key%2 != 0 ? 'my-3' : '' }}" id="{{ $key }}">
                        <div class="col-md-1">
                            <div class="d-flex justify-content-center align-items-center" id="div{{ $key }}" style="background: #C1C1C1; width: 40px; height: 40px; border-radius: 60px; ">
                                <a href="#" class="text-white font-weight-bold" id="number{{ $key }}">{{ $key+1 }}</a>
                                <i class="fas fa-check" id="ico{{ $key }}" style="display: none"></i>
                            </div>
                        </div>
                    </div>
                @endif
                <!-- <div class="col">
                    <div class="col-md-1">
                        <div class="d-flex justify-content-center align-items-center" id="divthree" style="background: #C1C1C1; width: 40px; height: 40px; border-radius: 60px; ">
                            <a href="" class="text-white font-weight-bold" id="three">3</a>
                            <i class="fas fa-check" id="ico3" style="display: none"></i>
                        </div>
                    </div>
                </div> -->
            @endforeach
        </div>
    </div>

    @foreach($cuestionario->preguntas as $key => $pregunta)
        
        <!-- 1 CUESTIONARIO MULTIPLE CON IMAGENES -->
        @if($pregunta->tipo == "imagenes")
            <div class="contenido col p-0 {{ $key == 0 ? '' : 'd-none' }}" id="item{{ $key }}">
                <div class="row-reverse">
                    <div class="col p-0">
                        <label for="" class="h4 font-weight-bold">
                            {{ $pregunta->pregunta }}:
                        </label>
                    </div>
                    <div class="row mx-auto mb-2">
                        <div class="col-md-12 p-0">
                            <!-- CADA UNA DE LAS OPCIONES CON IMAGEN -->
                            <div class="row">
                                <?php $todas_anteriores = null ?>
                                @foreach($pregunta->respuestas as $index => $respuesta)
                                    @if($respuesta->imagen)
                                        <div class="col-md-2">
                                            <div class="card bg-dark backgroundImage" >
                                                <a href="javascript:;" class="img" id="{{ $pregunta->id_pregunta }}">
                                                    <div class="card-body cardImage p-0 m-0 bg-white d-flex justify-content-center" 
                                                        id="{{ route('respuesta.image', basename($respuesta->imagen)) }}"
                                                        title="{{ $respuesta->id_respuesta }}"    
                                                        style="width: 100%; height: 300px; border-style: solid; border-color: #053F72; ">
                                                        <div class="mt-5 imageCheck{{ $pregunta->id_pregunta }} d-none" style="position: absolute;" id="check1">
                                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                                        </div>
                                                        <div class="col mt-auto d-flex justify-content-center">
                                                            <label for="" class="lead h4 text-center font-weight-bold">
                                                                {{ $respuesta->respuesta }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @else
                                        <?php $todas_anteriores = $respuesta ?>
                                    @endif
                                @endforeach
                            </div>
                            <!-- TODAS LAS ANTERIORES -->
                            <div class="row">
                                <div class="col-8 checkall" id="{{ $pregunta->id_pregunta }}" title="{{ $todas_anteriores->id_respuesta }}">
                                    <a class="btn form-control" style="color: #fff; background: #669D00">TODAS LAS ANTERIORES</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <!-- 2 CUESTIONARIO DE LINKS -->
        @if($pregunta->tipo == "relaciones")
            <div class="contenido col p-0 {{ $key == 0 ? '' : 'd-none' }}" id="item{{ $key }}">

                <div class="row mx-auto mb-2" >
                    <div class="col-md-12 p-0 " >
                        @include('patient.cuestionario.connect', [
                            'pregunta' => $pregunta, 
                            'respuestas' => $pregunta->respuestas,
                            'opciones' => $pregunta->opciones
                        ])
                    </div>
                </div>
            </div>
        @endif

        <!-- 3 CUESTIONARIO DE VERDADERO Y FALSO -->
        @if($pregunta->tipo == "vf")
            <div class="contenido col p-0 {{ $key == 0 ? '' : 'd-none' }}" id="item{{ $key }}">
                <div class="row-reverse">
                    <div class="col p-0">
                        <label for="" class="h4">Conecte las afirmaciones con el circulo correspondiente indicando si es verdadero (V) o falso (F)</label>
                    </div>
                    <div class="row mx-auto mb-2">
                        <div class="col-md-12 p-0">
                            @foreach($pregunta->respuestas as $respuesta)
                                <div class="row mx-auto p-0">
                                    <!-- SELECCIONADOR DE VERDADERO O FALSO -->
                                    <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                        <div class="row-reverse">
                                            <!-- SELECCIONADOR DE VERDADERO -->
                                            <div class="col mb-2" id="{{ $respuesta->id_respuesta }}">
                                                <i class="fas fa-check-circle text-success selectTrue" style="cursor: pointer;"> V</i>
                                            </div>
                                            <!-- SELECCIONADOR DE FALSO -->
                                            <div class="col" id="{{ $respuesta->id_respuesta }}">
                                                <i class=" fas fa-times-circle text-danger selectFalse" style="cursor: pointer;"> F</i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- PREGUNTA -->
                                    <div class="col-md-10 p-0 mt-3">
                                        <div class="row-reverse p-0 m-0 ">
                                            <div class="col m-0 p-0">
                                                <label for="" class="h5 font-weight-bold" style="float: left; color: #053F72">{{ $respuesta->respuesta }}.</label>
                                            </div>
                                            <div class="col m-0 p-0">
                                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                                            </div>
                                        </div>

                                    </div>
                                    <!-- ESPACIO DONDE DICE TU SELECCION -->
                                    <div class="col-md-1 d-flex align-items-center">
                                        <!-- VERDADERO -->
                                        <div class="col d-none" id="true{{ $respuesta->id_respuesta }}">
                                            <button class="btn-sm btn-success border-0" style="cursor: pointer;">
                                                <span>Verdadero
                                            </button>
                                        </div>
                                        <!-- FALSO -->
                                        <div class="col d-none" id="false{{ $respuesta->id_respuesta }}">
                                            <button class="btn-sm btn-danger border-0" style="cursor: pointer;">
                                                <span>Falso
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <!-- 4 CUESTIONARIO DE OPCIONES MULTIPLES SIN IMAGEN -->
        @if($pregunta->tipo == "multiple")
            <div class="contenido col p-0 {{ $key == 0 ? '' : 'd-none' }}" id="item{{ $key }}">
                <div class="row-reverse" >
                    <div class="col p-0">
                        <label for="" class="h4">
                            {{ $pregunta->pregunta }}:
                        </label>
                    </div>
                    <?php $alphabet = range('a', 'z'); ?>
                    @foreach($pregunta->respuestas as $dex => $respuesta)
                        <div class="row mx-auto mb-2">
                            <div class="col-md-12 p-0">
                                <div class="row mx-auto p-0">
                                    <!-- NUMERACION -->
                                    <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                        <div class="list-group fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                                style="width: 40px; height: 40px;"
                                                id="{{ $pregunta->id_pregunta }}"
                                                title="{{ $respuesta->id_respuesta }}" >
                                                <a href="javascript:;" class="selectMultiple list-group-item font-weight-bold">{{ $alphabet[$dex] }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- RESPUESTA -->
                                    <div class="col-md-10 p-0 mt-3">
                                        <div class="row-reverse p-0 m-0 ">
                                            <div class="col m-0 p-0">
                                                <label for="" class="h5 font-weight-bold"
                                                    style="float: left; color: #053F72">{{ $respuesta->respuesta }}</label>
                                            </div>
                                            <div class="col m-0 p-0">
                                                <hr class="m-0 p-0"
                                                    style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    @endforeach    
</div>

<!-- FORM QUE HIRE AUTOCOMPLETANDO CON JAVASCRIPT Y LO ENVIO CON JQUERY UN #SUBMIT -->
    <form class="d-none" id="submitPreguntas" action="{{ route('cuestionario_patient.store') }}" method="post">
        @csrf
        <input type="hidden" name="id_cuestionario" value="{{ $cuestionario->id_cuestionario }}">
        @foreach($cuestionario->preguntas as $index => $pregunta)

            @if($pregunta->tipo == "imagenes")
                <input type="hidden" name="imagenes[{{ $index }}][pregunta]" value="{{$pregunta->id_pregunta}}">
                <input type="hidden" name="imagenes[{{ $index }}][respuesta]" value="" id="respuesta{{$pregunta->id_pregunta}}">
            @endif

            @if($pregunta->tipo == "vf")
                @foreach($pregunta->respuestas as $dex => $respuesta)
                    <input type="hidden" name="vf[{{ $dex }}][respuesta]" value="{{$respuesta->id_respuesta}}">
                    <input type="hidden" name="vf[{{ $dex }}][opcion]" value="" id="resp{{$respuesta->id_respuesta}}">
                @endforeach
            @endif

            @if($pregunta->tipo == "multiple")
                <input type="hidden" name="multiple[{{ $index }}][pregunta]" value="{{$pregunta->id_pregunta}}">
                <input type="hidden" name="multiple[{{ $index }}][respuesta]" value="" id="respuesta{{$pregunta->id_pregunta}}">
            @endif

            @if($pregunta->tipo == "relaciones")
                @foreach($pregunta->respuestas as $dex => $respuesta)
                <!--     <input type="hidden" name="relaciones[{{ $dex }}][respuesta]" value="{{$respuesta->id_respuesta}}">
                    <input type="hidden" name="relaciones[{{ $dex }}][opcion]" value="" id="resp{{$respuesta->id_respuesta}}">
                 -->@endforeach
            @endif

        @endforeach
    </form>
<!-- FIN FORM JQUERY -->

<input type="hidden" value="0" id="currentDivs">
<input type="hidden" value="{{ $key }}" id="numberDivs">

<div class="row mx-auto" id="bottom1">
    <div class="col d-flex align-items-center">
        <a href="{{ route('retos_patient.index') }}" style="cursor:pointer">
            <label for="" style="text-decoration-line: underline; cursor:pointer">SALIR</label>
        </a>
    </div>
    <div class="col d-flex justify-content-end">
        <a href="javascript:;" class="btn" style="background: #669D00;" id="nextQuestion">Siguiente</a>
    </div>
</div>

<!-- PRIMER MODAL -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col d-flex justify-content-center">
                        <label for="" class="font-weight-bold lead" style="color: #053F72;">Has terminado el Cuestionario!</label>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%); width: 200px; height: 100px;">
                                <a href="javascript:;" id="totalPuntos" class="h2 font-weight-bold text-white">20</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="text-right">Tienes <span id="tCorrectas">3</span> de <span id="tRespuestas">5</span> respuestas correctas, has ganado:</label>
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #053F72">PUNTOS</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                            </div>
                        </div>


                    </div>
                    <div class="row-reverse ml-auto mt-3 mr-3">
                        <div class="col mx-auto">
                            <a href="{{route('retos_patient.index')}}" class="btn text-white " style="background: #669D00;">VOLVER A RETOS</a>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <a href="{{ route('cuestionario_patient.index', $cuestionario->FK_id_capacitacion) }}" style="color: #053F72; text-decoration-line: underline">Volver a hacer el examen</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SEGUNDO MODAL -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col d-flex justify-content-center">
                        <label for="" class="font-weight-bold lead" style="color: #053F72;">Has terminado los retos básicos de Adempas®</label>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%); width: 200px; height: 100px;">
                                <a href="javascript:;" class="h2 font-weight-bold text-white">20</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="text-right">Felicitaciones, has cumplido con todos los ejercicios del reto básico de Adempas®, has ganado:</label>
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #053F72">insignia Adempas JR.</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #053F72; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="h3 p-0 m-0" style="float: right; color: #669D00">200 Puntos</label>
                            </div>
                        </div>
                    </div>
                    <div class="row-reverse ml-auto mt-3">
                        <div class="col d-flex justify-content-end">
                            <a class="btn text-white" style="background: #669D00;" href="{{route('practice_profesional.index')}}">RETOS MEDIO</a>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <a href="{{route('practice_profesional.index')}}" style="color: #053F72; text-decoration-line: underline">Volver a hacer el entrenamiento básico</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- SCRIPT DE PASAR A SIGUIENTE O RETORNAR A OTRA PREGUNTA -->
<script>
    $(document).ready(function() {
        //Boton de siguiente
        $('#nextQuestion').click(function(){
            var current = parseInt($('#currentDivs').val()) + 1;
            var number = $('#numberDivs').val();

            if(current > number){
                console.log(current);

                var vacios = 0;
                $('#submitPreguntas input').each(function(){
                    if( !$(this).val() ) {
                        vacios++;
                    }
                });

                //TODO PERFECTO ENVIO EL AJAX SI CONFIRMA QUE LO QUIERE ENVIAR
                if(vacios == 0){
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Estas Seguro de enviar el cuestionario?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Si, por favor!',
                        cancelButtonText: 'No, cancelar!',
                        reverseButtons: true
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#submitPreguntas').submit();                 
                        } 
                    })
                }
                else{
                    Swal.fire({
                        icon: 'warning',
                        title: 'Completa el Cuestionario',
                        text: 'Faltan '+vacios+' preguntas por completar, porfavor revisa y culmina de hacerlo'
                    });
                }

                return;
            }

            //Ajustes de los Pasos
            $('#number'+(current-1)).addClass('d-none');
            $('#div'+current).css({
                'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%)'
            });
            $('#ico'+(current-1)).css({
                'display': 'block',
                'color': 'white'
            });

            //Ajuste del contenido visual | preguntas
            $('.contenido').addClass('d-none');
            $('#item'+current).removeClass('d-none');

            //Ajusto el ultimo current
            if(current < number){
                console.log(current);
                $('#currentDivs').val(current);
            }
            else if(current == number){
                console.log(current);
                $('#currentDivs').val(current);
                $(this).text('Finalizar');
            }
        });

        //Click sobre el paso en las preguntas de cuestionario para volver y corregir
        $('.passNumber').click(function(){
            var key = parseInt($(this).attr('id')); //pregunta actual de 0 hacia adelante
            var number = $('#numberDivs').val();

            //Ajustes de los Pasos a Transparente
            for (let index = key; index < number; index++) {
                $('#number'+(index+1)).removeClass('d-none');
                $('#div'+(index+1)).css({
                    'background': '#C1C1C1'
                });
                $('#ico'+(index+1)).css({
                    'display': 'none',
                    'color': 'white'
                });
            }

            //Ajustes de los pasos a color #AJUSTAR TODO
            for (let index = 0; index <= key; index++) {
                $('#number'+(index)).addClass('d-none');
                $('#div'+(index)).css({
                    'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%)'
                });
                $('#ico'+(index)).css({
                    'display': 'block',
                    'color': 'white'
                });
            }

            //Ajuste del contenido visual | preguntas
            $('.contenido').addClass('d-none');
            $('#item'+key).removeClass('d-none');

            //Ajusto el ultimo current
            if(key < number){
                console.log(key);
                $('#currentDivs').val(key);
                $('#nextQuestion').text('Siguiente');
            }
            else if(key == number){
                console.log(key);
                $('#currentDivs').val(key);
                $('#nextQuestion').text('Finalizar');
            }
        });

        //Envio AJAX del cuestionario
        $("#submitPreguntas").on('submit', function(e){
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');

            $.ajax({
                type: type,
                url: url,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                error: function(resp) {
                    console.log(resp);
                    if (resp.fallo){
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: resp.fallo
                        });
                    }
                    else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Fallo al enviar el cuestionario, por favor intentelo de nuevo!'
                        });
                    }
                },
                success: function(resp) {
                    console.log(resp);
                    
                    var html = "";
                    jQuery.each(resp.errors, function(key, value) {
                        html += value+"\n";
                    });

                    if (resp.errors){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Error en los datos...',
                            text: html
                        });
                    }

                    if (resp.success) {
                        $('#totalPuntos').text(resp.puntos);
                        $('#tRespuestas').text(resp.total);
                        $('#tCorrectas').text(resp.correctas);
                        
                        //MODAL DEL FINAL POST ENVIO EXITOSO
                        $('#exampleModal').modal('show');
                    }

                    if (resp.change){
                        swal.fire({
                            title: 'Oops...!',
                            text: 'Hubo un cambio en el cuestionario por lo que las preguntas enviadas no tuvieron un enlace, intentalo de nuevo',
                            icon: 'warning',
                        }),
                        setTimeout(function() {
                            window.top.location = "{{route('retos_patient.index')}}"
                        },2000)
                    }

                    if (resp.fallo){
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: resp.fallo
                        });
                    }
                },
            });   
        });
    });
</script>

<style>
    .img {
        background-color: powder#053F72;
        transition: .5s ease;
    }

    .img:hover {
        background-color: yellow !important;
        opacity: 0.9;
        color: #053F72;
    }

    .cardImage{
        /* background-image: url(https://cpmr-islands.org/wp-content/uploads/sites/4/2019/07/test.png) !important; */
        background-repeat: no-repeat;
        background-size: contain
    }
</style>

<!-- SCRIPT VERDADERO Y FALSO -->
<script>
    $(document).ready(function() {
        //Seleccionar Verdadero
        $('.selectTrue').click(function(){
            var id_respuesta = $(this).parent().attr('id'); 

            //Habilitar la visual de verdadero y falso
            $('#true'+id_respuesta).addClass('d-none');
            $('#false'+id_respuesta).addClass('d-none');

            $('#true'+id_respuesta).removeClass('d-none');

            //Arreglar la respuesta en el form
            $('#resp'+id_respuesta).val("true");
        });

        //Seleccionar Falso
        $('.selectFalse').click(function(){
            var id_respuesta = $(this).parent().attr('id'); 

            //Habilitar la visual de verdadero y falso
            $('#true'+id_respuesta).addClass('d-none');
            $('#false'+id_respuesta).addClass('d-none');

            $('#false'+id_respuesta).removeClass('d-none');

            //Arreglar la respuesta en el form
            $('#resp'+id_respuesta).val("false");
        });
    });
</script>

<!-- SCRIPT DE MULTIPLES CON IMAGENES -->
<script>
    $(document).ready(function() {
        //Marcar una sola imagen
        $('.cardImage').click(function(){
            var id_respuesta = $(this).attr('title');
            var id_pregunta = $(this).parent().attr('id'); 

            $('.cardImage .imageCheck'+id_pregunta).addClass('d-none');

            //Marco esto como la correcta y ajusto la respuesta en el form
            $(this).children().removeClass('d-none');
            $('#respuesta'+id_pregunta).val(id_respuesta);
        });
        
        //Todas correctas
        $('.checkall').click(function(){
            var id_respuesta = $(this).attr('title');
            var id_pregunta = $(this).attr('id');

            $('.cardImage .imageCheck'+id_pregunta).removeClass('d-none');

            //Marco esto como la correcta y ajusto la respuesta en el form
            $('#respuesta'+id_pregunta).val(id_respuesta);
        });

        //Ajusto de las imagenes de background
        $('.cardImage').each(function(){
            var background = $(this).attr('id');
            $(this).css({
                'background-image' : 'url("'+background+'")'
            });
        });
    });
</script>

<!-- SCRIPT DE MULTIPLES SIN IMAGENES -->
<script>
    $(document).ready(function() {
        //Marcar una opcion
        $('.selectMultiple').click(function(){
            var id_respuesta = $(this).parent().attr('title');
            var id_pregunta = $(this).parent().attr('id'); 
            
            //Arreglamos CSS
            $('.selectMultiple').css('color','gray');
            $(this).css('color','blue');

            //Marco la seleccionada en el form
            $('#respuesta'+id_pregunta).val(id_respuesta);
        });
    });
</script>



