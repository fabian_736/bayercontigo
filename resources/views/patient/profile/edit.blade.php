@extends('layouts.patient.app2')
@section('content')

<div class="row">
    <h3 for="" class="font-weight-bold" style="color:rgba(2, 113, 139, 1);">Editar perfil</h3><br>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="card m-0 p-5" id="card1">
                <div class="row">
                    <div class="col-md-12">
                    <form method="post" action="{{ route('profile_patient.update') }}" id="updateProfile" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_usuario" value="{{ auth()->user()->id }}">
                        <input type="hidden" name="tipo" value="paciente">
                        <div class="row">
                            <div class="col">
                                <input type="text" name="nombre" placeholder="NOMBRES*" value="{{ old('nombre', auth()->user()->paciente->nombre) }}"
                                       class="form-control @error('nombre') is-invalid @enderror" required>
                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <input type="text" name="apellido" placeholder="APELLIDOS*" value="{{ old('apellido', auth()->user()->paciente->apellido) }}"
                                       class="form-control @error('apellido') is-invalid @enderror" required>
                                @error('apellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" name="identificacion" placeholder="IDENTIFICACIÓN*" value="{{ old('identificacion', auth()->user()->paciente->identificacion) }}" 
                                       class="form-control @error('identificacion') is-invalid @enderror" required>
                                @error('identificacion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <textarea name="descripcion" id="" cols="30" rows="10" 
                                          class="form-control @error('descripcion') is-invalid @enderror" 
                                          placeholder="DESCRIPCIÓN (OPCIONAL)"
                                >{{ old('descripcion', auth()->user()->paciente->descripcion) }}</textarea>
                                @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <button type="submit" class="btn btn-danger ml-auto" style="background: radial-gradient(59.13% 153.27% at 6.94% 90.83%, #C4C4C4 0%, #787878 100%); box-shadow: 4.53333px 2.26667px 4.53333px rgba(0, 0, 0, 0.18);">
                        GUARDAR
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 mx-auto">
        <div style=" width: 30vw; background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #669D00 0%, #02718B 100%); height: 30vw;" class="p-2 rounded-circle mx-auto">
            <div class="rounded-circle d-flex justify-content-center align-items-center" style="background: rgb(255,255,255);
background: linear-gradient(90deg, rgba(255,255,255,0.4962359943977591) 100%, rgba(0,212,255,1) 100%); width: 65%; height: 65%; margin-left: 10%; margin-top: 5%">
                <img src="{{url('img/camara.png')}}" class="imagen w-50" alt="">
                <div class="circle_percent" data-percent="100" style="display: none">
                    <div class="circle_inner">
                        <div class="round_per"></div>
                    </div>
                </div>
            </div>

            <div class="row mx-auto">
                <a class="mx-auto btn-primary-outline btn-file mt-5">
                    <span class="fileinput-new text-white"><u>CARGAR IMAGEN</u></span>
                    <input type="file" name="avatar" id="progress" accept=".jpg, .jpeg, .png" />
                </a>
            </div>
        </div>
    </div>
    </form>
</div>
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<input type="hidden" id="updateForm" value="{{ env('WEB_PANEL_URL') }}">

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }

        $("#updateProfile").on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $('#updateForm').val()+"usuario/update";

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Estas Seguro de actualizar tu perfil?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, por favor!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: type,
                        url: url,
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        error: function(resp) {
                            console.log(resp);
                            if (resp.fallo){
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: resp.fallo
                                });
                            }
                            else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Fallo al editar el perfil, por favor intentelo de nuevo!'
                                });
                            }
                        },
                        success: function(resp) {
                            console.log(resp);
                            var html = "";
                            jQuery.each(resp.errors, function(key, value) {
                                html += value+"\n";
                            });

                            if (resp.errors){
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Error en los datos...',
                                    text: html
                                });
                            }

                            if (resp.success) {
                                swalWithBootstrapButtons.fire(
                                    'Guardando!',
                                    'El cambio de información ha sido exitoso.',
                                    'success'
                                ),
                                setTimeout(function() {
                                    window.top.location = "{{route('profile_patient.index')}}"
                                },2000)
                            }

                            if (resp.fallo){
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: resp.fallo
                                });
                            }
                        },
                    });                    
                } 
                else if (result.dismiss === Swal.DismissReason.cancel) {
                    swalWithBootstrapButtons.fire(
                        'Cancelado!',
                        'Tus cambios no se han guardado.',
                        'error'
                    )
                }
            })
        });
    });

    function guardar() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Estas Seguro de actualizar tu perfil?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, por favor!',
            cancelButtonText: 'No, cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                swalWithBootstrapButtons.fire(
                    'Guardando!',
                    'El cambio de información ha sido exitoso.',
                    'success'
                ),
                setTimeout(function() {
                    window.top.location = "{{route('profile_patient.index')}}"
                },2000)
            } 
            else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelado!',
                    'Tus cambios no se han guardado.',
                    'error'
                )
            }
        })

    }

    var progress = document.getElementById('progress')

    progress.onchange = charge

    function charge() {
        var url = progress.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

        console.log(url);
        if (progress.files && progress.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
            $('#progress').attr('disabled', true);
            document.body.onfocus = roar;
            console.log('chargin');
        }
        else{
            progress.value = null;
            
            $(".circle_percent").css({
                'display': 'none'
            });

            $(".imagen").css({
                'display': 'block'
            });

            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No has escogido ninguna imagen o no cumple el formato de imagen!'
            });
        }
    }

    function roar() {
        $(".circle_percent").css({
            'display': 'block'
        })

        $(".imagen").css({
            'display': 'none'
        })
        
        $(".circle_percent").each(function() {
            var $this = $(this),
                $dataV = $this.data("percent"),
                $dataDeg = $dataV * 3.6,
                $round = $this.find(".round_per");
                $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
                $this.append('<div class="circle_inbox"><span class="percent_text"></span></div>');

                $this.prop('Counter', 0).animate(
                    { Counter: $dataV, }, 
                    {
                        duration: 2000,
                        easing: 'swing',
                        step: function(now) {
                            $this.find(".percent_text").text(Math.ceil(now) + "%");
                        },
                });
            
            if ($dataV >= 100) {
                $round.css("transform", "rotate(" + 360 + "deg)");
                setTimeout(function() {
                    $this.addClass("percent_more");
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Imagen de perfil preparada para actualizar',
                        showConfirmButton: false,
                        timer: 1800
                    })
                }, 2000);
                setTimeout(function() {
                    $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
                }, 2000);

            }

        });
        document.body.onfocus = null;
        $('#progress').attr('disabled', false);
        console.log('depleted');
    }
</script>

<script>
    function f1() {
        $('#div1').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #669D00 0%, #02718B 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'position': 'absolute',
            'width': '12px',
            'border-radius': '40px',
            'z-index': '1000',
            'background': 'transparent'
        });
        $('#card1').css({
            'display': 'block'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f2() {
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #669D00 0%, #02718B 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(805.33% 412.26% at 48.77% -319.62%, #669D00 0%, #02718B 100%)',
            'height': '45%'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'block'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f3() {
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(54.06% 54.06% at 18.84% 26.21%, #669D00 0%, #02718B 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(805.33% 412.26% at 48.77% -319.62%, #669D00 0%, #02718B 100%)',
            'height': '100%'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'block'
        });

    };
</script>

@endsection

<style>
    .bottom-circle {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .bottom-circle-one {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #669D00 0%, #02718B 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .circle_percent {
        font-size: 200px;
        width: 1em;
        height: 1em;
        position: relative;
        background: #eee;
        border-radius: 50%;
        overflow: hidden;
        display: inline-block;
        margin: 20px;
    }

    .circle_inner {
        position: absolute;
        left: 0;
        top: 0;
        width: 1em;
        height: 1em;
        clip: rect(0 1em 1em .5em);
    }

    .round_per {
        position: absolute;
        left: 0;
        top: 0;
        width: 1em;
        height: 1em;
        background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #669D00 0%, #02718B 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
        clip: rect(0 1em 1em .5em);
        transform: rotate(180deg);
        transition: 1.05s;
    }

    .percent_more .circle_inner {
        clip: rect(0 .5em 1em 0em);
    }

    .percent_more:after {
        position: absolute;
        left: .5em;
        top: 0em;
        right: 0;
        bottom: 0;
        background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #669D00 0%, #02718B 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
        content: '';
    }

    .circle_inbox {
        position: absolute;
        top: 10px;
        left: 10px;
        right: 10px;
        bottom: 10px;
        background: #fff;
        z-index: 3;
        border-radius: 50%;
    }

    .percent_text {
        position: absolute;
        font-size: 36px;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        z-index: 3;
    }
</style>