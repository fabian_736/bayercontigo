@extends('layouts.auth.app')

@section('content')
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center ">
        <div class="col-xl-10 col-lg-12 col-md-12 ">
            <div class="o-hidden border-0 my-5">

                <div class="d-flex flex-row-reverse">
                    <div class="col-lg-6 shadow-lg mx-auto" style="background-color: rgba(245, 245, 245, 0.7);">
                        <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                            <div class="text-center ">
                                <img src="{{ url('img/logo_login.png') }}" class="w-50" alt=""
                                    style="margin-bottom: 30px;">
                            </div>

                            <!-- Email Input -->
                            @if (Session::has('message'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 3%;">
                                    {{ Session::get('message') }}
                                </div>
                            @endif

                            @if (Session::has('edit'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                    {{ Session::get('edit') }}
                                </div>
                            @endif

                            <div class="row mx-auto text-center my-3">
                                <div class="col">
                                    <i class="fas fa-key active"></i>
                                    <label for="" style="text-decoration: underline #053F72; color: #053F72; font-weight: bold">RECUPERARACIÓN DE CONTRASEÑA</label>
                                </div>
                            </div>

                            <!-- FORM -->
                                <form class="user" method="POST" action="{{ route('password.update') }}" id="primero">
                                    @csrf
                                    <input type="hidden" name="token" value="{{ $passwordReset->token }}">
                                    <div class="form-group">
                                        <input type="email"
                                            class="form-control form-control-user @error('email') is-invalid @enderror"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            value="{{ old('email') }}" autocomplete="email" autofocus
                                            placeholder="Correo electronico" name="email" required> 

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" 
                                            placeholder="Nueva contraseña..." name="password" id="">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                                            placeholder="Confirmar nueva contraseña" name="password_confirmation" id="">
                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <button 
                                        type="submit"
                                        class="btn btn-user btn-block mt-5"
                                        style="border-radius: 40px; color: #053F72;  background-color: #AEDC5A; font-weight: bold">
                                        Reestablecer Contraseña
                                    </button>
                                </form>
                            <!-- FIN FORM -->

                            <!-- RETORNAR AL INICIO -->
                                <div class="text-center">
                                    <a class="btn-icon-split text-white" href="{{ route('login.form_login') }}">
                                        <span style="color: #02718B">Retornar al Inicio de Sesión</span>
                                    </a>
                                </div>
                            <!-- FIN RETORNAR AL INICIO -->
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<style>::placeholder {
    color: gray !important;
    }</style>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });
</script>
@endsection