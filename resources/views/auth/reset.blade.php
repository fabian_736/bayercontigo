@extends('layouts.auth.app')
@section('content')



    <div class="container p-0 m-0">

        <!-- Outer Row -->
        <div class="row">

            <div class="col-xl-10 col-lg-12 col-md-12 ">
 
                <div class="o-hidden border-0 my-5" id="cardlogin">
                        <!-- Nested Row within Card Body -->
                        <div class="d-flex flex-row-reverse">
                            <div class="col-lg-6 shadow-lg mx-auto bg-white" >
                                <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                                    <div class="text-center ">
                                        <img src="{{ url('img/logo_login.png') }}" class="w-50" alt=""
                                            style="margin-bottom: 30px;">
                                    </div>

                                    <h4 for="" class="text-center">
                                        Tienes 10 minutos para enviar tu código de confirmación y validar tu usuario
                                    </h4>

                                    @if (Session::has('message'))
                                        <div
                                            style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                            {{ Session::get('message') }}
                                        </div>
                                    @endif

                                    <form class="code" method="POST" action="{{ route('login.code') }}">
                                        @csrf
                                        <div class="form-group">
                                            <input type="email"
                                                class="form-control form-control-user @error('email') is-invalid @enderror"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                value="{{ old('email') }}" autocomplete="email" autofocus
                                                placeholder="Correo electronico" name="email" required> 

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="text"
                                                class="form-control form-control-user @error('code') is-invalid @enderror"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                value="{{ old('code') }}" autofocus placeholder="Código de Verificación"
                                                name="code" required>
                                            @error('code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <button type="submit" class="btn btn-user btn-block mt-5" 
                                                style="border-radius: 40px; color: #053F72;  background-color: #AEDC5A; font-weight: bold">
                                            Confirmar
                                        </button>
                                    </form>
                                       
                                    <div class="text-center">
                                        <a class="btn-icon-split text-white" href="{{ url()->previous() }}">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-key" style="color: #053F72"></i>
                                            </span>
                                            <span style="color: #053F72; font-weight: bold">Enviar nuevamente el codigo de confirmacion</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

        </div>

    </div>

   

    <style>::placeholder {
        color: gray !important;
      }
      
      
      @media (min-width: 500px){
        #cardlogin{
            margin-left: 10px;
            margin-right: 10px;
        }
      }</style>



@endsection
