@extends('layouts.auth.app')
@section('content')

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center ">

        <div class="col-xl-10 col-lg-12 col-md-12 ">

            <div class="o-hidden border-0 my-5">
                <!-- Nested Row within Card Body -->
                <div class="d-flex flex-row-reverse">
                    <div class="col-lg-6 shadow-lg mx-auto" style="background-color: rgba(245, 245, 245, 0.7);">
                        <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                            <div class="text-center ">
                                <img src="{{ url('img/logo_login.png') }}" class="w-50" alt=""
                                    style="margin-bottom: 30px;">
                            </div>

                            <h4 for="" class="text-center">
                                Debes modificar tu contraseña, por favor completa el formulario y dale a enviar.
                            </h4>

                            @if (Session::has('edit'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                    {{ Session::get('edit') }}
                                </div>
                            @endif

                            @if (Session::has('error'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: red; color:#ffffff ; margin-bottom: 1%;">
                                    {{ Session::get('error') }}
                                </div>
                            @endif

                            <form id="formRequest" method="POST" action="{{ route('login.cambio') }}">
                                @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" 
                                            placeholder="Nueva contraseña..." name="password" id="">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row my-3">
                                    <div class="col">
                                        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                                            placeholder="Confirmar nueva contraseña" name="password_confirmation" id="">
                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <input type="hidden" name="email" value="{{ Session::get('email') }}">

                                <div class="form-row">
                                    <div class="col d-flex justify-content-end">
                                    <i class="fas fa-info-circle" data-container="body"
                                            data-toggle="popover" data-placement="right"
                                            data-content="Debe ser alfanúmerica, minimo una mayúscula, una minúscula y un caracter especial"></i>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-user btn-block mt-5"
                                    style="border-radius: 40px; color: #053F72;  background-color: #AEDC5A; font-weight: bold"
                                    >Continuar
                                </button>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<style>
    ::placeholder {
        color: gray !important;
    }
</style>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    function init() {
        var x = document.getElementById("primero");
        var y = document.getElementById("segundo");
        x.style.display = "block";
        y.style.display = "none";
    }

    function myFunctionB1() {
        var x = document.getElementById("primero");
        var y = document.getElementById("segundo");
        if (x.style.display === "none") {
            x.style.display = "block";
            y.style.display = "none";
        } else {
            x.style.display = "none";
            y.style.display = "none";
        }

    }

    function myFunctionB2() {
        var x = document.getElementById("primero");
        var y = document.getElementById("segundo");
        if (y.style.display === "none") {
            y.style.display = "block";
            x.style.display = "none";
        } else {
            x.style.display = "none";
            y.style.display = "none";
        }
    }

    init();

    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });
</script>
@endsection
