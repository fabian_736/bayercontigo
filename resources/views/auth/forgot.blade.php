@extends('layouts.auth.app')
@section('content')

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center ">

        <div class="col-xl-10 col-lg-12 col-md-12 ">

            <div class="o-hidden border-0 my-5">
                <!-- Nested Row within Card Body -->
                <div class="d-flex flex-row-reverse">
                    <div class="col-lg-6 shadow-lg mx-auto" style="background-color: rgba(245, 245, 245, 0.7);">
                        <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                            <div class="text-center ">
                                <img src="{{ url('img/logo_login.png') }}" class="w-50" alt=""
                                    style="margin-bottom: 30px;">
                            </div>

                            @if (Session::has('edit'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                    {{ Session::get('edit') }}
                                </div>
                            @endif
                            
                            @if (Session::has('message'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                    {{ Session::get('message') }}
                                </div>
                            @endif

                            <div class="row mx-auto text-center my-3">
                                <div class="col">
                                    <label for=""
                                        style="text-decoration: underline #053F72; color: #053F72; font-weight: bold">Recibirá
                                        el código de confirmación al siguiente correo electrónico</label>
                                </div>
                            </div>

                            <form id="formRequest" method="POST" action="{{ route('login.solicitar') }}">
                                @csrf
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" id="exampleInputEmail"
                                        aria-describedby="emailHelp" value="{{ old('email') }}" autocomplete="email"
                                        autofocus placeholder="Correo electronico" name="email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </form>
                            
                            <button onclick="alert()" class="btn btn-user btn-block mt-5"
                                style="border-radius: 40px; color: #053F72;  background-color: #AEDC5A; font-weight: bold">
                                Enviar Codigo
                            </button>

                            <div class="text-center">
                                <a class="btn-icon-split text-white" href="{{ route('password.request') }}">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-key"></i>
                                    </span>
                                    <span style="color: #02718B">Recuperar mi contraseña</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
</div>

<style>
    ::placeholder {
        color: gray !important;
    }
</style>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    function alert() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Estas seguro de enviar el codigo?',
            text: "Una vez enviado el codigo, caducara en los proximos 15 minutos",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, enviar codigo!',
            cancelButtonText: 'No, cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                swalWithBootstrapButtons.fire(
                    'Enviado!',
                    'Tu codigo sera enviado.',
                    'success'
                ).then(function() {
                    //window.location = "{{route('login.reset')}}";
                    $('#formRequest').submit();
                });
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'No se ha enviado el codigo',
                    'error'
                )
            }
        })
    }

    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });
</script>
@endsection
