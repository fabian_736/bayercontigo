@extends('layouts.auth.app')

@section('content')
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center ">
        <div class="col-xl-10 col-lg-12 col-md-12 ">
            <div class="o-hidden border-0 my-5">

                <div class="d-flex flex-row-reverse">
                    <div class="col-lg-6 shadow-lg mx-auto" style="background-color: rgba(245, 245, 245, 0.7);">
                        <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                            <div class="text-center ">
                                <img src="{{ url('img/logo_login.png') }}" class="w-50" alt=""
                                    style="margin-bottom: 30px;">
                            </div>

                            <!-- Email Input -->
                            @if (Session::has('message'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 3%;">
                                    {{ Session::get('message') }}
                                </div>
                            @endif

                            @if (Session::has('edit'))
                                <div
                                    style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                    {{ Session::get('edit') }}
                                </div>
                            @endif

                            <div class="row mx-auto text-center my-3">
                                <div class="col">
                                    <i class="fas fa-stethoscope active"></i>
                                    <label for="" onclick="myFunctionB1()" style="text-decoration: underline #053F72; color: #053F72; font-weight: bold">PROFESIONALES</label>
                                </div>
                                <div class="col">
                                    <i class="far fa-head-side-medical"></i>
                                    <label for="" onclick="myFunctionB2()" style="text-decoration: underline #053F72; color: #053F72; font-weight: bold">PACIENTES</label>
                                </div>
                            </div>

                            <!-- LOGIN DE PROFESIONALES -->
                                <form class="user" method="POST" action="{{ route('login.auth') }}" id="primero">
                                    @csrf
                                    <div class="row my-3">
                                        <div class="col text-center">
                                            <label for="" class="font-weight-bold" style="color: #053F72;">PROFESIONALES</label>
                                        </div>
                                    </div>    
                                    <div class="form-group">
                                        <input type="email"
                                            class="form-control form-control-user @error('email') is-invalid @enderror"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            value="{{ old('email') }}" autocomplete="email" autofocus
                                            placeholder="Correo electronico" name="email" required> 

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input type="password"
                                            class="form-control form-control-user @error('password') is-invalid @enderror"
                                            id="exampleInputPassword" placeholder="Contraseña" name="password" required
                                            autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <button 
                                        type="submit"
                                        class="btn btn-user btn-block mt-5"
                                        style="border-radius: 40px; color: #053F72;  background-color: #AEDC5A; font-weight: bold">
                                        Iniciar Sesion
                                    </button>
                                </form>
                            <!-- FIN LOGIN DE PROFESIONALES -->

                            <!-- LOGIN DE PACIENTES -->
                                <form class="user" method="POST" action="{{ route('login.auth') }}" id="segundo" style="display: none;">
                                    @csrf
                                    <div class="row my-3">
                                        <div class="col text-center">
                                            <label for="" class="font-weight-bold" style="color: #053F72;">PACIENTES</label>
                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <input type="email"
                                            class="form-control form-control-user @error('email') is-invalid @enderror"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            value="{{ old('email') }}" autocomplete="email" autofocus
                                            placeholder="Correo electronico" name="email" required>  
                                            
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input type="password"
                                            class="form-control form-control-user @error('password') is-invalid @enderror"
                                            id="exampleInputPassword" placeholder="Contraseña" name="password" required
                                            autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <button 
                                        type="submit"
                                        class="btn btn-user btn-block mt-5"
                                        style="border-radius: 40px; color: #053F72;  background-color: #AEDC5A; font-weight: bold">
                                        Iniciar Sesion
                                    </button>
                                </form>
                            <!-- FIN LOGIN DE PACIENTES -->

                            <!-- RECUPERAR PASSWORD -->
                                <div class="text-center">
                                    <a class="btn-icon-split text-white" href="{{ route('password.request') }}">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-key"></i>
                                        </span>
                                        <span style="color: #02718B">Recuperar mi contraseña</span>
                                    </a>
                                </div>
                            <!-- FIN RECUPERAR PASSWORD -->
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</div>

<style>::placeholder {
    color: gray !important;
    }</style>

<script>
    function init() {
        var x = document.getElementById("primero");
        var y = document.getElementById("segundo");
        x.style.display = "block";
        y.style.display = "none";
    }

    function myFunctionB1() {
        var x = document.getElementById("primero");
        var y = document.getElementById("segundo");
        if (x.style.display === "none") {
            x.style.display = "block";
            y.style.display = "none";
        } else {
            x.style.display = "none";
            y.style.display = "none";
        }
    }

    function myFunctionB2() {
        var x = document.getElementById("primero");
        var y = document.getElementById("segundo");
        if (y.style.display === "none") {
            y.style.display = "block";
            x.style.display = "none";
        } else {
            x.style.display = "none";
            y.style.display = "none";
        }
    }

    init();
</script>
@endsection
