<?php

use Illuminate\Support\Facades\Route;

//CONTROLADORES
use App\Http\Controllers\RutasController;
use App\Http\Controllers\ProfessionalController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\PasswordResetController;
use App\Http\Controllers\NotificacionController;

/* RUTAS DEL INICIO | RECUPERAR PASSWORD | SOLICITAR CÓDIGO */
    Route::get('/',[LoginController::class,'form_login'])->name('login'); //mostrar el login o dashboard
    Route::prefix('/auth')->group(function(){
        //rutas visuales
        Route::get('/login',[LoginController::class,'form_login'])->name('login.form_login'); //vista login

        //código de confirmación de correo
        Route::get('/forgot',[LoginController::class,'form_forgot'])->name('login.forgot'); //vista para enviar el código
        Route::get('/reset',[LoginController::class,'form_reset'])->name('login.reset'); //vista de confirmar código enviado
        Route::post('/codigo',[LoginController::class,'sendCode'])->name('login.code'); //metodo de enviar el código a confirmar
        Route::post('/solicitar',[LoginController::class,'requestCode'])->name('login.solicitar'); //metodo de solicitar código

        //Rutas de login y logout
        Route::post('/authentication',[LoginController::class,'login'])->name('login.auth'); //metodo de loggueo
        Route::get('/logout',[LoginController::class,'logout'])->name('login.logout'); //metodo de logout

        //Cambio de Password debido a que el admin te la módifico o te creo por primera vez
        Route::get('/changepass',[LoginController::class,'form_newpass'])->name('login.newpass'); //vista para modificar password
        Route::post('/change/password',[LoginController::class,'changePassword'])->name('login.cambio'); //envio de cambio de password
    });

    // RUTAS RESET PASSWORD
    Route::prefix('/password')->group(function(){
        Route::get('/request', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
        Route::get('/forgot',[PasswordResetController::class,'forgot'])->name('password.forgot');
        Route::get('/find/{token}', [PasswordResetController::class,'find'])->name('password.find');

        Route::post('/email',[PasswordResetController::class,'create'])->name('password.email');
        Route::post('/reset', [PasswordResetController::class, 'reset'])->name('password.update');
    });
/* FIN RUTAS DEL INICIO */

/* HOME | TUTORIALES | IMAGENES */
    Route::get('/user/getImage/{filename?}', [DashboardController::class, 'getAvatar'])->name('user.icon');
    Route::get('/noticia/getImage/{filename?}', [DashboardController::class, 'getImageNoticia'])->name('noticia.image');
    Route::get('/noticia/getDocument/{filename?}', [DashboardController::class, 'getDocumentNoticia'])->name('noticia.document');
    Route::get('/noticia/getVideo/{filename?}', [DashboardController::class, 'getVideoNoticia'])->name('noticia.video');
    Route::get('/muro/getImage/{filename?}', [DashboardController::class, 'getImageMuro'])->name('muro.image');
    Route::get('/producto/getImage/{filename?}', [DashboardController::class, 'getImageProducto'])->name('producto.image');
    Route::get('/categoria/getImage/{filename?}', [DashboardController::class, 'getImageCategoria'])->name('categoria.image');
    
    Route::middleware(['auth', 'state'])->get('home',[DashboardController::class,'index'])->name('home');
    Route::middleware(['auth', 'state'])->get('tutorials',[DashboardController::class,'tutorial'])->name('tutorials');
    Route::middleware(['auth', 'state'])->get('tutorial/past/{id}',[DashboardController::class,'pastTutorial'])->name('tutorial.home');
/* FIN HOME | TUTORIALES */

/* RUTAS DE NOTIFICACIONES */
    Route::middleware(['auth', 'state'])->prefix('/notificaciones')->group(function(){
        /* DELETE */
        Route::post('/deleteone/{id?}',[NotificacionController::class,'deleteone'])->name('notificacion.deleteone');
        Route::get('/deleteall',[NotificacionController::class,'deletetodo'])->name('notificacion.deleteall');

        /* READ */
        Route::post('/readone',[NotificacionController::class,'marcarLeido'])->name('notificacion.readone');
        Route::get('/readall',[NotificacionController::class,'marcartodo'])->name('notificacion.readall');
    });
/* FIN RUTAS DE NOTIFICACIONES */

/* RUTAS DE PACIENTES */
    Route::middleware(['auth', 'state', 'paciente'])->prefix('/patient')->group(function(){
        /* INICIO */
        Route::get('/tutorial/index',[PatientController::class,'tutorial_patient'])->name('tutorial_patient.index');
        Route::get('/index',[PatientController::class,'portal_patient'])->name('portal_patient.index');

        /* PERFIL */
        Route::get('/perfil',[PatientController::class,'profile_patient'])->name('profile_patient.index');
        Route::get('/perfil/edit',[PatientController::class,'profile_edit_patient'])->name('profile_patient.edit');
        Route::post('/perfil/update',[PatientController::class,'profile_update'])->name('profile_patient.update');

        /* RETOS | ENTRENAIMENTOS */
        Route::get('/retos',[PatientController::class,'retos_patient'])->name('retos_patient.index');
        Route::get('/retos/getArchive/{filename?}/{id?}', [PatientController::class, 'openArchive'])->name('retos_patient.archive');

        /* CUESTIONARIO */
        Route::get('/cuestionario/{id?}',[PatientController::class,'cuestionario_patient'])->name('cuestionario_patient.index');
        Route::get('/cuestionario/imagen/{filename?}', [PatientController::class, 'getImageRespuesta'])->name('respuesta.image');
        Route::post('/cuestionario/store',[PatientController::class,'storeCuestionario'])->name('cuestionario_patient.store');
        
        /* NOTICIAS */
        Route::get('/noticias',[PatientController::class,'noticias_patient'])->name('noticias_patient.index');
        Route::get('/noticias/detail/{id?}',[PatientController::class,'noticia'])->name('noticias_patient.detail');
        Route::get('/noticia/add/like/{id}',[PatientController::class,'noticialike'])->name('noticias_patient.like');
        Route::get('/noticia/add/dislike/{id}',[PatientController::class,'noticiadislike'])->name('noticias_patient.dislike');
        Route::get('/noticia/add/',[PatientController::class,'like'])->name('noticias_patient.like.url');
        
        /* MUROS | BLOGS */
        Route::get('/blog',[PatientController::class,'blog_patient'])->name('blog_patient.index');
        Route::post('/blog/comentario/store/{id}',[PatientController::class,'comentarioStore'])->name('blog_patient.comment.store');
        Route::post('/blog/comentario/delete/{id}',[PatientController::class,'comentarioDelete'])->name('blog_patient.comment.delete');
        
        Route::get('/blog/add/like/{id}',[PatientController::class,'murolike'])->name('blog_patient.like');
        Route::get('/blog/add/dislike/{id}',[PatientController::class,'murodislike'])->name('blog_patient.dislike');
        Route::get('/blog/add/',[PatientController::class,'like'])->name('blog_patient.like.url');
    });
/* FIN RUTAS DE PACIENTES */

/* RUTAS DE PROFESIONALES */
    Route::middleware(['auth', 'state', 'profesional'])->prefix('/professional')->group(function(){
        /* PANTALLA DE INICIO */
        Route::get('/tutorial/index',[ProfessionalController::class,'tutorial_professional'])->name('tutorial_professional.index');
        Route::get('/index',[ProfessionalController::class,'portal_professional'])->name('portal_professional.index');

        /* PERFIL */
        Route::get('/perfil',[ProfessionalController::class,'perfil_professional'])->name('perfil_professional.index');
        Route::get('/perfil/edit',[ProfessionalController::class,'perfil_edit_professional'])->name('perfil_professional.edit');
        Route::post('/perfil/update',[PatientController::class,'perfil_update_professional'])->name('perfil_professional.update');
        Route::get('/perfil/certificado/{id?}',[ProfessionalController::class,'perfil_certificado_professional'])->name('certificado_profesional.edit');

        /* ENTRENAMIENTOS */
        Route::get('/practice/{id?}/{id_capacitacion?}',[ProfessionalController::class,'practice_professional'])->name('practice_profesional.index');
        Route::get('/practice/getArchive/{filename?}/{id?}', [ProfessionalController::class, 'openArchive'])->name('practice_profesional.archive');

        /* CUESTIONARIOS */
        Route::get('/cuestionario/{id?}',[ProfessionalController::class,'cuestionario_professional'])->name('cuestionario_profesional.index');
        Route::get('/cuestionario/imagen/{filename?}', [ProfessionalController::class, 'getImageRespuesta'])->name('respuesta.image');
        Route::post('/cuestionario/store',[ProfessionalController::class,'storeCuestionario'])->name('cuestionario_profesional.store');

        /* INSIGNIAS */
        Route::get('/insignia/index',[ProfessionalController::class,'insignia_professional'])->name('insignia_profesional.index');

        /* MUROS */
        Route::get('/muro/index',[ProfessionalController::class,'muro_professional'])->name('muro_profesional.index');
        Route::post('/muro/comentario/store/{id}',[ProfessionalController::class,'comentarioStore'])->name('muro_profesional.comment.store');
        Route::post('/muro/comentario/delete/{id}',[ProfessionalController::class,'comentarioDelete'])->name('muro_profesional.comment.delete');
        
        Route::get('/muro/add/like/{id}',[ProfessionalController::class,'murolike'])->name('muro_profesional.like');
        Route::get('/muro/add/dislike/{id}',[ProfessionalController::class,'murodislike'])->name('muro_profesional.dislike');
        Route::get('/muro/add/',[ProfessionalController::class,'like'])->name('muro_profesional.like.url');

        /* PREMIOS */
        Route::get('/premios/',[ProfessionalController::class,'premio_professional'])->name('premio_profesional.index');
        Route::get('/premios/select/{id?}',[ProfessionalController::class,'premioselect_professional'])->name('premio_profesional.select');
        Route::post('/premios/detail/{id?}',[ProfessionalController::class,'premio_detail'])->name('premio_profesional.detail');
        Route::post('/premios/canjeo',[ProfessionalController::class,'premio_canjeo'])->name('premio_profesional.canjeo');
    });
/* FIN RUTAS DE PROFESIONALES */

//Auth::routes();
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
