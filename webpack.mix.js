const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


 mix.browserSync('127.0.0.1:8000');

 mix.js('resources/js/app.js', 'public/js')
 .postCss('resources/css/app.css', 'public/css', [
     //
 ]);

 mix.styles([
     'resources/assets/css/material-dashboard.css',
     'resources/assets/demo/demo.css',
 ], 'public/css/app.css')
 
     .scripts([
        'resources/assets/js/core/jquery.min.js',
        'resources/assets/js/core/popper.min.js',
        'resources/assets/js/core/bootstrap-material-design.min.js',
        'resources/assets/js/plugins/perfect-scrollbar.jquery.min.js',
        'resources/assets/js/plugins/moment.min.js',
        'resources/assets/js/plugins/sweetalert2.js',
        'resources/assets/js/plugins/jquery.validate.min.js',
        'resources/assets/js/plugins/jquery.bootstrap-wizard.js',
        'resources/assets/js/plugins/bootstrap-selectpicker.js',
        'resources/assets/js/plugins/bootstrap-datetimepicker.min.js',
        'resources/assets/js/plugins/jquery.dataTables.min.js',
        'resources/assets/js/plugins/bootstrap-tagsinput.js',
        'resources/assets/js/plugins/jasny-bootstrap.min.js',
        'resources/assets/js/plugins/fullcalendar.min.js',
        'resources/assets/js/plugins/jquery-jvectormap.js',
        'resources/assets/js/plugins/nouislider.min.js',
        'resources/assets/js/plugins/arrive.min.js',
        'resources/assets/js/plugins/chartist.min.js',
        'resources/assets/js/plugins/bootstrap-notify.js',
        'resources/assets/js/material-dashboard.js',
        'resources/assets/demo/demo.js',
        ], 'public/js/app.js');