<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\User;
use App\Models\UserPassword;
use App\Models\UserHistorial;

//MAILABLE
use App\Mail\CodeVerifier;
use Illuminate\Support\Facades\Mail;

//USABLE
use Carbon\Carbon;
use DateTime;
use Session;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/tutorials';//RouteServiceProvider::HOME;

    public function __construct(){
        $this->middleware('guest')->except('logout');
    }

    //redirección post login
    protected function redirectTo(){
        if(!Auth::user()->email_verified){
            $caracteres_permitidos = '0123456789';
            $codigo = substr(str_shuffle($caracteres_permitidos), 0, 6);

            $information = new \stdClass();
            $information->asunto = 'Código de Verificación de Correo';
            $information->correo = Auth::user()->email;
            $information->codigo = $codigo;

            $user = Auth::user();
            $user->email_verified_at = date("Y-m-d H:i:s");
            $user->email_code = $codigo;
            $user->save();

            try{
                Mail::to(Auth::user()->email)->queue(new CodeVerifier($information));
            }
            catch(\Exception $e){
                "";
            }
            
            //Cerrando la Session
            Auth::logout();

            return route('login.reset');
        }

        if(Auth::user()->cambiar){
            $email = Auth::user()->email;
            //Cerrando la Session
            Auth::logout();

            Session::put('email', $email);
            return route('login.newpass');
        }

        //Diferencia entre la fecha actual y la ultima vez que se edito la contraseña
        $fechainicial = new DateTime(Auth::user()->fecha_cambio);
        $fechafinal = new DateTime('now');
        $diferencia = $fechainicial->diff($fechafinal);
        $meses = ( $diferencia->y * 12 ) + $diferencia->m;

        //Si pasaron 6 meses de la contraseña debe ser cambiada
        if($meses >= 6){
            $email = Auth::user()->email;
            //Cerrando la Session
            Auth::logout();

            Session::put('email', $email);
            return route('login.newpass');
        }
        
        //Historial de Login o visita al aplicativo
        $loghistory = new UserHistorial();
        $loghistory->accion = "Visita al Aplicativo - Login";
        $loghistory->fecha_registro = date('Y-m-d');
        $loghistory->FK_id_user = Auth::user()->id;
        $loghistory->save();

        return route('tutorials');
    }

    //visual del login
    public function form_login(){
        return view('auth.form_login');
    }

    //visual de solicitar código
    public function form_forgot(){
        return view('auth.forgot');
    }

    //visual de enviar código
    public function form_reset(){
        return view('auth.reset');
    }

    //visual de cambiar password
    public function form_newpass(){

        $email = Session::has('email') ? Session::get('email') : null;

        if(!$email)
            return redirect()->route('login.form_login');
        
        $user = User::where('email', $email)->first();
        if(!$user)
            return redirect()->route('login.form_login'); 
            
        return view('auth.newpass', compact('email'));
    }

    //PROCESOS DE CRUD Y ENVIO DE CÓDIGO DE CONFIRMACIÓN Y PASSWORD
    public $message_error = [
        'numeric' => 'El dato debe viajar en formato númerico.',
        'required' => 'El dato es requerido',
        'max' => 'El dato debe llevar maximo 255 caracteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'same' => 'La confirmación y la contraseña deben coincidir',
        'min' => 'El dato debe llevar minimo 8 caracteres',
        'regex' => 'La contraseña debe contener mayúscula, números y caracteres especiales',
    ];

    //enviar código a verificar
    public function sendCode(Request $request){

        try{
            $validate = Validator::make($request->all(), [
                'code' => 'required|numeric',
                'email' => 'required|string|email|max:255',
            ], $this->message_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();  
            }

            $code = $request->code;
            $email = $request->email;

            $codeToken = User::where('email', $email)->first();//->where('email_code', $code)->first();
            
            //revisar si el usuario existe
            if(!$codeToken){
                return Redirect::back()->with('message', 'El correo suministrado no se encuentra registrado, porfavor intentalo de nuevo'); 
            }

            //reviso si el token existe
            if ($codeToken->email_code != $code){
                return redirect()->route('login.forgot')->with('edit', 'El código de verificación no existe o esta erroneo, por favor solicita uno nuevo'); 
            }

            //revisamos si no han pasado 10 miutos
            if (Carbon::parse($codeToken->email_verified_at)->addMinutes(10)->isPast()) {
                $codeToken->email_code = null;
                $codeToken->email_verified_at = null;
                $codeToken->save();
                return redirect()->route('login.forgot')->with('edit', 'El código de verificación ha expirado, por favor solicita uno nuevo'); 
            }

            //Todo paso de forma excelente
            $user = $codeToken;
            $user->email_verified = 1;
            $user->email_verified_at = date("Y-m-d H:i:s");
            $user->save();

            //Historial de verificación de cuenta
            $loghistory = new UserHistorial();
            $loghistory->accion = "Verificación de Correo Electrónico";
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = $user->id;
            $loghistory->save();

            return redirect()->route('login.form_login')->with('edit', 'Cuenta Verificada Exitosamente!, Ingresa de Nuevo para completar el usuario'); 
        } 
        catch(\Exception $e){
            return Redirect::back()->with('message', 'Fallo en el proceso de verificación de código, intentelo más tarde'); 
        }       
    }

    //solicitar nuevo código
    public function requestCode(Request $request){
        
        try{
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email|max:255',
            ], $this->message_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();  
            }

            $email = $request->email;

            $user = User::where('email', $email)->first();//->where('email_code', $code)->first();
            
            //revisar si el usuario existe
            if(!$user){
                return Redirect::back()->with('message', 'El correo suministrado no se encuentra registrado, porfavor intentalo de nuevo'); 
            }

            if(!$user->email_verified){
                $caracteres_permitidos = '0123456789';
                $codigo = substr(str_shuffle($caracteres_permitidos), 0, 6);
    
                $information = new \stdClass();
                $information->asunto = 'Código de Verificación de Correo';
                $information->correo = $user->email;
                $information->codigo = $codigo;
    
                $user->email_verified_at = date("Y-m-d H:i:s");
                $user->email_code = $codigo;
                $user->save();
    
                try{
                    Mail::to($user->email)->queue(new CodeVerifier($information));
                }
                catch(\Exception $e){
                    return Redirect::back()->with('message', 'El correo de verificación no pudo ser enviado, por favor intentelo más tarde'); 
                }
                
                //Historial de Envio de Solicitud de Código de Verificación
                $loghistory = new UserHistorial();
                $loghistory->accion = "Solicitud de Código para Verificación de Correo";
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = $user->id;
                $loghistory->save();

                return redirect()->route('login.reset')->with('message', 'Se envio el correo de forma exitosa, revisa tu bandeja y suministra el código!'); 
            }

        } 
        catch(\Exception $e){
            return Redirect::back()->with('message', 'Fallo en el proceso de envio de código, intentelo más tarde'); 
        } 
    }

    //checking password si se asemeja al correo
    public function checkPassword($check){
        //Chequiamos si es el correo y el password son iguales
        $check["email"] = explode(' ',strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["email"])));
        $check["password"] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["password"]));
        
        if(stripos($check["password"],$check['email'][0]) !== false)
            return true;
        else
            return false;
    }

    //solicitar cambio de password
    public function changePassword(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email',
                'password' => [
                    'string',
                    'min:8',
                    'max:50',
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&+-]/',
                    'required'
                ],
                'password_confirmation' => 'required|string|same:password',
            ], $this->message_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput(); 
            }

            $user = User::where('email', $request->email)->first();
            
            if (!$user){
                return Redirect::back()->with('edit', 'No se encontro ningun usuario bajo este correo electrónico.'); 
            }

            //Chequiamos si no estamos repitiendo el mismo password
            if (Hash::check($request->input('password'), $user->password)) {
                return Redirect::back()->with('edit', 'El nuevo password es igual al anterior'); 
            }

            //Chequiamos que no se asemeje al correo
            $data["email"] = $request->email;
            $data["password"] = $request->password;
            if($this->checkPassword($data)){
                return Redirect::back()->with('edit', 'El password se asemeja al correo, por favor utiliza otro');
            }

            //Revisamos si el password no es el mismo de los ultimos 3 utilizados
            foreach ($user->historialpass->take(3) as $pass) {
                if(Hash::check($request->input('password'), $pass->password))
                    return Redirect::back()->with('edit', 'La contraseña a editar ya fue utilizada en las ultimas 3 contraseñas utilizadas');
            }

            //Grabamos el historial de passwords
                $history = new UserPassword();
                $history->password   = Hash::make($request->input('password'));
                $history->registro   = date('Y-m-d');
                $history->FK_id_user = $user->id;
                $history->save();
            
            //Update de los datos de login
                $user->password = Hash::make($request->input('password'));
                $user->cambiar = 0;
                $user->fecha_cambio = date('Y-m-d');
                $user->update();

            //Grabamos el historial del usuario login update
                $loghistory = new UserHistorial();
                $loghistory->accion = "Cambio de Contraseña del Usuario";
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = $user->id;
                $loghistory->save();

            DB::commit();
            Auth::login($user);
            return redirect()->route('tutorials')->with('message', 'Cambio de Contraseña exitosa, porfavor completa el tutorial y disfruta de Bayer Contigo'); 
        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al editar contraseña: '.$e->getMessage());
        }
    }
}
