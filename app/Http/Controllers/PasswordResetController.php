<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\User;
use App\Models\PasswordReset;
use App\Models\UserPassword;
use App\Models\UserHistorial;

//USABLES
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;

//MAILABLE
use App\Mail\PasswordResetMail;
use Illuminate\Support\Facades\Mail;

class PasswordResetController extends Controller
{
    public $message_error = [
        'email' => 'El correo debe ser una dirección de correo valida.',
        'required' => 'El dato es requerido',
        'same' => 'La confirmación y la contraseña deben coincidir',
        'min' => 'El dato debe llevar minimo 8 caracteres',
        'max' => 'El dato debe llevar maximo 8 caracteres',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
    ];

    public function create(Request $request){

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email',
            ], $this->message_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $user = User::where('email', $request->email)->first();
            
            if (!$user){
                return Redirect::back()->with('message', 'El correo no se encuentra registrado dentro del sistema');
            }

            if($user->tipo == 'paciente'){
                $nombre = $user->paciente ? $user->paciente->nombre . ' ' . $user->paciente->apellido : ""; 
            }
            else{
                $nombre = $user->profesional ? $user->profesional->nombre . ' ' . $user->profesional->apellido : "";
            }

            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'tipo' => "reset",
                    'email' => $user->email,
                    'token' => Str::random(60)
                ]
            );

            if ($user && $passwordReset)
                Mail::to($user->email)->queue(new PasswordResetMail($passwordReset->token, $passwordReset->tipo, $nombre));

            DB::commit();
            return Redirect::back()->with('edit', 'Se envio un correo con el link para resetear su contraseña. Tienes 30 min para cambiar la misma');
        }catch(\Exception $e){
            DB::rollback();
            return Redirect::back()->with('message', 'Fallo al enviar el correo, intentelo más tarde: '.$e->getMessage());
        }
    }
    
    public function find($token, $tipo = null){

        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset){
            return redirect()->route('login.form_login')->with('message', 'El token de recuperación de clave es invalido'); 
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(30)->isPast()) {
            $passwordReset->delete();
            return redirect()->route('login.form_login')->with('message', 'El token de recuperación de clave ya se encuentra expirado'); 
        }

        return view('auth.passwords.reset',compact('passwordReset'));
    }
    
    //Chequiamos password si se asemeja al correo
    public function checkPassword($check){
        //Chequiamos si es el correo y el password son iguales
        $check["email"] = explode(' ',strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["email"])));
        $check["password"] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["password"]));
        
        if(stripos($check["password"],$check['email'][0]) !== false)
            return true;
        else
            return false;
    }

    public function reset(Request $request){

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email',
                'password' => [
                    'string',
                    'min:8',
                    'max:50',
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&+-]/',
                    'required'
                ],
                'password_confirmation' => 'required|string|same:password',
                'token' => 'required',
            ], $this->message_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();  
            }

            $passwordReset = PasswordReset::where('token', $request->token)->first();

            if (!$passwordReset){
                return redirect()->route('login')->with('message', 'El token de recuperación de clave es invalido'); 
            }

            $user = User::where('email', $request->email)->first();
            
            if (!$user){
                return Redirect::back()->with('message', 'No se encontro ningun usuario bajo este correo electrónico.'); 
            }

            //Chequiamos si no estamos repitiendo el mismo password
            if (Hash::check($request->input('password'), $user->password)) {
                return Redirect::back()->with('edit', 'El nuevo password es igual al anterior'); 
            }

            //Chequiamos que no se asemeje al correo
            $data["email"] = $request->email;
            $data["password"] = $request->password;
            if($this->checkPassword($data)){
                return Redirect::back()->with('edit', 'El password se asemeja al correo, por favor utiliza otro');
            }

            //Revisamos si el password no es el mismo de los ultimos 3 utilizados
            foreach ($user->historialpass->take(3) as $pass) {
                if(Hash::check($request->input('password'), $pass->password))
                    return Redirect::back()->with('edit', 'La contraseña a editar ya fue utilizada en las ultimas 3 contraseñas utilizadas');
            }

            //Grabamos el historial de passwords
                $history = new UserPassword();
                $history->password   = Hash::make($request->input('password'));
                $history->registro   = date('Y-m-d');
                $history->FK_id_user = $user->id;
                $history->save();
            
            //Update de los datos de login
                $user->password = Hash::make($request->input('password'));
                $user->cambiar = 0;
                $user->fecha_cambio = date('Y-m-d');
                $user->update();

            //Grabamos el historial del usuario login update
                $loghistory = new UserHistorial();
                $loghistory->accion = "Recuperación de Contraseña";
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = $user->id;
                $loghistory->save();

            $passwordReset->delete();

            DB::commit();
            Auth::login($user);
            return redirect()->route('tutorials')->with('message', 'Cambio de Contraseña exitosa, porfavor completa el tutorial y disfruta de Bayer Contigo'); 
        
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
}
