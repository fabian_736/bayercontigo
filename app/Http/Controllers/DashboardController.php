<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

//MODELOS
use App\Models\User;
use App\Models\UserHistorial;

//MAILABLE
use App\Mail\CodeVerifier;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{   
    //Envio del Home y redirección de acuerdo al usuario
    public function index(){  
        if(Auth::user()->tipo == "paciente")
            return redirect()->route('portal_patient.index');
        else
            return redirect()->route('portal_professional.index');
    }

    //Tutorial del programa a sus visuales si no las ha visto por primera vez
    public function tutorial(){
        if(Auth::user()->tipo == "paciente"){
            if(Auth::user()->tutorial)
                return redirect()->route('portal_patient.index');
            else
                return redirect()->route('tutorial_patient.index');
        }
        else{
            if(Auth::user()->tutorial)
                return redirect()->route('portal_professional.index');
            else
                return redirect()->route('tutorial_professional.index');
        }
    }

    //Envio de skip o avanzar del tutorial y almacenar su estado de ya haberlo visualizado
    public function pastTutorial($id = null){
        $user = Auth::user();

        $user->tutorial = 1;
        $user->save();

        //Historial de Visualización de Tutorial
        $loghistory = new UserHistorial();
        $loghistory->accion = "Visualización de Tutorial de Iniciación";
        $loghistory->fecha_registro = date('Y-m-d');
        $loghistory->FK_id_user = $user->id;
        $loghistory->save();

        if(Auth::user()->tipo == "paciente")
            return redirect()->route('portal_patient.index');
        else
            return redirect()->route('portal_professional.index');
    }

    //Recoger la imagen de perfil de los usuarios
    public function getAvatar($filename = null){
        
        $url = env('WEB_PANEL_URL')."avatar/".$filename;

        $response = Http::get($url);
        
        return $response;
    }

    //Recoger la imagen de fondo de la Noticia
    public function getImageNoticia($filename = null){
        
        $url = env('WEB_PANEL_URL')."noticia/imagen/".$filename;

        $response = Http::get($url);
        
        return $response;
    }

    //Recoger el documento de la noticia
    public function getDocumentNoticia($filename = null){
        
        $url = env('WEB_PANEL_URL')."noticia/document/".$filename;

        $response = Http::get($url);

        $extension = pathinfo($filename)['extension'];

        if($extension == "pdf"){
            return Response($response, 200, [ 
                'Content-Type' =>  'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]); 
        }
        else{
            return Response($response, 200, [ 
                'Content-Type' =>  'image/'.$extension,
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]); 
        }
    }

    //Recoger stream del video
    public function getVideoNoticia($filename = null){
        
        $url = env('WEB_PANEL_URL')."noticia/video/".$filename;

        return view('patient.noticias.stream',compact('filename', 'url'));
    }

    //Recoger la imagen si tiene del muro
    public function getImageMuro($filename = null){
        
        $url = env('WEB_PANEL_URL')."muro/imagen/".$filename;

        $response = Http::get($url);
        
        return $response;
    }

    //Recoger la imagen si tiene el producto
    public function getImageProducto($filename = null){
        
        $url = env('WEB_PANEL_URL')."producto/imagen/".$filename;

        $response = Http::get($url);
        
        return $response;
    }

    //Recoger la imagen de la categoria
    public function getImageCategoria($filename = null){
        
        $url = env('WEB_PANEL_URL')."premio/imagen/".$filename;

        $response = Http::get($url);
        
        return $response;
    }
}
