<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\CapacitacionUser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Http;

//MODELOS
use App\Models\User;
use App\Models\Cuestionario;
use App\Models\CuestionarioUser;
use App\Models\Muro;
use App\Models\MuroComentario;
use App\Models\MuroLike;
use App\Models\Noticia;
use App\Models\NoticiaLike;
use App\Models\Pregunta;
use App\Models\Puntos;
use App\Models\Respuesta;
use App\Models\RespuestaUser;
use App\Models\UserHistorial;

class PatientController extends Controller
{

    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'file' => 'El dato debe llegar como un archivo',
        'mimes' => 'El archivo debe llegar en formato png, jpg, o jpeg',
        'imagen.max' => 'La imagen no puede ser mayor a 2Mb',
        'boolean' => 'Debes enviar el dato como true (1) o false (0)',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'contenido.max' => "El dato no debe ser mayor a 65535 carácteres",
    ];

    /////////////////////////////////////////////VISUALES////////////////////
    
    //TUTORIAL
        public function tutorial_patient(){
            return view('patient.tutorial.index');
        }
    //FIN TUTORIAL

    //DASHBOARD
        public function portal_patient(){  
            $user = Auth::user();
            return view('patient.portal.index', compact('user'));
        } 
    //FIN DASHBOARD

    //PERFIL
        //inicio
        public function profile_patient(){  
            $user = Auth::user();

            //Historial de Visualización de Perfil
            /* $loghistory = new UserHistorial();
            $loghistory->accion = "Visualización del Perfil";
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = $user->id;
            $loghistory->save(); */

            return view('patient.profile.index', compact('user'));
        } 

        //editar
        public function profile_edit_patient(){  
            $user = Auth::user();
            
            return view('patient.profile.edit', compact('user'));
        } 
    //FIN PERFIL

    //RETOS
        public function retos_patient(){      
            try{
                $user = Auth::user();
                $patology = null;
                $capacitacion = null;

                foreach($user->paciente->patologias->take(1) as $patologia){
                    $patology = $patologia->patologia;
                }
                
                foreach($patology->capacitaciones->take(1) as $training){
                    $capacitacion = $training;
                }
                
                //Historial de Visualización de Retos
                /* $loghistory = new UserHistorial();
                $loghistory->accion = "Visualización de Pantalla de Inicio Retos de Capacitación";
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = $user->id;
                $loghistory->save(); */

                return view('patient.retos.index', compact('capacitacion'));
            }
            catch(\Exception $e){
                return redirect()->route('portal_patient.index')->with('message', 'Fallo en la lectura de retos, intentalo más tarde');
            }
        } 

        public function openArchive($filename = null, $id = null){
            
            $url = env('WEB_PANEL_URL')."capacitacion/getArchive/".$filename;
            
            $response = Http::get($url);

            $extension = pathinfo($response)['extension'];

            //Arreglo de que el User haya visto realmente la capacitación
            $data["FK_id_user"] = Auth::user()->id;
            $data["FK_id_capacitacion_documento"] = $id;
            CapacitacionUser::firstOrCreate($data);

            //Historial de Visualización de Documento de Capacitación
            $loghistory = new UserHistorial();
            $loghistory->accion = "Lectura Documento de Capacitación (Pdf/Video)";
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = Auth::user()->id;
            $loghistory->save();

            if($extension == "pdf")
                $content = 'application/pdf';
            elseif($extension == "mp4"){
                $response = basename($response);
                return view('patient.retos.stream',compact('response'));
            }
            else
                $content = 'image/'.$extension;

            return Response(file_get_contents($response), 200, [ 
                'Content-Type' => $content,
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]);  
        }
    //FIN RETOS

    //CUESTIONARIOS
        public function cuestionario_patient($id_capacitacion = null){  
            $cuestionario = Cuestionario::where('FK_id_capacitacion', $id_capacitacion)->first();

            if(!$cuestionario){
                return redirect()->route('retos_patient.index')->with('message', 'El reto aun no posee cuestionario afiliado');
            }
            
            //Revisamos si ya vio todas las capacitaciones (retos/entrenamientos)
            $user = Auth::user();
            $retos = $cuestionario->capacitacion->entrenamientos->count();
            $vistas = $user->capacitacionesSeen->count();

            if($vistas < $retos){
                return redirect()->route('retos_patient.index')->with('message', 'Porfavor terminar de ver todos los documentos de entrenamientos / retos para hacer el cuesitonario');
            }

            if(!$cuestionario->preguntas->count() > 0){
                return redirect()->route('retos_patient.index')->with('message', 'El cuestionario aun se esta ensamblando, por favor espera a otro día a que este disponible');
            }

            //Historial de Visualización de Capacitacion
            $loghistory = new UserHistorial();
            $loghistory->accion = "Visualización de Cuestionario | Código: ".$cuestionario->id_cuestionario;
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = Auth::user()->id;
            $loghistory->save();

            return view('patient.cuestionario.index', compact('cuestionario'));
        }

        public function getImageRespuesta($filename = null){
            $url = env('WEB_PANEL_URL')."cuestionario/".$filename;

            $response = Http::get($url);
            
            return $response;
        }

        public function storeCuestionario(Request $request){
            
            $validaciones = [
                'id_cuestionario' => 'required',

                'imagenes.*.pregunta' => 'required|numeric', 
                'imagenes.*.respuesta' => 'required|numeric', 

                'vf.*.respuesta' => 'required|numeric', 
                'vf.*.opcion' => 'required|string', 

                'multiple.*.pregunta' => 'required|numeric', 
                'multiple.*.respuesta' => 'required|numeric', 

                /* 'relaciones.*.respuesta' => 'required|numeric', 
                'relaciones.*.opcion' => 'required|numeric',  */
            ];

            DB::beginTransaction();
            try{
                $validate = Validator::make($request->all(), $validaciones);
                
                if($validate->fails()){
                    return response()->json(['errors'=>$validate->errors()->all()],200);
                }

                $user = Auth::user();
                $quest = Cuestionario::find($request->id_cuestionario);

                if(!$quest)
                    return response()->json([
                        'fallo'=>'El cuestionario no se encontro dentro de la base de datos',
                        'id' => $request->id_cuestionario,
                    ],200);

                $cuestionario = new CuestionarioUser();
                $cuestionario->titulo = $quest->titulo;
                $cuestionario->descripcion = $quest->descripcion;
                $cuestionario->tipo = "Cuestionario";
                $cuestionario->puntos = 0;
                $cuestionario->FK_id_cuestionario = $quest->id_cuestionario;
                $cuestionario->FK_id_user = $user->id;
                $cuestionario->save();

                $total_puntos = $quest->puntuacion;
                $total_correctas = 0;
                $total_respuestas = 0;

                //Evaluamos las de opcion multiple + imagen
                    foreach ($request->imagenes as $key => $imagenes) {
                        
                        $pregunta = Pregunta::find($imagenes["pregunta"]);
                        
                        if(!$pregunta)
                            return response()->json([
                                "message" => "La pregunta respondida no fue encontrada, probablemente se módifico la misma",
                                "change" => true,
                            ],200);
                        
                        $respuesta = $pregunta->respuestas->where('id_respuesta', $imagenes["respuesta"])->first();

                        $QUser = new RespuestaUser();
                        $QUser->respuesta = $respuesta->respuesta;
                        $QUser->pregunta = $pregunta->pregunta;
                        $QUser->correcta = $respuesta->correcta;
                        $QUser->FK_id_cuestionario_user = $cuestionario->id_cuestionario_user;
                        $QUser->save();
                        
                        //Sumamos totales correctas y respuestas si esta bien
                        $respuesta->correcta ? $total_correctas++ : "";
                        $total_respuestas++;
                    }

                //Evaluamos las de verdadero y falso
                    foreach ($request->vf as $key => $vf) {
                        $respuesta = Respuesta::find($vf["respuesta"]);
                        
                        if(!$respuesta)
                            return response()->json([
                                "message" => "La respuesta respondida no fue encontrada, probablemente se módifico la misma",
                                "change" => true,
                            ],200);
                        
                        $QUser = new RespuestaUser();
                        $QUser->respuesta = $vf["opcion"];
                        $QUser->pregunta = $respuesta->respuesta;
                        $answer = $respuesta->correcta ? "true" : "false";
                        $QUser->correcta = $answer == $vf["opcion"] ? 1 : 0;
                        $QUser->FK_id_cuestionario_user = $cuestionario->id_cuestionario_user;
                        $QUser->save();
                        
                        //Sumamos totales correctas y respuestas si esta bien
                        $QUser->correcta ? $total_correctas++ : "";
                        $total_respuestas++;
                    }

                //Evaluamos las de opcion multiple normal
                    foreach ($request->multiple as $key => $multiple) {
                        
                        $pregunta = Pregunta::find($multiple["pregunta"]);
                        
                        if(!$pregunta)
                            return response()->json([
                                "message" => "La pregunta respondida no fue encontrada, probablemente se módifico la misma",
                                "change" => true,
                            ],200);
                        
                        $respuesta = $pregunta->respuestas->where('id_respuesta', $multiple["respuesta"])->first();

                        $QUser = new RespuestaUser();
                        $QUser->respuesta = $respuesta->respuesta;
                        $QUser->pregunta = $pregunta->pregunta;
                        $QUser->correcta = $respuesta->correcta;
                        $QUser->FK_id_cuestionario_user = $cuestionario->id_cuestionario_user;
                        $QUser->save();
                        
                        //Sumamos totales correctas y respuestas si esta bien
                        $respuesta->correcta ? $total_correctas++ : "";
                        $total_respuestas++;
                    }

                //Evaluamos la de relación, respuesta + opción


                //Ahora evaluamos los puntos generados y grabamos y actualizamos
                    //Puntos de CuestionUser
                    $cuestionario->puntos = ($total_correctas * $total_puntos) / $total_respuestas;
                    $cuestionario->save();

                    //Puntos de User
                    $user->total_puntos += $cuestionario->puntos;
                    $user->save();

                    //Historial Puntos
                    $notPuntos = new Puntos();
                    $notPuntos->descripcion = "Ganados por cuestionario (".$quest->titulo.") Código: ".$quest->id_cuestionario;
                    $notPuntos->cantidad = $cuestionario->puntos;
                    $notPuntos->FK_id_user = $user->id;
                    $notPuntos->save();

                //Historial de Acciones
                    $historial = new UserHistorial();
                    $historial->accion = "Cuestionario Respondido | Código: ".$quest->id_cuestionario;
                    $historial->fecha_registro = date('Y-m-d');
                    $historial->FK_id_user = $user->id;
                    $historial->save();

                //Mensajes de notificacion

                DB::commit();
                return response()->json([
                    "puntos" => $notPuntos->cantidad,
                    "total" => $total_respuestas,
                    "correctas" => $total_correctas,
                    "success" => true,
                ],200);
            }
            catch(\Exception $e){
                DB::rollback();
                return response()->json([
                    'fallo'=>'No pudo almacenarse el cuestionario, intentelo más tarde | Error: '.$e->getMessage()
                ],200);
            }
        }
    //FIN CUESTIONARIO

    //NOTICIAS
        //devovler todas las noticias
        public function noticias_patient(){
            $noticias = Noticia::where('estado', 1)->get();
            
            $recetas = collect();
            $modas = collect();
            $saludes = collect();

            foreach ($noticias as $key => $noticia) {
                switch($noticia->tipo->nombre){
                    case "Receta": $recetas->push($noticia); break;
                    case "Tip de Moda": $modas->push($noticia); break;
                    case "Salud": $saludes->push($noticia); break;
                }
            }

            //Historial de Visualización de Noticias
            /* $loghistory = new UserHistorial();
            $loghistory->accion = "Visualización de Noticias";
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = Auth::user()->id;
            $loghistory->save(); */

            return view('patient.noticias.index', compact('noticias', 'recetas', 'modas', 'saludes'));
        }

        //enviar solo el detalle de una noticias
        public function noticia($id = null){
            
            if(!$id){
                return response()->json([
                    'status' => 'error',
                    'message' => "Por favor envia un ID para retornar los datos de forma correcta",
                ], 200);
            }

            if(!is_numeric($id)){
                return response()->json([
                    'status' => 'error',
                    'message' => "Por favor envia el ID en formato númerico para recolectar los datos de forma correcta",
                ], 200);
            }

            $noticia = Noticia::find($id);
            
            if(!$noticia){
                return response()->json([
                    'status' => 'error',
                    'message' => "La noticia enviada no fue encontrada entre los registros almacenados",
                ], 200);
            }

            $noticia->documento = basename($noticia->documento);
            
            //Historial de Visualización de Detallado de Noticia
            $loghistory = new UserHistorial();
            $loghistory->accion = "Ver Más Noticia: ".substr($noticia->titulo, 0, 230)."...";
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = Auth::user()->id;
            $loghistory->save();

            return response()->json([
                'status' => 'success',
                'message' => "La noticia fue encontrada exitosamente",
                'noticia' => $noticia,
                'items' => $noticia->items,
                'directorio' => $noticia->directorios,
            ], 200);
        }

        //like a la noticia
        public function noticialike($id_noticia){
            //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
            DB::beginTransaction();
            try{
                $user = Auth::user();
    
                $like = new NoticiaLike();
    
                //REVISO SI YA TIENE LIKE
                $revision = NoticiaLike::where('FK_id_user', $user->id)->where('FK_id_noticia', $id_noticia)->count();
    
                $cantidad = NoticiaLike::where('FK_id_noticia', $id_noticia)->count();
                
                if($revision == 0){
                    //SETTEO LOS VALORES
                    $like->FK_id_noticia = (int)$id_noticia;
                    $like->FK_id_user = $user->id;
    
                    //GUARDO EL LIKE
                    $like->save();
                    
                    //Historial de Like a Noticia
                    $loghistory = new UserHistorial();
                    $loghistory->accion = "Like a Noticia: ".substr($revision->noticia->titulo, 0, 230)."...";
                    $loghistory->fecha_registro = date('Y-m-d');
                    $loghistory->FK_id_user = Auth::user()->id;
                    $loghistory->save();

                    DB::commit();
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'like' => $like,
                        'cantidad' => $cantidad + 1,
                        'message' => "Has dado like con exito"
                    ]);
                }
                else{
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'message' => "El like ya existe",
                        'cantidad' => $cantidad,
                    ]);
                }
            }
            catch(\Exception $e){
                DB::rollback();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'error' => "Fallo en el proceso de dar like: ".$e->getMessage()
                ]);
            }
        }
        
        //dislike de la noticia
        public function noticiadislike($id_noticia){
            DB::beginTransaction();
            try{
                //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
                $user = Auth::user();
    
                //REVISO SI YA TIENE LIKE
                $like = NoticiaLike::where('FK_id_user', $user->id)->where('FK_id_noticia', $id_noticia)->first();
    
                $cantidad = NoticiaLike::where('FK_id_noticia', $id_noticia)->count();
    
                if($like){
    
                    //BORRO EL LIKE
                    $like->delete();

                    //Historial de Dislike a Noticia
                    $loghistory = new UserHistorial();
                    $loghistory->accion = "Dislike a Noticia: ".substr($like->noticia->titulo, 0, 230)."...";
                    $loghistory->fecha_registro = date('Y-m-d');
                    $loghistory->FK_id_user = Auth::user()->id;
                    $loghistory->save();
                    
                    DB::commit();
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'dislike' => $like,
                        'cantidad' => $cantidad - 1,
                        'message' => 'Has dado dislike correctamente'
                    ]);
                }
                else{
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'message' => "La persona no posee likes colocados",
                        'cantidad' => $cantidad,
                    ]);
                }
            }
            catch(\Exception $e){
                DB::rollback();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'error' => "Fallo en el proceso de dar like: ".$e->getMessage(),
                ]);
            }
        }
    //FIN NOTICIAS

    //BLOG | MURO
        public function blog_patient(){  
            $muros = Muro::orderBy('created_at', 'desc')->get();

            $muros = $muros->map(function ($muro, $key) {
                $muro->cantidad = $muro->likes->count();
                return $muro;
            });

            $trending = $muros->sortByDesc('cantidad')->first();
            
            //Historial de Visualización de Muros
            /* $loghistory = new UserHistorial();
            $loghistory->accion = "Visualización de Blogs";
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = Auth::user()->id;
            $loghistory->save(); */

            return view('patient.blog.index', compact('muros', 'trending'));
        }

        //like al muro
        public function murolike($id_muro){
            //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
            DB::beginTransaction();
            try{
                $user = Auth::user();
    
                $like = new MuroLike();
    
                //REVISO SI YA TIENE LIKE
                $revision = MuroLike::where('FK_id_user', $user->id)->where('FK_id_muro', $id_muro)->count();
    
                $cantidad = MuroLike::where('FK_id_muro', $id_muro)->count();
                
                if($revision == 0){
                    //SETTEO LOS VALORES
                    $like->FK_id_muro = (int)$id_muro;
                    $like->FK_id_user = $user->id;
    
                    //GUARDO EL LIKE
                    $like->save();

                    //Historial de Like a muro
                    $loghistory = new UserHistorial();
                    $loghistory->accion = "Like a Muro | Código: ".$id_muro;
                    $loghistory->fecha_registro = date('Y-m-d');
                    $loghistory->FK_id_user = Auth::user()->id;
                    $loghistory->save();
    
                    DB::commit();
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'like' => $like,
                        'cantidad' => $cantidad + 1,
                        'message' => "Has dado like con exito"
                    ]);
                }
                else{
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'message' => "El like ya existe",
                        'cantidad' => $cantidad,
                    ]);
                }
            }
            catch(\Exception $e){
                DB::rollback();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'error' => "Fallo en el proceso de dar like: ".$e->getMessage()
                ]);
            }
        }
        
        //dislike al muro
        public function murodislike($id_muro){
            DB::beginTransaction();
            try{
                //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
                $user = Auth::user();
    
                //REVISO SI YA TIENE LIKE
                $like = MuroLike::where('FK_id_user', $user->id)->where('FK_id_muro', $id_muro)->first();
    
                $cantidad = MuroLike::where('FK_id_muro', $id_muro)->count();
    
                if($like){
    
                    //BORRO EL LIKE
                    $like->delete();
                    
                    //Historial de Dislike a muro
                    $loghistory = new UserHistorial();
                    $loghistory->accion = "Dislike a Muro | Código: ".$id_muro;
                    $loghistory->fecha_registro = date('Y-m-d');
                    $loghistory->FK_id_user = Auth::user()->id;
                    $loghistory->save();

                    DB::commit();
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'dislike' => $like,
                        'cantidad' => $cantidad - 1,
                        'message' => 'Has dado dislike correctamente'
                    ]);
                }
                else{
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'message' => "La persona no posee likes colocados",
                        'cantidad' => $cantidad,
                    ]);
                }
            }
            catch(\Exception $e){
                DB::rollback();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'error' => "Fallo en el proceso de dar like: ".$e->getMessage(),
                ]);
            }
        }

        //store de comentario
        public function comentarioStore(Request $request, $id_muro){
            DB::beginTransaction();
            try{
                $validate = Validator::make($request->all(), [
                    'comentario' => 'required|string|max:255',
                ], $this->mensajes_error);

                if($validate->fails()){
                    return Redirect::back()->withErrors($validate)->withInput();
                }
                
                $muro = Muro::find($id_muro);
                
                if(!$muro)
                    return Redirect::back()->with('error', 'El muro no fue encontrado');

                $comment = new MuroComentario();
                $comment->comentario = $request->comentario;
                $comment->FK_id_user = Auth::user()->id;
                $comment->FK_id_muro = $id_muro;
                $comment->save();

                //Historial de store comentario
                $loghistory = new UserHistorial();
                $loghistory->accion = "Nuevo Comentario a Muro | Código: ".$id_muro;
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = Auth::user()->id;
                $loghistory->save();

                DB::commit();
                return Redirect::back()->with('create', 'Comentario Añadido Exitosamente');
            }catch(\Exception $e){
                DB::rollback();
                return Redirect::back()->with('error', 'El Comentario no pudo ser agregado: '.$e->getMessage());
            }
        }

        //delete de comentario
        public function comentarioDelete($id_comentario){
            
            DB::beginTransaction();
            try{
                $comentario = MuroComentario::find($id_comentario);

                if(!$comentario)
                    return response()->json([
                        'message' => "El comentario no fue encontrado, por favor intentalo más tarde"
                    ]);

                $comentario->delete();

                //Historial de delete comentario
                $loghistory = new UserHistorial();
                $loghistory->accion = "Delete de Comentario en Muro | Código: ".$comentario->muro->id_muro;
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = Auth::user()->id;
                $loghistory->save();

                DB::commit();
                return response()->json([
                    'delete' => true,
                    'message' => "El comentario se ha eliminado exitosamente"
                ]);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json([
                    'message' => "Error al eliminar comentario, intentelo más tarde"
                ]);
            }
        }
    //FIN BLOG | MURO
}
