<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\Notificaciones;
use App\Models\UserNotificacion;

class NotificacionController extends Controller
{
    //Marcar como leido una notificación
    public function marcarLeido($id_notificacion = null){
        
        DB::beginTransaction();
        try{
            if(!$id_notificacion)
                return response()->json([
                    'status' => 'error',
                    'message' => 'El id de notificación no pudo ser procesada, intentelo más tarde',
                ], 200);

            UserNotificacion::where('id_user_notificaciones',$id_notificacion)->
                              update(['read_at' => date('Y-m-d H:i:s')]);

            DB::commit();
            return response()->json([
                'status' => 'success',
                'read' => true,
                'message' => 'Notificación leida',
            ], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al marcar leida la notificación',
                'errores' => $e->getMessage()
            ], 500);
        }

    }

    //Marcar como leido todas las notificaciones
    public function marcartodo(){
        
        DB::beginTransaction();
        try{
            UserNotificacion::where('FK_id_user',auth()->user()->id)->
                              update(['read_at' => date('Y-m-d H:i:s')]);

            DB::commit();
            return response()->json([
                'status' => 'success',
                'read' => true,
                'message' => 'Todas las notificaciones fueron marcadas como leidas',
            ], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al marcar todas las notificaciones como leida',
                'errores' => $e->getMessage()
            ], 200);
        }

    }

    //Eliminar el global o la data general de una notificación
    public function deleteGeneral(){
        $notificaciones = Notificaciones::all();
        
        foreach($notificaciones as $notificacion){
            if($notificacion->admins->isEmpty() && $notificacion->users->isEmpty())
                $notificacion->delete();
        }
    }

    //Eliminar una sola notificación
    public function deleteone($id_notificacion = null){
        
        DB::beginTransaction();
        try{

            if(!$id_notificacion)
                return response()->json([
                    'status' => 'error',
                    'message' => 'El id de notificación no pudo ser procesada, intentelo más tarde',
                ], 200);

            $usernot = UserNotificacion::find($id_notificacion);

            if(!$usernot)
                return response()->json([
                    'status' => 'error',
                    'message' => 'La Notificación no pudo ser encontrada',
                ], 200);

            $usernot->delete();

            //Revisamos si la notificación se puede eliminar completamente
            $notificacion = $usernot->notificacion;
            
            if($notificacion->admins->isEmpty() && $notificacion->users->isEmpty())
                $notificacion->delete();

            DB::commit();
            return response()->json([
                'status' => 'success',
                'delete' => true,
                'message' => 'La Notificación fue eliminada correctamente',
            ], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' => 'La Notificación no pudo ser eliminada, intentalo más tarde',
                'errores' => $e->getMessage()
            ], 200);
        }

    }

    //Eliminar todas las notificaciones
    public function deletetodo(){
        
        DB::beginTransaction();
        try{
            UserNotificacion::where('FK_id_user',auth()->user()->id)->delete();

            $this->deleteGeneral();

            DB::commit();
            return response()->json([
                'status' => 'success',
                'delete' => true,
                'message' => 'Todas las notificaciones fueron eliminadas',
            ], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' => 'Las Notificaciones no pudieron ser eliminadas, intentelo más tarde',
                'errores' => $e->getMessage()
            ], 200);
        }

    }
}
