<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\CapacitacionUser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Http;

//MODELOS
use App\Models\User;
use App\Models\Cuestionario;
use App\Models\CuestionarioUser;
use App\Models\Muro;
use App\Models\MuroComentario;
use App\Models\MuroLike;
use App\Models\Noticia;
use App\Models\NoticiaLike;
use App\Models\Pregunta;
use App\Models\Premio;
use App\Models\PremioCanjeado;
use App\Models\PremioCategoria;
use App\Models\PremioEntrega;
use App\Models\Producto;
use App\Models\Puntos;
use App\Models\Respuesta;
use App\Models\RespuestaUser;
use App\Models\UserHistorial;

//Helper o Usables
use App\Helper\Notificacion;

class ProfessionalController extends Controller
{

    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'file' => 'El dato debe llegar como un archivo',
        'mimes' => 'El archivo debe llegar en formato png, jpg, o jpeg',
        'imagen.max' => 'La imagen no puede ser mayor a 2Mb',
        'boolean' => 'Debes enviar el dato como true (1) o false (0)',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'contenido.max' => "El dato no debe ser mayor a 65535 carácteres",
    ];

    /////////////////////////////////////////////VISUALES////////////////////
    
    //TUTORIAL
        public function tutorial_professional(){
            return view('professional.tutorial.index');
        }
    //FIN TUTORIAL
    
    //DASHBOARD
        public function portal_professional(){  
            $user = Auth::user();
            $productos = Producto::all();

            return view('professional.portal.index', compact('user', 'productos'));
        } 
    //FIN DASHBOARD

    //PERFIL
        //vista del inicio de perfil
        public function perfil_professional(){  
            $user = Auth::user();
            return view('professional.perfil.index', compact('user'));
        } 

        //vista de editar perfil
        public function perfil_edit_professional(){  
            return view('professional.perfil.edit');
        } 

        //certificados - descargar que creo que son cuestionarios
        public function perfil_certificado_professional($id_cuestionario){  
            $cuestionario = CuestionarioUser::find($id_cuestionario);

            if(!$cuestionario)
                return redirect()->route('perfil_professional.index')->with('message', 'El certificado a descargar no fue encontrado');

            return view('professional.perfil.certificado', compact('cuestionario'));
        }
    //FIN PERFIL

    //PRACTICAS
        public function practice_professional($id_producto = null, $id_capacitacion = null){  

            $producto = Producto::find($id_producto);

            if(!$producto)
                return redirect()->route('portal_professional.index')->with('message', 'El producto seleccionado no fue encontrado');

            if(!$producto->capacitaciones->count() > 0)
                return redirect()->route('portal_professional.index')->with('message', 'El producto seleccionado no posee capacitaciones cargadas aún');

            return view('professional.practice.index', compact('producto', 'id_capacitacion'));
        }

        public function openArchive($filename = null, $id = null){
            
            $url = env('WEB_PANEL_URL')."capacitacion/getArchive/".$filename;
            
            $response = Http::get($url);

            $extension = pathinfo($response)['extension'];

            //Arreglo de que el User haya visto realmente la capacitación
            $data["FK_id_user"] = Auth::user()->id;
            $data["FK_id_capacitacion_documento"] = $id;
            CapacitacionUser::firstOrCreate($data);

            //Historial de Visualización de Documento de Capacitación
            $loghistory = new UserHistorial();
            $loghistory->accion = "Lectura Documento de Capacitación (Pdf/Video)";
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = Auth::user()->id;
            $loghistory->save();

            if($extension == "pdf")
                $content = 'application/pdf';
            elseif($extension == "mp4"){
                $response = basename($response);
                return view('professional.practice.stream',compact('response'));
            }
            else
                $content = 'image/'.$extension;

            return Response(file_get_contents($response), 200, [ 
                'Content-Type' => $content,
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]);  
        }
    //FIN PRACTICAS

    //CUESTIONARIOS
        public function cuestionario_professional($id_capacitacion = null){  

            $cuestionario = Cuestionario::where('FK_id_capacitacion', $id_capacitacion)->first();

            if(!$cuestionario){
                return Redirect::back()->with('message', 'El reto aun no posee cuestionario afiliado');
            }
            
            //Revisamos si ya vio todas las capacitaciones (retos/entrenamientos)
            $user = Auth::user();
            $retos = $cuestionario->capacitacion->entrenamientos;
            $vistas = $user->capacitacionesSeen;

            $retos = $retos->map(function ($item, $key) {
                return $item->id_capacitacion_documento;
            });
            
            $vistas = $vistas->map(function ($item, $key) {
                return $item->FK_id_capacitacion_documento;
            });
            
            $diff = $retos->diff($vistas);

            if($diff->count() != 0){
                return Redirect::back()->with('message', 'Porfavor terminar de ver todos los documentos de entrenamientos / retos para hacer el cuesitonario');
            }

            if(!$cuestionario->preguntas->count() > 0){
                return Redirect::back()->with('message', 'El cuestionario aun se esta ensamblando, por favor espera a otro día a que este disponible');
            }

            //Historial de Visualización de Capacitacion
            $loghistory = new UserHistorial();
            $loghistory->accion = "Visualización de Cuestionario | Código: ".$cuestionario->id_cuestionario;
            $loghistory->fecha_registro = date('Y-m-d');
            $loghistory->FK_id_user = Auth::user()->id;
            $loghistory->save();

            return view('professional.cuestionario.index', compact('cuestionario'));
        }

        public function getImageRespuesta($filename = null){
            $url = env('WEB_PANEL_URL')."cuestionario/".$filename;

            $response = Http::get($url);
            
            return $response;
        }

        public function storeCuestionario(Request $request){
            
            $validaciones = [
                'id_cuestionario' => 'required',

                'imagenes.*.pregunta' => 'required|numeric', 
                'imagenes.*.respuesta' => 'required|numeric', 

                'vf.*.respuesta' => 'required|numeric', 
                'vf.*.opcion' => 'required|string', 

                'multiple.*.pregunta' => 'required|numeric', 
                'multiple.*.respuesta' => 'required|numeric', 

                /* 'relaciones.*.respuesta' => 'required|numeric', 
                'relaciones.*.opcion' => 'required|numeric',  */
            ];

            DB::beginTransaction();
            try{
                $validate = Validator::make($request->all(), $validaciones);
                
                if($validate->fails()){
                    return response()->json(['errors'=>$validate->errors()->all()],200);
                }

                $user = Auth::user();
                $quest = Cuestionario::find($request->id_cuestionario);

                if(!$quest)
                    return response()->json([
                        'fallo'=>'El cuestionario no se encontro dentro de la base de datos',
                        'id' => $request->id_cuestionario,
                    ],200);

                $cuestionario = new CuestionarioUser();
                $cuestionario->titulo = $quest->titulo;
                $cuestionario->descripcion = $quest->descripcion;
                $cuestionario->tipo = "Cuestionario";
                $cuestionario->puntos = 0;
                $cuestionario->FK_id_cuestionario = $quest->id_cuestionario;
                $cuestionario->FK_id_user = $user->id;
                $cuestionario->save();

                $total_puntos = $quest->puntuacion;
                $total_correctas = 0;
                $total_respuestas = 0;

                //Evaluamos las de opcion multiple + imagen
                    if($request->imagenes){
                        foreach ($request->imagenes as $key => $imagenes) {
                            
                            $pregunta = Pregunta::find($imagenes["pregunta"]);
                            
                            if(!$pregunta)
                                return response()->json([
                                    "message" => "La pregunta respondida no fue encontrada, probablemente se módifico la misma",
                                    "change" => true,
                                ],200);
                            
                            $respuesta = $pregunta->respuestas->where('id_respuesta', $imagenes["respuesta"])->first();

                            $QUser = new RespuestaUser();
                            $QUser->respuesta = $respuesta->respuesta;
                            $QUser->pregunta = $pregunta->pregunta;
                            $QUser->correcta = $respuesta->correcta;
                            $QUser->FK_id_cuestionario_user = $cuestionario->id_cuestionario_user;
                            $QUser->save();
                            
                            //Sumamos totales correctas y respuestas si esta bien
                            $respuesta->correcta ? $total_correctas++ : "";
                            $total_respuestas++;
                        }
                    }

                //Evaluamos las de verdadero y falso
                    if($request->vf){
                        foreach ($request->vf as $key => $vf) {
                            $respuesta = Respuesta::find($vf["respuesta"]);
                            
                            if(!$respuesta)
                                return response()->json([
                                    "message" => "La respuesta respondida no fue encontrada, probablemente se módifico la misma",
                                    "change" => true,
                                ],200);
                            
                            $QUser = new RespuestaUser();
                            $QUser->respuesta = $vf["opcion"];
                            $QUser->pregunta = $respuesta->respuesta;
                            $answer = $respuesta->correcta ? "true" : "false";
                            $QUser->correcta = $answer == $vf["opcion"] ? 1 : 0;
                            $QUser->FK_id_cuestionario_user = $cuestionario->id_cuestionario_user;
                            $QUser->save();
                            
                            //Sumamos totales correctas y respuestas si esta bien
                            $QUser->correcta ? $total_correctas++ : "";
                            $total_respuestas++;
                        }
                    }

                //Evaluamos las de opcion multiple normal
                    if($request->multiple){
                        foreach ($request->multiple as $key => $multiple) {
                            
                            $pregunta = Pregunta::find($multiple["pregunta"]);
                            
                            if(!$pregunta)
                                return response()->json([
                                    "message" => "La pregunta respondida no fue encontrada, probablemente se módifico la misma",
                                    "change" => true,
                                ],200);
                            
                            $respuesta = $pregunta->respuestas->where('id_respuesta', $multiple["respuesta"])->first();

                            $QUser = new RespuestaUser();
                            $QUser->respuesta = $respuesta->respuesta;
                            $QUser->pregunta = $pregunta->pregunta;
                            $QUser->correcta = $respuesta->correcta;
                            $QUser->FK_id_cuestionario_user = $cuestionario->id_cuestionario_user;
                            $QUser->save();
                            
                            //Sumamos totales correctas y respuestas si esta bien
                            $respuesta->correcta ? $total_correctas++ : "";
                            $total_respuestas++;
                        }
                    }

                //Evaluamos la de relación, respuesta + opción


                //Ahora evaluamos los puntos generados y grabamos y actualizamos
                    //Puntos de CuestionUser
                    $cuestionario->puntos = ($total_correctas * $total_puntos) / $total_respuestas;
                    $cuestionario->save();

                    //Puntos de User
                    $user->total_puntos += $cuestionario->puntos;
                    $user->save();

                    //Historial Puntos
                    $notPuntos = new Puntos();
                    $notPuntos->descripcion = "Ganados por cuestionario (".$quest->titulo.") Código: ".$quest->id_cuestionario;
                    $notPuntos->cantidad = $cuestionario->puntos;
                    $notPuntos->FK_id_user = $user->id;
                    $notPuntos->save();

                //Historial de Acciones
                    $historial = new UserHistorial();
                    $historial->accion = "Cuestionario Respondido | Código: ".$quest->id_cuestionario;
                    $historial->fecha_registro = date('Y-m-d');
                    $historial->FK_id_user = $user->id;
                    $historial->save();

                //Mensajes de notificacion

                DB::commit();
                return response()->json([
                    "puntos" => $notPuntos->cantidad,
                    "total" => $total_respuestas,
                    "correctas" => $total_correctas,
                    "success" => true,
                ],200);
            }
            catch(\Exception $e){
                DB::rollback();
                return response()->json([
                    'fallo'=>'No pudo almacenarse el cuestionario, intentelo más tarde | Error: '.$e->getMessage()
                ],200);
            }
        }
    //FIN CUESTIONARIOS

    //INSIGNIAS
        public function insignia_professional(){  
            return view('professional.insignia.index');
        }
    //FIN INSIGNIAS
    
    //BLOGS
        public function muro_professional(){  

            $muros = Muro::orderBy('created_at', 'desc')->get();

            $muros = $muros->map(function ($muro, $key) {
                $muro->cantidad = $muro->likes->count();
                return $muro;
            });

            $trending = $muros->sortByDesc('cantidad')->first();

            return view('professional.muro.index', compact('muros', 'trending'));
        }

        //like al muro
        public function murolike($id_muro){
            //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
            DB::beginTransaction();
            try{
                $user = Auth::user();
    
                $like = new MuroLike();
    
                //REVISO SI YA TIENE LIKE
                $revision = MuroLike::where('FK_id_user', $user->id)->where('FK_id_muro', $id_muro)->count();
    
                $cantidad = MuroLike::where('FK_id_muro', $id_muro)->count();
                
                if($revision == 0){
                    //SETTEO LOS VALORES
                    $like->FK_id_muro = (int)$id_muro;
                    $like->FK_id_user = $user->id;
    
                    //GUARDO EL LIKE
                    $like->save();

                    //Historial de Like a muro
                    $loghistory = new UserHistorial();
                    $loghistory->accion = "Like a Muro | Código: ".$id_muro;
                    $loghistory->fecha_registro = date('Y-m-d');
                    $loghistory->FK_id_user = Auth::user()->id;
                    $loghistory->save();
    
                    DB::commit();
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'like' => $like,
                        'cantidad' => $cantidad + 1,
                        'message' => "Has dado like con exito"
                    ]);
                }
                else{
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'message' => "El like ya existe",
                        'cantidad' => $cantidad,
                    ]);
                }
            }
            catch(\Exception $e){
                DB::rollback();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'error' => "Fallo en el proceso de dar like: ".$e->getMessage()
                ]);
            }
        }
        
        //dislike al muro
        public function murodislike($id_muro){
            DB::beginTransaction();
            try{
                //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
                $user = Auth::user();
    
                //REVISO SI YA TIENE LIKE
                $like = MuroLike::where('FK_id_user', $user->id)->where('FK_id_muro', $id_muro)->first();
    
                $cantidad = MuroLike::where('FK_id_muro', $id_muro)->count();
    
                if($like){
    
                    //BORRO EL LIKE
                    $like->delete();
                    
                    //Historial de Dislike a muro
                    $loghistory = new UserHistorial();
                    $loghistory->accion = "Dislike a Muro | Código: ".$id_muro;
                    $loghistory->fecha_registro = date('Y-m-d');
                    $loghistory->FK_id_user = Auth::user()->id;
                    $loghistory->save();

                    DB::commit();
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'dislike' => $like,
                        'cantidad' => $cantidad - 1,
                        'message' => 'Has dado dislike correctamente'
                    ]);
                }
                else{
                    //JSON PORQUE ESTO SE EJECUTA CON AJAX
                    return response()->json([
                        'message' => "La persona no posee likes colocados",
                        'cantidad' => $cantidad,
                    ]);
                }
            }
            catch(\Exception $e){
                DB::rollback();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'error' => "Fallo en el proceso de dar like: ".$e->getMessage(),
                ]);
            }
        }

        //store de comentario
        public function comentarioStore(Request $request, $id_muro){
            DB::beginTransaction();
            try{
                $validate = Validator::make($request->all(), [
                    'comentario' => 'required|string|max:255',
                ], $this->mensajes_error);

                if($validate->fails()){
                    return Redirect::back()->withErrors($validate)->withInput();
                }
                
                $muro = Muro::find($id_muro);
                
                if(!$muro)
                    return Redirect::back()->with('error', 'El muro no fue encontrado');

                $comment = new MuroComentario();
                $comment->comentario = $request->comentario;
                $comment->FK_id_user = Auth::user()->id;
                $comment->FK_id_muro = $id_muro;
                $comment->save();

                //Historial de store comentario
                $loghistory = new UserHistorial();
                $loghistory->accion = "Nuevo Comentario a Muro | Código: ".$id_muro;
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = Auth::user()->id;
                $loghistory->save();

                DB::commit();
                return Redirect::back()->with('create', 'Comentario Añadido Exitosamente');
            }catch(\Exception $e){
                DB::rollback();
                return Redirect::back()->with('error', 'El Comentario no pudo ser agregado: '.$e->getMessage());
            }
        }

        //delete de comentario
        public function comentarioDelete($id_comentario){
            
            DB::beginTransaction();
            try{
                $comentario = MuroComentario::find($id_comentario);

                if(!$comentario)
                    return response()->json([
                        'message' => "El comentario no fue encontrado, por favor intentalo más tarde"
                    ]);

                $comentario->delete();

                //Historial de delete comentario
                $loghistory = new UserHistorial();
                $loghistory->accion = "Delete de Comentario en Muro | Código: ".$comentario->muro->id_muro;
                $loghistory->fecha_registro = date('Y-m-d');
                $loghistory->FK_id_user = Auth::user()->id;
                $loghistory->save();

                DB::commit();
                return response()->json([
                    'delete' => true,
                    'message' => "El comentario se ha eliminado exitosamente"
                ]);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json([
                    'message' => "Error al eliminar comentario, intentelo más tarde"
                ]);
            }
        }
    //FIN BLOGS

    //PREMIOS
        //Listado de premios por categoria
        public function premio_professional(){  
            $categorias = PremioCategoria::all();

            return view('professional.premio.index', compact('categorias'));
        }

        //Categoria seleccionada
        public function premioselect_professional($id_categoria = null){
            
            $categoria = PremioCategoria::find($id_categoria);

            if(!$categoria)
                return redirect()->route('premio_profesional.index');

            $premios = Premio::where('FK_categoria', $id_categoria)->get();

            return view('professional.premio.premioselect', compact('premios', 'categoria'));
        }

        //Información de premio seleccionado para canjear o no
        public function premio_detail($id_premio = null){
            try{
                $premio = Premio::find($id_premio);
    
                if(!$premio)
                    return response()->json([
                        'message' => "El premio no pudo ser encontrado"
                    ]);
                
                $premio->imagen = basename($premio->imagen);
                $premio->caracteristicas;
                $user = Auth::user();

                return response()->json([
                    'user_puntos' => $user->total_puntos,
                    'premio' => $premio,
                    'message' => "Premio encontrado exitosamente"
                ]);
            }
            catch(\Exception $e){
                return response()->json([
                    'message' => "Error al consultar premio: ".$e->getMessage()
                ]);
            }
        }

        //Canjear premio seleccionado
        public function premio_canjeo(Request $request){

            DB::beginTransaction();
            try{
                $validate = Validator::make($request->all(), [
                    'cantidad' => 'required|numeric|min:0',
                    'id_premio' => 'required',
                    'telefono' => 'required|string|max:255',
                    'cedula' => 'required|numeric|digits_between:4,20',
                    'receptor' => 'string|max:255',
                    'correo' => 'string|max:255',
                    'direccion' => 'string|max:255',
                    'adicional' => 'string|max:255',
                ]);

                if($validate->fails()){
                    return response()->json(['errors'=>$validate->errors()->all()]);
                }

                $premio = Premio::find($request->id_premio);

                if(!$premio)
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Premio no encontrado',
                        'fallo' => 'El premio a canjear no se encuentra registrado',
                    ], 200);

                $data = $request->all();
                
                if($data["cantidad"] > $premio->stock)
                    return response()->json([
                        'status' => 'warning',
                        'message' => 'Stock no disponible',
                        'fallo' => 'La cantidad solicitada supera al stock actual del premio',
                    ], 200);
                
                if(Auth::user()->total_puntos < $premio->puntos * $data["cantidad"])
                    return response()->json([
                        'status' => 'warning',
                        'message' => 'Puntos insuficientes',
                        'fallo' => 'No posees la cantidad de puntos suficientes para redimir el premio',
                    ], 200);

                //Creamos el objeto del canjeo
                $canjeado = new PremioCanjeado();
                $canjeado->nombre = $premio->nombre;
                $canjeado->marca = $premio->marca;
                $canjeado->categoria = $premio->categoria->categoria;
                $canjeado->descripcion = $premio->descripcion;
                $canjeado->cantidad = $data["cantidad"];
                $canjeado->total_puntos = $premio->puntos * $data["cantidad"];
                $canjeado->FK_id_premio = $request->id_premio;
                $canjeado->FK_id_user = Auth::user()->id;
                $canjeado->save();

                //Creamos el objeto con los datos de entrega
                $data["FK_id_canjeado"] = $canjeado->id_canjeado;
                $entrega = PremioEntrega::create($data);

                //Actualizamos el stock del premio
                $premio->stock -= $data["cantidad"];
                $premio->update();

                //Grabamos el historial del cliente
                $historial = new UserHistorial();
                $historial->accion = "Registro de Premio Canjeado: ".$canjeado->nombre;
                $historial->fecha_registro = date('Y-m-d');
                $historial->FK_id_user = Auth::user()->id;
                $historial->save();

                //Modificamos los puntos del usuario
                $user = Auth::user();
                $user->total_puntos -= $premio->puntos * $data["cantidad"];
                $user->update();
                
                //Notificación del server
                $dataClient = [
                    "nombre" => '¡NUEVO PREMIO '.strtoupper($canjeado->nombre).' CANJEADO! 👨🎁',
                    "tipo" => "Premio",
                    "descripcion" => 'En los días siguiente se ira actualizando el estado de canjeo de tu premio para la entrega del mismo',
                    "user" => Auth::user()->id,
                ];
    
                Notificacion::instance()->storeUniqueClient($dataClient);

                //Notificación por correo
                /* try{
                    $information = new \stdClass();
                    $information->asunto = 'Felicitaciones por Canjear tu Premio en DentApp';
                    $information->nombre = $canjeado->nombre;
                    $information->puntos = $canjeado->total_estrellas;
                    $information->cantidad = $canjeado->cantidad;
                    $information->direccion = $canjeado->direccion;

                    if(strtolower($premio->tag) == "viajes"){
                        Mail::to($cliente->correo)->queue(new PremioViaje($information));
                    }
                    elseif (strtolower($premio->tag) == "tecnología") {
                        Mail::to($cliente->correo)->queue(new PremioTecno($information));
                    }
                    elseif (strtolower($premio->tag) == "bonos digitales") {
                        Mail::to($cliente->correo)->queue(new PremioBono($information));
                    }
                }
                catch(\Exception $e){
                    "";
                } */

                //Notificación Push
                /* $title = "¡NUEVO PREMIO CANJEADO!";
                $text = "Tu Premio ".$canjeado->nombre." ha sido redimido exitosamente";
                event(new PremioEvent($title, $text, $canjeado->id_premio_canjeado)); */

                $nombre = strtoupper($user->profesional->nombre . ' ' . $user->profesional->apellido);
                $premio->imagen = basename($premio->imagen);

                DB::commit();
                return response()->json([
                    'success' => 'Premio Canjeado Exitosamente',
                    'canjeado' => $canjeado,
                    'entrega' => $entrega,
                    'premio' => $premio,
                    'ganador' => $nombre,
                ]);

            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Fallo interno al redimir',
                    'fallo' => $e->getMessage(),
                ], 200);
            }
        }
    //FIN PREMIOS
}