<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminNotificacion extends Model
{
    use HasFactory;

    protected $table='bc_admin_notificaciones';
    protected $primaryKey='id_admin_notificaciones';

    protected $fillable =[
        'id_admin_notificaciones',
        'FK_id_useradmin',
        'FK_id_notificaciones',
        'read_at',
    ];

    public function admin(){
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin')->withTrashed();
    }

    public function notificacion(){
        return $this->belongsTo('App\Models\Notificaciones', 'FK_id_notificaciones');
    }
}
