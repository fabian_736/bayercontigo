<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cuestionario extends Model
{
    use HasFactory;

    protected $table='bc_cuestionario';
    protected $primaryKey='id_cuestionario';

    protected $fillable =[
        'id_cuestionario',
        'titulo',
        'descripcion',
        'puntuacion',
        'estado',
        'FK_id_capacitacion',
    ];

    public function capacitacion(){
        return $this->belongsTo('App\Models\Capacitacion', 'FK_id_capacitacion');
    }

    public function preguntas(){
        return $this->hasMany('App\Models\Pregunta', 'FK_id_cuestionario')->orderBy(DB::raw('RAND()'));
    }

    public function cusuarios(){
        return $this->hasMany('App\Models\CuestionarioUser', 'FK_id_cuestionario');
    }
}
