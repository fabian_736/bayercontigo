<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaLike extends Model
{
    use HasFactory;

    protected $table='bc_noticia_like';
    protected $primaryKey='id_noticia_like';

    protected $fillable =[
        'id_noticia_like',
        'FK_id_user',
        'FK_id_noticia',
    ];

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }

    public function profesional(){
        return $this->belongsTo('App\Models\Profesional', 'FK_id_profesional');
    }

    public function noticia(){
        return $this->belongsTo('App\Models\Noticia', 'FK_id_noticia');
    }
}
