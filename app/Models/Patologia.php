<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patologia extends Model
{
    use HasFactory;

    protected $table='bc_patologia';
    protected $primaryKey='id_patologia';

    protected $fillable =[
        'id_patologia',
        'nombre',
    ];

    public function pacientes(){
        return $this->hasMany('App\Models\PacientePatologia', 'FK_id_patologia');
    }

    public function capacitaciones(){
        return $this->hasMany('App\Models\Capacitacion', 'FK_id_patologia')->orderBy('id_capacitacion','asc');
    }
}
