<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capacitacion extends Model
{
    use HasFactory;

    protected $table='bc_capacitacion';
    protected $primaryKey='id_capacitacion';

    protected $fillable =[
        'id_capacitacion',
        'titulo',
        'descripcion',
        'FK_id_patologia',
        'FK_id_producto',
    ];

    public function producto(){
        return $this->belongsTo('App\Models\Producto', 'FK_id_producto');
    }

    public function patologia(){
        return $this->belongsTo('App\Models\Patologia', 'FK_id_patologia');
    }

    public function entrenamientos(){
        return $this->hasMany('App\Models\CapacitacionDocumento', 'FK_id_capacitacion');
    }

    public function cuestionario(){
        return $this->hasOne('App\Models\Cuestionario', 'FK_id_capacitacion');
    }
}
