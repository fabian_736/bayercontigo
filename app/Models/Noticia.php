<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    use HasFactory;

    protected $table='bc_noticia';
    protected $primaryKey='id_noticia';

    protected $fillable =[
        'id_noticia',
        'titulo',
        'categoria', //EN PARTE DEL TIPO POR DENTRO NO LLEVARA SELECT
        'descripcion', 
        'contenido', //NO LO LLEVAN TODOS ASI QUE PUEDE IR NULL
        'estado',  //IRRELEVANTE (NO TIENE DEFAULT)
        'fecha_inicio', //IRRELEVANTE (NULLABLE)
        'fecha_fin',  //IRRELEVANTE (NULLABLE)
        'imagen',
        'documento',
        'FK_id_tipo', //VIENE A SER SI RECETA | TIP DE MODA | SALUD | OTROS
    ];

    public function tipo(){
        return $this->belongsTo('App\Models\NoticiaTipo', 'FK_id_tipo');
    }

    public function likes(){
        return $this->hasMany('App\Models\NoticiaLike', 'FK_id_noticia');
    }

    public function items(){
        return $this->hasMany('App\Models\NoticiaItem', 'FK_id_noticia');
    }

    public function directorios(){
        return $this->hasMany('App\Models\NoticiaDirectorio', 'FK_id_noticia')->with('profesional');
    }
}
