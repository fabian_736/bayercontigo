<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuestionarioUser extends Model
{
    use HasFactory;

    protected $table='bc_cuestionario_user';
    protected $primaryKey='id_cuestionario_user';

    protected $fillable =[
        'id_cuestionario_user',
        'titulo',
        'descripcion',
        'tipo',
        'puntos',
        'FK_id_cuestionario',
        'FK_id_user',
    ];

    public function cuestionario(){
        return $this->belongsTo('App\Models\Cuestionario', 'FK_id_cuestionario');
    }

    public function respuestas(){
        return $this->hasMany('App\Models\RespuestaUser', 'FK_id_cuestionario_user');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }
}
