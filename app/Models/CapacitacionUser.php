<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapacitacionUser extends Model
{
    use HasFactory;

    protected $table='bc_capacitacion_user';
    protected $primaryKey='id_capacitacion_user';

    protected $fillable =[
        'id_capacitacion_user',
        'FK_id_user',
        'FK_id_capacitacion_documento',
    ];

    public function entrenamiento(){
        return $this->belongsTo('App\Models\CapacitacionDocumento', 'FK_id_capacitacion_documento');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }
}
