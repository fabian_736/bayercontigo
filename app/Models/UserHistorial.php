<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserHistorial extends Model
{
    use HasFactory;

    protected $table='bc_historial_user';
    protected $primaryKey='id_historial_user';

    protected $fillable = [
        'id_historial_user', 
        'accion', 
        'fecha_registro',
        'FK_id_user',
    ];

    public function cliente(){
        return $this->belongsTo('App\Models\User', 'FK_id_user')->withTrashed();
    }
}
