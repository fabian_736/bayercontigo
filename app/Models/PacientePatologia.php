<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PacientePatologia extends Model
{
    use HasFactory;

    protected $table='bc_pa_patologia';
    protected $primaryKey='id_pa_patologias';

    protected $fillable =[
        'id_pa_patologias',
        'FK_id_paciente',
        'FK_id_patologia',
    ];

    public function paciente(){
        return $this->belongsTo('App\Models\Paciente', 'FK_id_paciente');
    }

    public function patologia(){
        return $this->belongsTo('App\Models\Patologia', 'FK_id_patologia');
    }
}
