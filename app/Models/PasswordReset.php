<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    use HasFactory;

    protected $table='password_resets';
    protected $primaryKey='id_password_reset';
    protected $fillable = [
        'id_password_reset',
        'tipo',
        'email', 
        'token',
    ];

    protected $hidden=[
        'id_password_reset',
        'created_at',
        'updated_at',
    ];
}
