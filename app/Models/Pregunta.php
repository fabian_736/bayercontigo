<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    use HasFactory;

    protected $table='bc_pregunta';
    protected $primaryKey='id_pregunta';

    protected $fillable =[
        'id_pregunta',
        'pregunta',
        'tipo',
        'puntuacion',
        'estado',
        'FK_id_cuestionario',
    ];

    public function cuestionario(){
        return $this->belongsTo('App\Models\Cuestionario', 'FK_id_cuestionario');
    }

    public function opciones(){
        return $this->hasMany('App\Models\RespuestaOpcion', 'FK_id_pregunta');
    }

    public function respuestas(){
        return $this->hasMany('App\Models\Respuesta', 'FK_id_pregunta');
    }
}
