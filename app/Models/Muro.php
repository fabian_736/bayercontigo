<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Muro extends Model
{
    use HasFactory;

    protected $table='bc_muro';
    protected $primaryKey='id_muro';

    protected $fillable =[
        'id_muro',
        'tipo',
        'contenido',
        'imagen',
        'FK_id_useradmin',
    ];

    public function creador(){
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin');
    }

    public function likes(){
        return $this->hasMany('App\Models\MuroLike', 'FK_id_muro');
    }

    public function comments(){
        return $this->hasMany('App\Models\MuroComentario', 'FK_id_muro');
    }
}
