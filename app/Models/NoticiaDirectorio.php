<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaDirectorio extends Model
{
    use HasFactory;

    protected $table='bc_noticia_directorio';
    protected $primaryKey='id_noticia_directorio';

    protected $fillable =[
        'id_noticia_directorio',
        'FK_id_profesional',
        'FK_id_noticia',
    ];

    public function noticia(){
        return $this->belongsTo('App\Models\Noticia', 'FK_id_noticia');
    }

    public function profesional(){
        return $this->belongsTo('App\Models\Profesional', 'FK_id_profesional')->with('user');
    }
}
