<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapacitacionDocumento extends Model
{
    use HasFactory;

    protected $table='bc_capacitacion_documento';
    protected $primaryKey='id_capacitacion_documento';

    protected $fillable =[
        'id_capacitacion_documento',
        'nombre',
        'documento',
        'video',
        'FK_id_capacitacion',
    ];

    public function capacitacion(){
        return $this->belongsTo('App\Models\Capacitacion', 'FK_id_capacitacion');
    }

    public function usuarios(){
        return $this->hasMany('App\Models\CapacitacionUser', 'FK_id_capacitacion_documento');
    }
}
