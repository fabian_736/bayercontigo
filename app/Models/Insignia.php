<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Insignia extends Model
{
    use HasFactory;

    protected $table='bc_insignias';
    protected $primaryKey='id_insignia';

    protected $fillable =[
        'id_insignia',
        'nombre',
        'imagen',
    ];

    public function profesionales(){
        return $this->hasMany('App\Models\ProfesionalInsignia', 'FK_id_insignia');
    }
}
