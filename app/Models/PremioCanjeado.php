<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioCanjeado extends Model
{
    use HasFactory;

    protected $table='bc_premio_canjeado';
    protected $primaryKey='id_canjeado';

    protected $fillable =[
        'id_canjeado',
        'nombre',
        'marca',
        'categoria',
        'descripcion',
        'cantidad',
        'total_puntos',
        'FK_id_premio',
        'FK_id_user',
    ];

    public function premio(){
        return $this->belongsTo('App\Models\Premio', 'FK_id_premio');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }

    public function entrega(){
        return $this->hasOne('App\Models\PremioEntrega', 'FK_id_canjeado');
    }
}
