<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Puntos extends Model
{
    use HasFactory;

    protected $table='bc_puntos';
    protected $primaryKey='id_puntos';

    protected $fillable =[
        'id_puntos',
        'descripcion',
        'cantidad',
        'estado',
        'used',
        'fecha_vencimiento',
        'FK_id_user',
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }
}
