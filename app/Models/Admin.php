<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $table='bc_useradmin';
    protected $primaryKey='id_useradmin';
    protected $guarded = ['id_useradmin'];

    protected $fillable =[
        'id_useradmin',
        'nombre',
        'apellido',
        'email',
        'password',
        'email_verified_at',
        'email_code',
        'estado',
        'avatar',
        //PERMISOS
        'p_store',
        'p_update',
        'p_destroy',
        'p_info',
        'p_capacitaciones',
        'p_cuestionarios',
        'p_premios',
        //FIN PERMISOS
        'remember_token',
        'FK_id_adminrol',
    ];

    protected $hidden=[
        'password',
        'remember_token',
    ];

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function roles(){
        return $this->belongsTo('App\Models\Rol', 'FK_id_adminrol');
    }

    /* APARTADO DE LAS NOTIFICACIONES */
        public function allnotifications(){
            return $this->hasMany('App\Models\AdminNotificacion', 'FK_id_useradmin')->orderBy('created_at','desc');;
        }

        public function notificaciones(){
            return $this->hasMany('App\Models\AdminNotificacion', 'FK_id_useradmin')->whereNotNull('read_at')->orderBy('created_at','desc');;
        }

        public function unreadnotification(){
            return $this->hasMany('App\Models\AdminNotificacion', 'FK_id_useradmin')->whereNull('read_at')->orderBy('created_at','desc');
        }

        public function ownotification(){
            return $this->hasMany('App\Models\Notificaciones', 'FK_id_useradmin');
        }
    /* FIN APARTADO DE LAS NOTIFICACIONES */

    /* PANEL INFORMATIVO */
    public function post(){
        return $this->hasMany('App\Models\Muro', 'FK_id_useradmin');
    }

    public function muroLikes(){
        return $this->hasMany('App\Models\MuroLike', 'FK_id_useradmin');
    }

    public function comentarios(){
        return $this->hasMany('App\Models\MuroComentario', 'FK_id_useradmin');
    }
    /* FIN PANEL INFORMATIVO */
}
