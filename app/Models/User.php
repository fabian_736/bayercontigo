<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $table='users';
    protected $primaryKey='id';
    protected $fillable = [
        'pais',
        'email',
        'password',
        'email_verified',
        'email_verified_at',
        'email_code',
        'estado',
        'tipo',
        'ip',
        'total_puntos',
        'avatar',
        'cambiar',
        'fecha_cambio',
        'tutorial',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function historialpass(){
        return $this->hasMany('App\Models\UserPassword', 'FK_id_user')->orderBy('registro','desc');
    }

    public function acciones(){
        return $this->hasMany('App\Models\UserHistorial', 'FK_id_user')->orderBy('fecha_registro','desc');
    }

    public function paciente(){
        return $this->hasOne('App\Models\Paciente', 'FK_id_user');
    }

    public function profesional(){
        return $this->hasOne('App\Models\Profesional', 'FK_id_user');
    }

    public function puntos(){
        return $this->hasMany('App\Models\Puntos', 'FK_id_user')->orderBy('created_at','desc');
    }

    /* APARTADO DE LAS NOTIFICACIONES */
        public function notificaciones(){
            return $this->hasMany('App\Models\UserNotificacion', 'FK_id_user')->whereNotNull('read_at')->orderBy('created_at','desc');;
        }

        public function unreadnotification(){
            return $this->hasMany('App\Models\UserNotificacion', 'FK_id_user')->whereNull('read_at')->orderBy('created_at','desc');
        }
    /* FIN APARTADO DE LAS NOTIFICACIONES */

     /* PANEL INFORMATIVO */
        public function noticiaLikes(){
            return $this->hasMany('App\Models\NoticiaLike', 'FK_id_user');
        }

        public function muroLikes(){
            return $this->hasMany('App\Models\MuroLike', 'FK_id_user');
        }

        public function comentarios(){
            return $this->hasMany('App\Models\MuroComentario', 'FK_id_user');
        }
    /* FIN PANEL INFORMATIVO */

    /* PANEL DE CAPACITACIONES + CUESTIONARIOS */
        public function capacitacionesSeen(){
            return $this->hasMany('App\Models\CapacitacionUser', 'FK_id_user');
        }

        public function cuestionarios(){
            return $this->hasMany('App\Models\CuestionarioUser', 'FK_id_user');
        }
    /* FIN PANEL DE CAPACITACIONES */

    /* PANEL DE PREMIOS */
        public function canjeados(){
            return $this->hasMany('App\Models\PremioCanjeado', 'FK_id_user')->orderBy('created_at','desc');
        }
    /* FIN PANEL DE PREMIOS */
}
