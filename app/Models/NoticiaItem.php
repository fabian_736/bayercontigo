<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaItem extends Model
{
    use HasFactory;

    protected $table='bc_noticia_item';
    protected $primaryKey='id_noticia_item';

    protected $fillable =[
        'id_noticia_item',
        'item',
        'FK_id_noticia',
    ];

    public function noticia(){
        return $this->belongsTo('App\Models\Noticia', 'FK_id_noticia');
    }
}
