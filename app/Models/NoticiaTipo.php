<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaTipo extends Model
{
    use HasFactory;

    protected $table='bc_noticias_tipo';
    protected $primaryKey='id_noticia_tipo';

    protected $fillable =[
        'id_noticia_tipo',
        'nombre',
    ];

    public function noticias(){
        return $this->hasMany('App\Models\Noticia', 'FK_id_tipo');
    }
}
