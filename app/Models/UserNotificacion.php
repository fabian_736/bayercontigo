<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserNotificacion extends Model
{
    use HasFactory;

    protected $table='bc_user_notificaciones';
    protected $primaryKey='id_user_notificaciones';

    protected $fillable =[
        'id_user_notificaciones',
        'FK_id_user',
        'FK_id_notificaciones',
        'read_at',
    ];

    protected $hidden = [
        'FK_id_notificaciones',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'FK_id_user')->withTrashed();
    }

    public function notificacion(){
        return $this->belongsTo('App\Models\Notificaciones', 'FK_id_notificaciones');
    }
}
