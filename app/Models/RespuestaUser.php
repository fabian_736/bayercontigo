<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RespuestaUser extends Model
{
    use HasFactory;

    protected $table='bc_respuesta_user';
    protected $primaryKey='id_respuesta_user';

    protected $fillable =[
        'id_respuesta_user',
        'respuesta',
        'pregunta',
        'opcion',
        'correcta',
        'FK_id_cuestionario_user',
    ];

    public function cusuario(){
        return $this->belongsTo('App\Models\CuestionarioUser', 'FK_id_cuestionario_user');
    }
}
