<?php

namespace App\Helper;

use App\Models\Notificaciones;
use App\Models\UserNotificacion;
use App\Models\AdminNotificacion;
use App\Models\Admin;
use App\Models\User;

class Notificacion
{
    
    public static function instance()
    {
        return new Notificacion();
    }
    
    ///////////////////////////////////////NOTIFICACIONES DE USUARIO NORMAL

    public function storeUniqueClient($data){
        $notificacion = Notificaciones::create($data);
        
        $value['FK_id_notificaciones'] = $notificacion->id_notificaciones;
        $value['FK_id_user'] = $data["user"];
        UserNotificacion::create($value);
    }
}

?>